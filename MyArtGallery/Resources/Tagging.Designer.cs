﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.34014
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MyArtGallery.Resources {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class Tagging {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Tagging() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("MyArtGallery.Resources.Tagging", typeof(Tagging).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Dodaj własny tag.
        /// </summary>
        public static string AddTag {
            get {
                return ResourceManager.GetString("AddTag", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Edytuj tag.
        /// </summary>
        public static string EditTag {
            get {
                return ResourceManager.GetString("EditTag", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Tag.
        /// </summary>
        public static string Tag {
            get {
                return ResourceManager.GetString("Tag", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Obrazy z tagiem.
        /// </summary>
        public static string TagImages {
            get {
                return ResourceManager.GetString("TagImages", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Nazwa tagu.
        /// </summary>
        public static string TagName {
            get {
                return ResourceManager.GetString("TagName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Taki tag już istnieje.
        /// </summary>
        public static string TagNameError {
            get {
                return ResourceManager.GetString("TagNameError", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Nazwa tagu jest za długa.
        /// </summary>
        public static string TagNameLong {
            get {
                return ResourceManager.GetString("TagNameLong", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Musisz podać słowo kluczowe.
        /// </summary>
        public static string TagNameRequired {
            get {
                return ResourceManager.GetString("TagNameRequired", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Brak słowa kluczowego.
        /// </summary>
        public static string TagNotFound {
            get {
                return ResourceManager.GetString("TagNotFound", resourceCulture);
            }
        }
    }
}
