﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.34014
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MyArtGallery.Resources {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class Errors {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Errors() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("MyArtGallery.Resources.Errors", typeof(Errors).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Aby używać taj opcji należy mieć uprawnienia administratora.
        /// </summary>
        public static string AdminOnly {
            get {
                return ResourceManager.GetString("AdminOnly", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Aby używać taj opcji należy mieć uprawnienia administratora lub moderatora.
        /// </summary>
        public static string AdminOrMod {
            get {
                return ResourceManager.GetString("AdminOrMod", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Nie można przenieść albumu.
        /// </summary>
        public static string AlbumMoveError {
            get {
                return ResourceManager.GetString("AlbumMoveError", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Album o takiej samej nazwie już istnieje w docelowej lokalizacji.
        /// </summary>
        public static string AlbumNameCollision {
            get {
                return ResourceManager.GetString("AlbumNameCollision", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Nie odnaleziono galerii.
        /// </summary>
        public static string AlbumNotFound {
            get {
                return ResourceManager.GetString("AlbumNotFound", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Zostałeś zbanowany - nie możesz publikować ani edytować istniejących treści, możesz korzystać tylko z korespondencji prywatnej.
        /// </summary>
        public static string BannedUser {
            get {
                return ResourceManager.GetString("BannedUser", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Nie można samemu sobie zmienić uprawnień.
        /// </summary>
        public static string CantChangeYourRole {
            get {
                return ResourceManager.GetString("CantChangeYourRole", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Nie możesz usunąć albumu domyślnego.
        /// </summary>
        public static string CantDeleteDefault {
            get {
                return ResourceManager.GetString("CantDeleteDefault", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Nie można przenieść albumu domyślnego.
        /// </summary>
        public static string CantMoveDefault {
            get {
                return ResourceManager.GetString("CantMoveDefault", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Komentarz nie istnieje.
        /// </summary>
        public static string CommentNotFound {
            get {
                return ResourceManager.GetString("CommentNotFound", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Można usunąć tylko pusty album.
        /// </summary>
        public static string DeleteError {
            get {
                return ResourceManager.GetString("DeleteError", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Błąd!.
        /// </summary>
        public static string Error {
            get {
                return ResourceManager.GetString("Error", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Zawartość przeznaczona dla zalogowanych użytkowników.
        /// </summary>
        public static string LoginRequired {
            get {
                return ResourceManager.GetString("LoginRequired", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Nie odnaleziono wiadomości.
        /// </summary>
        public static string MessageNotFound {
            get {
                return ResourceManager.GetString("MessageNotFound", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Możesz usunąć tylko pusty album.
        /// </summary>
        public static string MoveOnlyEmpty {
            get {
                return ResourceManager.GetString("MoveOnlyEmpty", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Nie możesz przenieść albumu do niego samego.
        /// </summary>
        public static string MovingError {
            get {
                return ResourceManager.GetString("MovingError", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Kolizja nazw.
        /// </summary>
        public static string NameCollision {
            get {
                return ResourceManager.GetString("NameCollision", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Nie ma takiej kategorii.
        /// </summary>
        public static string NoSuchGenre {
            get {
                return ResourceManager.GetString("NoSuchGenre", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Nie odnaleziono adresu.
        /// </summary>
        public static string NotFound {
            get {
                return ResourceManager.GetString("NotFound", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Nie odnaleziono obrazu(ów).
        /// </summary>
        public static string PictureNotFound {
            get {
                return ResourceManager.GetString("PictureNotFound", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Możesz edytować tylko swoje treści!.
        /// </summary>
        public static string PrivacyViolation {
            get {
                return ResourceManager.GetString("PrivacyViolation", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Materiał przeznaczony dla zarejestrowanych użytkowników.
        /// </summary>
        public static string PublicError {
            get {
                return ResourceManager.GetString("PublicError", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Materiał przeznaczony dla użytkowników powyżej 18 lat.
        /// </summary>
        public static string TooYoung {
            get {
                return ResourceManager.GetString("TooYoung", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Nie odnaleziono użytkownika(ów).
        /// </summary>
        public static string UserNotFound {
            get {
                return ResourceManager.GetString("UserNotFound", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Niewłaściwy adres email.
        /// </summary>
        public static string WrongEmail {
            get {
                return ResourceManager.GetString("WrongEmail", resourceCulture);
            }
        }
    }
}
