﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.34014
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MyArtGallery.Resources {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class CommentOptions {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal CommentOptions() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("MyArtGallery.Resources.CommentOptions", typeof(CommentOptions).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Skomentuj.
        /// </summary>
        public static string AddComment {
            get {
                return ResourceManager.GetString("AddComment", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Słabe.
        /// </summary>
        public static string Bad {
            get {
                return ResourceManager.GetString("Bad", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to komentarz.
        /// </summary>
        public static string Comment {
            get {
                return ResourceManager.GetString("Comment", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Komentarz został dodany.
        /// </summary>
        public static string CommentAdded {
            get {
                return ResourceManager.GetString("CommentAdded", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Komentarz został usunięty.
        /// </summary>
        public static string CommentDeleted {
            get {
                return ResourceManager.GetString("CommentDeleted", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Szczegóły komentarza.
        /// </summary>
        public static string CommentDetails {
            get {
                return ResourceManager.GetString("CommentDetails", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Zapisano zmiany.
        /// </summary>
        public static string CommentEdited {
            get {
                return ResourceManager.GetString("CommentEdited", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Za długi komentarz.
        /// </summary>
        public static string CommentLong {
            get {
                return ResourceManager.GetString("CommentLong", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Oceniłeś komentarz.
        /// </summary>
        public static string CommentRated {
            get {
                return ResourceManager.GetString("CommentRated", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Komentarz nie może być pusty.
        /// </summary>
        public static string CommentRequired {
            get {
                return ResourceManager.GetString("CommentRequired", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Dodawanie komentarzy do tego obrazu jest wyłączone.
        /// </summary>
        public static string CommentsOff {
            get {
                return ResourceManager.GetString("CommentsOff", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Wyłącz komentarze.
        /// </summary>
        public static string DisableComments {
            get {
                return ResourceManager.GetString("DisableComments", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Mocne.
        /// </summary>
        public static string Good {
            get {
                return ResourceManager.GetString("Good", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Brak komentarzy.
        /// </summary>
        public static string NoComments {
            get {
                return ResourceManager.GetString("NoComments", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Oceń.
        /// </summary>
        public static string Rate {
            get {
                return ResourceManager.GetString("Rate", resourceCulture);
            }
        }
    }
}
