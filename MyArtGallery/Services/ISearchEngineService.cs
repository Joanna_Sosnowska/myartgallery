﻿using MyArtGallery.Models;
using MyArtGallery.Models.SearchEngineViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace MyArtGallery.Services
{
    public interface ISearchEngineService:ISelectList
    {
        IQueryable<Picture> SearchPictures(string phrase);
        IQueryable<Picture> SearchPictures(SearchImagesViewModel model);
        IQueryable<UserProfile> SearchProfiles(string phrase);
        IQueryable<UserProfile> SearchProfiles(SearchProfilesViewModel model);

    }
}
