﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using MyArtGallery.Models;
using MyArtGallery.Models.Helpers;
using MyArtGallery.Repositories;
using MyArtGallery.Resources;
using MyArtGallery.Validation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace MyArtGallery.Services
{
    public class UserAccountService:IUserAccountService
    {
        ICategoryRepository category_repo;
        IAlbumRepository album_repo;
        UserManager<UserProfile> usermanager;
        IValidate validationdictionary;
        MyArtGalleryContext context;
        public UserAccountService()
        {
            this.context = new MyArtGalleryContext();
            this.usermanager = new UserManager<UserProfile>(new UserStore<UserProfile>(context));
            this.category_repo = new CategoryRepository(context);
            this.album_repo = new AlbumRepository(context);
        }
        public UserAccountService(IValidate val)
            :this()
        {
            this.validationdictionary = val;
        }

        public IEnumerable<SelectListItem> CategoriesSelect(string culture)
        {
            return category_repo.SelectCategories(culture);
        }

        public IEnumerable<SelectListItem> SelectAlbums(string username)
        {
            return album_repo.SelectAlbumsExcluding("", username);
        }

        public IQueryable<Category> SelectedCategories(IEnumerable<int> items)
        {
            return category_repo.Categories().Where(m => items.Contains(m.CategoryId));
        }


        public UserManager<UserProfile> UserManager
        {
            get
            {
                return usermanager;
            }
        }


        public IQueryable<Category> Categories()
        {
            return category_repo.Categories();
        }
        public async Task<UserProfile> CheckUserByEmail(string username, string email)
        {
            UserProfile user = await UserManager.FindByNameAsync(username);
            if(user==null)
                validationdictionary.AddError("UserName", Errors.UserNotFound);
            else if(!(user.Email).Equals(email))
                validationdictionary.AddError("Email", Errors.WrongEmail);
            return user;

        }


        public bool ValidAvatar(string file_url, bool is_url)
        {
            if (file_url.Equals("") && is_url)
                validationdictionary.AddError("Url", ImageEditorRes.AddUrl);
            else if(file_url.Equals("") && !is_url)
                validationdictionary.AddError("File", ImageEditorRes.AddFile);
            else
            {
                string typ = ImageHelper.GetFileType(file_url).ToLower();
                if (is_url)
                {
                    if (typ == "tiff" || typ == "tif")
                        validationdictionary.AddError("Url", ImageEditorRes.OldType);
                    if (!ImageHelper.ValidType(file_url))
                        validationdictionary.AddError("Url", ImageEditorRes.NotAnImage);
                }
                else
                {
                    if (typ == "tiff" || typ == "tif")
                        validationdictionary.AddError("File", ImageEditorRes.OldType);
                    if (!ImageHelper.ValidType(file_url))
                        validationdictionary.AddError("File", ImageEditorRes.NotAnImage);
                }
            }
            return validationdictionary.IsValid;
        }
    }
}