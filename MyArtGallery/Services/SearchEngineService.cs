﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using MyArtGallery.Helpers;
using MyArtGallery.Repositories;
using MyArtGallery.Models.SearchEngineViewModels;
using System;
using System.Collections.Generic;
using System.Data.Entity.SqlServer;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MyArtGallery.Models;

namespace MyArtGallery.Services
{
    public class SearchEngineService:ISearchEngineService
    {
        IPictureRepository picture_repo;
        ICategoryRepository category_repo;
        IAlbumRepository album_repo;
        UserManager<UserProfile> usermanager;
        MyArtGalleryContext context;
        public SearchEngineService()
        {
            this.context = new MyArtGalleryContext();
            this.usermanager = new UserManager<UserProfile>(new UserStore<UserProfile>(context));
            this.picture_repo = new PictureRepository(usermanager);
            this.category_repo = new CategoryRepository(context);
            this.album_repo = new AlbumRepository(context);
        }


        public IQueryable<Picture> SearchPictures(string phrase)
        {
            IQueryable <Picture> pics = picture_repo.All();
            if (phrase==null)
                return pics;
            IQueryable<Picture> pictures = pics.Where(p => SqlFunctions.PatIndex("%" + phrase.ToLower() + "%", p.Title.ToLower()) > 0);

            IQueryable<Picture> pictures2 = pics.Where(p => SqlFunctions.PatIndex("%" + phrase.ToLower() + "%", p.Category.CategoryName.ToLower()) > 0);
            IQueryable<Picture> pictures3 = pics.Where(p => SqlFunctions.PatIndex("%" + phrase.ToLower() + "%", p.Description.ToLower()) > 0);
            IQueryable<Picture> pictures4 = pictures3;
            foreach (var item in phrase.Split(' '))
            {
                pictures4 = pics.Where(p => p.Tags.Any(t => SqlFunctions.PatIndex("%" + item.ToLower() + "%", t.Name.ToLower()) > 0));
            
            }
            IQueryable<Picture> pictures5 = pics.Where(p => SqlFunctions.PatIndex("%" + phrase.ToLower() + "%", p.UserName.ToLower()) > 0);
            //or
            return ((pictures.Union(pictures2)).Union(pictures3.Union(pictures4))).Union(pictures5);

        }

        public IQueryable<Picture> SearchPictures(SearchImagesViewModel model)
        {
            IQueryable<Picture> pics = picture_repo.All();
            //AND!
            if (model.Tags != null)
            {
                foreach (string item in model.Tags.Split(','))
                {
                    string s = item.Trim();
                    pics = pics.Where(p => p.Tags.Any(t => t.Name.ToLower().Equals(s.ToLower())));
                }
            }
            if (model.Author != null)
                pics = pics.Where(p => SqlFunctions.PatIndex("%" + model.Author.ToLower() + "%", p.UserName.ToLower()) > 0);
            if (model.Title != null)
                pics = pics.Where(p => SqlFunctions.PatIndex("%"+model.Title.ToLower()+"%", p.Title.ToLower()) > 0);
            if(model.Description!=null)
                pics = pics.Where(p => SqlFunctions.PatIndex("%" + model.Description.ToLower() + "%", p.Description.ToLower()) > 0);
            if (!model.AllCategories)
                pics = pics.Where(p => p.CategoryId == model.CategoryId);


            return pics;
        }

        public IQueryable<UserProfile> SearchProfiles(string phrase)
        {
            //tylko po nickach
            IQueryable<UserProfile> users = usermanager.Users;
            if(phrase==null)
                return users;
            return users.Where(u => SqlFunctions.PatIndex("%" + phrase.ToLower() + "%", u.UserName.ToLower()) > 0);
        }

        public IQueryable<UserProfile> SearchProfiles(SearchProfilesViewModel model)
        {
            //AND
            IQueryable<UserProfile> users = usermanager.Users;
            if (model.SelectedGenres != null)
            {
                foreach (var item in model.SelectedGenres)
                {
                    users = users.Where(u => u.Genres.Any(g => g.CategoryId == item));
                }
            }
             if (model.UserName != null)
             {
                 users = users.Where(u => SqlFunctions.PatIndex("%" + model.UserName.ToLower() + "%", u.UserName.ToLower()) > 0);
             }
             return users;
        }


        public IEnumerable<SelectListItem> CategoriesSelect(string culture)
        {
            return category_repo.SelectCategories(culture);
        }


        public IEnumerable<SelectListItem> SelectAlbums(string username)
        {
            return album_repo.SelectAlbumsExcluding("", username);
        }
    }
}