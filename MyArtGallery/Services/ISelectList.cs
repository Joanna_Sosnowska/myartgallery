﻿using MyArtGallery.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace MyArtGallery.Services
{
    public interface ISelectList
    {
        IEnumerable<SelectListItem> CategoriesSelect(string culture);
        IEnumerable<SelectListItem> SelectAlbums(string username);
    }
}