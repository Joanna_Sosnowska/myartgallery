﻿using MyArtGallery.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyArtGallery.Services
{
    public interface IImageUserService
    {
        Task AddNote(Notification note);
        Task<bool> AddFavourite(Favourite favourite);
        Task<bool> DeleteFav(int id);
        Picture GetPicture(int id);
        Task<Album> GetAlbum(int id);
        Task<Picture> GetPictureAsync(int id);
        Task<UserProfile> GetUser(string name);
        Task<bool> CheckRole(string userid, string role);
    }
}
