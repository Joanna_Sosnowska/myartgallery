﻿using MyArtGallery.Helpers;
using MyArtGallery.Models.Helpers;
using MyArtGallery.Repositories;
using MyArtGallery.Validation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using MyArtGallery.Models;
using MyArtGallery.Resources;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace MyArtGallery.Services
{
    public class ImageService:IImageService
    {
        IPictureRepository picture_repo;
        IAlbumRepository album_repo;
        ICategoryRepository category_repo;
        ITagRepository tag_repo;
        INoteRepository note_repo;
        IValidate validationdictionary;
        private UserManager<UserProfile> usermanager;
        MyArtGalleryContext context;
        public ImageService()
        {
            this.context = new MyArtGalleryContext();
            this.picture_repo = new PictureRepository(context);
            this.category_repo = new CategoryRepository(context);
            this.album_repo = new AlbumRepository(context);
            this.tag_repo = new TagRepository(context);
            this.note_repo = new NoteRepository(context);
            usermanager = new UserManager<UserProfile>(new UserStore<UserProfile>(context));
        }
        public ImageService(IValidate val)
            :this()
        {
            this.validationdictionary = val;
        }

        public async Task<bool> UploadPicture(Picture picture, string username)
        {
            try
            {
                picture.AdditionDate = DateTime.Now;
                picture.LastModifiedDate=DateTime.Now;
                picture.Downloads = 0;
                picture.Favourites = 0;
                picture.Comments = new List<Comment> { };
                await picture_repo.AddPicture(picture);
                
                return true;
            }
            catch
            {
                return false;
            }
        }
        //rozszerzenie metody helpera - np. w przypadku avatara plik może być null, a w przypadku obrazu w galerii - nie
        public bool UrlValidation(string url)
        {
            if (url.Equals(""))
                validationdictionary.AddError("Url", ImageEditorRes.AddUrl);
            else
            {
                string typ = ImageHelper.GetFileType(url).ToLower();
                if (typ == "tiff" || typ == "tif")
                    validationdictionary.AddError("Url", ImageEditorRes.OldType);
                if (!ImageHelper.ValidType(url))
                    validationdictionary.AddError("Url", ImageEditorRes.NotAnImage);
            }
           return validationdictionary.IsValid;
        }
        public bool FileValidation(string name)
        {
            if (name.Equals(""))
                validationdictionary.AddError("File", ImageEditorRes.AddUrl);
            else
            {
                string typ = ImageHelper.GetFileType(name).ToLower();
                if (typ == "tiff" || typ == "tif")
                    validationdictionary.AddError("File", ImageEditorRes.OldType);
                if (!ImageHelper.ValidType(name))
                    validationdictionary.AddError("File", ImageEditorRes.NotAnImage);
            }
            return validationdictionary.IsValid;
        }

        public async Task<Picture> GetPictureAsync(int id)
        {
            return await picture_repo.getPictureAsync(id);
        }

        public Picture GetPicture(int id)
        {
            return picture_repo.getPicture(id);
        }

        public async Task<bool> DeletePicture(Picture picture)
        {
            try
            {
            Album a = album_repo.getAlbum(picture.Album.Path);
            if (a.CoverUrl != null)
            {
                if (a.CoverUrl.Equals(picture.ThumbnailUrl))
                    a.CoverUrl = ImageHelper.DefaultPath;
            }
            await album_repo.UpdateAlbum(a);
            await picture_repo.DeletePicture(picture.PictureId);
            }
            catch
            {
                return false;
            }
            return true;
        }

        public async Task<bool> EditPictureInfo(Picture picture)
        {

            try
            {
                await picture_repo.UpdatePicture(picture);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public async Task<bool> EditPicture(Picture picture, WebImage image)
        {
            await picture_repo.UpdatePicture(picture);
            return true;
        }


        public IQueryable<Picture> Pictures()
        {
            return picture_repo.All();
        }


        public IQueryable<Picture> Pictures(string name)
        {
            return picture_repo.userPictures(name);
        }


        public async Task<bool> SetAlbumCover(Picture picture)
        {
            Album album = await album_repo.getAlbum(picture.AlbumId);
            album.CoverUrl = picture.ThumbnailUrl;
            album.LastModifiedDate = DateTime.Now;

            await album_repo.UpdateAlbum(album);
            return true;

        }


        public async Task<Album> GetAlbum(int id)
        {
            return await album_repo.getAlbum(id);
        }

        public IEnumerable<SelectListItem> CategoriesSelect(string culture)
        {
            return category_repo.SelectCategories(culture);
        }


        public IEnumerable<SelectListItem> SelectAlbums(string username)
        {
            return album_repo.SelectAlbumsExcluding("", username);
        }


        public IQueryable<Picture> Pictures(string name, int id)
        {
            return picture_repo.userPicturesLatest(name, id);
        }

        public async Task<List<Tag>> AddTags(string raw)
        {
            List<Tag> tags = new List<Tag> { };
            foreach (string item in raw.Split(','))
            {
                string proto_tag = item.Trim(' ');
                if (proto_tag != "")
                {
                    Tag t = await tag_repo.getTag(proto_tag);
                    if (t == null)
                    {
                        Tag t1 = new Tag { Name = proto_tag, Pictures = new List<Picture> { } };
                        await tag_repo.AddTag(t1);
                        //tag staje sie istniejacym tagiem
                        Tag t2 = await tag_repo.getTag(proto_tag);
                        tags.Add(t2);
                    }
                    else tags.Add(t);
                }
            }
            return tags;
            
        }


        public async Task<bool> AddFavourite(Favourite favourite)
        {
            await picture_repo.AddFavourite(favourite);
            return true;
        }


        public async Task AddNote(Notification note)
        {
            await note_repo.AddNote(note);
        }


        public async Task<bool> DeleteFav(int id)
        {
            try
            {
                await picture_repo.DeleteFav(id);
                return true;
            }
            catch
            {
                return false;
            }
        }


        public async Task<UserProfile> GetUser(string name)
        {
            return await usermanager.FindByNameAsync(name);
        }


        public async Task<bool> CheckRole(string userid, string role)
        {
            return await usermanager.IsInRoleAsync(userid, role);
        }
    }
}