﻿using MyArtGallery.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Helpers;
using System.Web.Mvc;

namespace MyArtGallery.Services
{
    public interface IImageService:IImageUserService, ISelectList
    {
        Task<bool> UploadPicture(Picture picture, string username);

        Task<List<Tag>> AddTags(string raw);
        Task<bool> SetAlbumCover(Picture picture);
        IQueryable<Picture> Pictures();
        IQueryable<Picture> Pictures(string name);
        IQueryable<Picture> Pictures(string name, int id);
        Task<bool> DeletePicture(Picture picture);
        Task<bool> EditPictureInfo(Picture picture);
        bool FileValidation(string filename);
        bool UrlValidation(string url);
        Task<bool> EditPicture(Picture picture, WebImage image);


    }
}
