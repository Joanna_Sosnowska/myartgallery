﻿using Microsoft.AspNet.Identity;
using MyArtGallery.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyArtGallery.Services
{
    public interface IUserAccountService:ISelectList
    {
        IQueryable<Category> SelectedCategories(IEnumerable<int> items);
        IQueryable<Category> Categories();
        UserManager<UserProfile> UserManager{get;}
        Task<UserProfile> CheckUserByEmail(string username, string email);
        bool ValidAvatar(string file_url, bool is_url);
    }

}
