﻿$(function () {
    $('.form-horizontal #cancel').click(function () {
        $('#top').empty();
        return false;
    });
    $('.form-horizontal').submit(function () {
        var form = $(this);
        $.post(form.attr('action'), $(this).serialize(), function (result) {
            $('#top').html(result);
        });
        return false;
    });
    $('.rating-form').submit(function () {
        var form = $(this);
        $.post(form.attr('action'), $(this).serialize(), function (result) {
            $('#top').html(result);
        }); return false;
    });
    });