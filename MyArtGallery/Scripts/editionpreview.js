﻿$(document).ready(function () {
    var rotated = false;
    var preview = $('img#preview');
    preview.load(function () {           
        preview.css({
            width: '400px',
            height:'400px',
            marginTop: '30px',
            marginLeft: '30px',
        });
        setCoordinates();
        imgselect.setOptions({
            x1: 0,
            y1: 0,
            x2:  $(this).width(),
            y2: $(this).height(),
            show: true,
        });

    });
    var picture_proto = $('img')[0].src;
    var setCoordinates = function (x, y, width, height) {
                $('#X').val(x || 0);
                $('#Y').val(y || 0);
                $('#Width').val(width || preview[0].naturalWidth);
                $('#Height').val(height || preview[0].naturalHeight);
    };

    var imgselect = preview.imgAreaSelect({
    handles: true,
    instance: true,
    parent:'body',
    onSelectChange: setCoordinates,
    onSelectEnd: function (s, e) {
        //nowy układ współrzędnych
        var scale_w = preview[0].naturalWidth / preview.width();
        var scale_h = preview[0].naturalHeight / preview.height();
        //jeszcze inny układ wspórzędnych - obrót powoduje dwa przeskalowania
        var scale_w2 = preview[0].naturalWidth / preview[0].naturalHeight;
        var scale_h2 = preview[0].naturalHeight / preview[0].naturalWidth;
        var podloga = Math.floor;
        if (!rotated) {
            setCoordinates(podloga(scale_w * e.x1), podloga(scale_h * e.y1), podloga(scale_w * e.width), podloga(scale_h * e.height));
        }
        else {
            setCoordinates(podloga(scale_h * scale_w2 * e.y1), podloga(scale_w * scale_h2 * e.x1), podloga(scale_h * scale_w2 * e.height), podloga(scale_w * scale_h2 * e.width));
        }
    }
    });
    preview.load();
      $('#imgurl input').change(function () {
          preview.attr('src', this.value);
      });
      $('#imgfile input').change(function (evt) {
          var f = evt.target.files[0];
          var fr = new FileReader();
          fr.onload = function (e) {
              var im = e.target.result;
              preview.attr('src', im);
          };
          fr.readAsDataURL(f);
      });

      preview.change(function () {
          var h1 = preview[0].height();
          preview.css('width', '400px');
          preview.css('height', h1);

      });

      $('div#edit_image input').change(function () {
          if ($(this).is(':checked')) {
              preview.attr('src', picture_proto);
          }

      });
  $('div#angles input[value="90"]').change(function () {
      if ($(this).is(':checked')) {
          rotated = true;
          preview.transition({ rotate: 90 });
          preview.load();
      }
  });
  $('div#angles input[value="270"]').change(function () {
      if ($(this).is(':checked')) {
          rotated = true;
          preview.transition({ rotate: 270 });
          preview.load();
          }
  });
  $('div#angles input[value="180"]').change(function () {
      if ($(this).is(':checked')) {
          rotated = false;
          preview.transition({ rotate: 180 });
          preview.load();
      }
  });
  $('div#angles input[value="0"]').change(function () {
      if ($(this).is(':checked')) {
          rotated = false;
          preview.transition({ rotate: 0 });
          preview.load();
      }
  });
  $('#fliphor input').change(function () {      
      if ($(this).is(':checked')) {
          if (!rotated) {
              preview.transition({
                  perspective: '100px', rotateY: '180deg'
              });
          }
          else {
              preview.transition({
                  perspective: '100px', rotateX: '180deg'
              });
          }
      }
      else {
          if (!rotated) {
              preview.transition({
                  perspective: '100px', rotateY: '0deg'
              });
          }
          else {
              preview.transition({
                  perspective: '100px', rotateX: '0deg'
              });
          }
      }
      preview.load();
  });

  $('#flipvert input').change(function () {
      if ($(this).is(':checked')) {
          if (!rotated) {
              preview.transition({
                  perspective: '100px', rotateX: '180deg'
              });
          }
          else {
              preview.transition({
                  perspective: '100px', rotateY: '180deg'
              });
          }
      }
      else {
          if (!rotated) {
              preview.transition({
                  perspective: '100px', rotateX: '0deg'
              });
          }
          else {
              preview.transition({
                  perspective: '100px', rotateY: '0deg'
              });
          }
      }
      preview.load();
  });
  $('div#animated input').change(function () {
      var angles = $('div#angles input');
      var flip1 = $('div#fliphor input');
      var flip2 = $('div#flipvert input');
      var angles_par = $('div#angles');
      var flip1_par = $('div#fliphor');
      var flip2_par = $('div#flipvert');
      if ($(this).is(':checked')) {
          angles.each(function (index, element) { $(this).prop('disabled', true) });
          flip1.prop('disabled', true);
          flip2.prop('disabled', true);
          angles_par.css('background-color', '#cddfc9');
          flip1_par.css('background-color', '#cddfc9');
          flip2_par.css('background-color', '#cddfc9');

      }
      else {
          angles.each(function (index, element) { $(this).prop('disabled', false) });
          flip1.prop('disabled', false);
          flip2.prop('disabled', false);
          angles_par.css('background-color', 'transparent');
          flip1_par.css('background-color', 'transparent');
          flip2_par.css('background-color', 'transparent');
      }

  });

});