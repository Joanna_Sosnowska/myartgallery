﻿$(document).ready(function () {
    $('div#imgurl').hide();
    $('div#imgfile').hide();
    $('div#replacements').hide();
    $('div#isurl > input').change(function () {
        $('div#replacements').css({
            'height': '100px',
        });
        if ($(this).is(':checked')) {
            $('div#imgurl').show();
            $('div#imgfile').hide();
            
            $('div#isfile input').attr('checked', false);
        }
        else {
            $('div#imgurl').hide();
            $('div#imgfile').show();
        }
    });
    $('div#isfile > input').change(function () {
        $('div#replacements').css({
            'height': '100px',
        });
        if ($(this).is(':checked')) {
            $('div#imgfile').show();
            $('div#imgurl').hide();
            $('div#isurl input').attr('checked', false);
        }
        else {
            $('div#imgfile').hide();
            $('div#imgurl').show();
        }
    });
    $('div#replace_image > input').change(function () {
        if ($(this).is(':checked')) {
            $('div#replacements').show();
            $('div#imgurl').show();
            $('div#imgfile').show();
            $('div#edit_image input').attr('checked', false);
        }
        else {
            $('div#replacements').hide();
            $('div#imgurl').hide();         
            $('div#imgfile').hide();
        }
    });
    $('div#edit_image > input').change(function () {
        if ($(this).is(':checked')) {
            $('div#replacements').hide();
            $('div#imgurl').hide();
            $('div#imgfile').hide();
            $('div#replace_image input').attr('checked', false);
        }
    });
});