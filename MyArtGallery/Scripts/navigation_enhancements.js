﻿$(document).ready(function () {
    $('input[type="submit"]').attr('disabled', false);
    $('.form-horizontal, .image-editor').submit(function () {
        if ($(this).valid()) {
            $('input[type="submit"]').attr('disabled', true);
            $('input[type="submit"]').attr('value', "....");
            //wygląd przycisku
            $('.sub-button').css({
                'background-color': '#cddfc9',
                'background-image': 'url(/content/Images/fancybox_loading.gif)',
                'background-repeat': 'no-repeat',
            });
        }
    });
    $('.form-horizontal input[type="text"]').first().focus();

    var myAddress = location.protocol + '//' + location.hostname;
    $('a[href^="http://"]').not('a[href^="'+myAddress+'"]').attr('target', '_blank');
    $('#protest-list a').attr('target', '_blank');
    $('#featured > a').fancybox({
        padding: 6,
        overlayColor: '#333',
        overlayOpacity: 0.5,
        transitionIn: 'elastic',
        transitionOut: 'elastic',
        width: '90%',
        height: '90%'

    });

    $('#featured-images a').fancybox({
        padding: 6,
        overlayColor: '#333',
        overlayOpacity: 0.5,
        transitionIn: 'elastic',
        transitionOut: 'elastic',
        width: '90%',
        height: '90%'

    });


});