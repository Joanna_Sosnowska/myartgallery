﻿$(document).ready(function () {
    function activate() {
        $('input[type="submit"]').attr('disabled', false);
        $('.sub-button').css({
            'background-color': '#89a820',
            'color': '#333'
        });
    }
    function deactivate() {
        $('input[type="submit"]').attr('disabled', true);
        $('.sub-button').css({
            'background-color': '#cddfc9',
            'color': '#746f6f'
        });
    }
    deactivate();
    $('div#imgurl > input').on('change', function () {
        if ($(this).val().length != 0 && $('div#isurl > input').is(':checked')) {
            //aktywuj przycisk
            activate();
        }
        else {
            //deaktywuj przycisk
            deactivate();
        }

    });
    $('div#isurl > input').on('change', function () {
        if ($('div#imgurl > input').val().length != 0 && $(this).is(':checked')) {
            //aktywuj przycisk
            activate();
        }
        else {
            //deaktywuj przycisk
            deactivate();
        }

    });
    $('div#imgfile > input').on('change', function () {
        if ($(this).val().length != 0 && $('div#isfile > input').is(':checked')) {
            //aktywuj przycisk
            activate();
        }
        else {
            //deaktywuj przycisk
            deactivate();
        }


    });
    $('div#isfile > input').on('change', function () {
        if ($('div#imgfile > input').val().length != 0 && $(this).is(':checked')) {
            //aktywuj przycisk
            activate();
        }
        else {
            //deaktywuj przycisk
            deactivate();
        }

    });

    $('div#isurl > input').on('change', function () {
        if (!$(this).is(':checked')) {
            deactivate();
        }

    });
    $('div#isfile > input').on('change', function () {
        if (!$(this).is(':checked')) {
            deactivate();
        }

    });
});