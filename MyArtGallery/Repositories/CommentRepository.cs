﻿using MyArtGallery.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace MyArtGallery.Repositories
{
    public class CommentRepository:ICommentRepository
    {
        private MyArtGalleryContext db;
        public CommentRepository()
        {
            this.db = new MyArtGalleryContext();
        }
        public CommentRepository(MyArtGalleryContext ctx)
        {
            this.db = ctx;
        }
        public async Task<bool> RemoveComment(Comment comment)
        {
            try
            {
                db.Comments.Remove(await getCommentAsync(comment.CommentId));
                await db.SaveChangesAsync();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public async Task<Comment> AddComment(Comment comment)
        {
            comment.AddedDate = DateTime.Now;
            comment.EditedDate = DateTime.Now;
            comment.Bad = 0;
            comment.Good = 0;
            comment.Replies = new List<Comment> { };
            db.Comments.Add(comment);
            await db.SaveChangesAsync();
            return comment;

        }

        public async Task<Comment> EditComment(Comment comment)
        {
            comment.EditedDate = DateTime.Now;
            db.Entry(comment).State = EntityState.Modified;
            await db.SaveChangesAsync();
            return comment;
        }

        public IQueryable<Comment> ListComments(int pictureid)
        {
            return db.Comments.Where(c => c.PictureId.Equals(pictureid)).OrderByDescending(c=>c.EditedDate);
        }

        public IQueryable<Comment> ListComments(string username)
        {
            return db.Comments.Where(c => c.UserName.Equals(username)).OrderByDescending(c => c.EditedDate);
        }

        public async Task<Comment> getCommentAsync(int id)
        {
            return await db.Comments.FindAsync(id);
        }

        public Comment getComment(int id)
        {
            return db.Comments.Find(id);
        }

        public void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
        }

    }
}