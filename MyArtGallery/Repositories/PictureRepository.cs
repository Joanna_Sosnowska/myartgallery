﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using MyArtGallery.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace MyArtGallery.Repositories
{
    public class PictureRepository:IPictureRepository
    {
        MyArtGalleryContext db = new MyArtGalleryContext();
        public UserManager<UserProfile> UserManager { get; private set; }
        public PictureRepository(UserManager<UserProfile> userManager)
        {
            UserManager = userManager;
        }

        public PictureRepository()
            : this(new UserManager<UserProfile>(new UserStore<UserProfile>(new MyArtGalleryContext())))
        {
        }
        public PictureRepository(MyArtGalleryContext ctx)
        {
            this.db = ctx;
            this.UserManager = new UserManager<UserProfile>(new UserStore<UserProfile>(db));
        }
        public async Task<Picture> getPictureAsync(int id)
        {
            return await db.Pictures.FindAsync(id);
        }

        public Picture getPicture(int id)
        {
            return db.Pictures.Find(id);
        }


        public IQueryable<Picture> userPictures(string username)
        {
            return db.Pictures.Where(p => p.UserName.Equals(username));
        }

        public async Task AddPicture(Picture pic)
        {
            db.Pictures.Add(pic);
            await db.SaveChangesAsync();
        }

        public async Task UpdatePicture(Picture pic)
        {
            db.Entry(pic).State = EntityState.Modified;
            await db.SaveChangesAsync();
        }

        public async Task DeletePicture(int id)
        {
            db.Pictures.Remove(await getPictureAsync(id));
            await db.SaveChangesAsync();
        }

        public IQueryable<Picture> All()
        {
            return db.Pictures;
        }


        public IQueryable<Picture> userPicturesLatest(string username, int i)
        {
            IQueryable<Picture> userpictures = userPictures(username);
            if (userpictures.Count() <= i)
                return userpictures;
            else
                return userpictures.OrderByDescending(c => c.LastModifiedDate).Take(i);
        }

        public async Task AddFavourite(Favourite fav)
        {
            db.Favourites.Add(fav);
            await db.SaveChangesAsync();

        }


        public async Task DeleteFav(int id)
        {
            db.Favourites.Remove(await db.Favourites.FindAsync(id));
            await db.SaveChangesAsync();
        }

        public void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
        }


    }
}