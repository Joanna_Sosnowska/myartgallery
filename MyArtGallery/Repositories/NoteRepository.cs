﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using MyArtGallery.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace MyArtGallery.Repositories
{
    public class NoteRepository:INoteRepository
    {
        private MyArtGalleryContext db = new MyArtGalleryContext();
        public UserManager<UserProfile> UserManager { get; private set; }
        public NoteRepository(UserManager<UserProfile> userManager)
        {
            UserManager = userManager;
        }

        public NoteRepository()
            : this(new UserManager<UserProfile>(new UserStore<UserProfile>(new MyArtGalleryContext())))
        {
        }
        public NoteRepository(MyArtGalleryContext ctx)
        {
            this.db = ctx;
            this.UserManager = new UserManager<UserProfile>(new UserStore<UserProfile>(db));
        }
        public async Task AddNote(Notification note)
        {
            note.WasRead = false;
            db.Notifications.Add(note);

            await db.SaveChangesAsync();
        }

        public IQueryable<Notification> ReceivedNotes(string username)
        {
            return db.Notifications.Where(n => n.AddresseeId.Equals(username));
        }

        public IQueryable<Notification> SentNotes(string name)
        {
            return db.Notifications.Where(n => n.UserId.Equals(name));
        }

        public IQueryable<Notification> ReceivedByAuthor(string adr, string auth)
        {
            IQueryable<Notification> notes = db.Notifications.Where(n => n.AddresseeId.Equals(adr));
            return notes.Where(n => n.UserId.Equals(auth));
        }

        public async Task<Notification> getNotification(int? id)
        {
            return await db.Notifications.FindAsync(id);
        }

        public async Task SignAsRead(Notification note)
        {
            note.WasRead = true;
            db.Entry(note).State = EntityState.Modified;
            await db.SaveChangesAsync();
        }

        public IQueryable<Notification> NewNotifications(string username)
        {
            IQueryable<Notification> list1 = db.Notifications.Where(m => m.AddresseeId.Equals(username));
            return list1.Where(m => m.WasRead.Equals(false));
        }


        public IQueryable<Notification> ReceivedByType(string userid, int type)
        {
            return ReceivedNotes(userid).Where(n => (int)n.Kind == type);
        }

        public void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
        }
    }
}