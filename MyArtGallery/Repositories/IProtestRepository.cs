﻿using MyArtGallery.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyArtGallery.Repositories
{
    public interface IProtestRepository:IRepository
    {
        Task AddProtest(Protest protest);
        Task<bool> DeleteProtest(int id);
        Task<Protest> GetProtest(int id);
        IQueryable<Protest> GetProtests();
        IQueryable<Protest> GetProtests(string UserName);
        IQueryable<Protest> GetProtests(ProtestType kind);
    }
}
