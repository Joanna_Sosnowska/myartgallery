﻿using MyArtGallery.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace MyArtGallery.Repositories
{
    public class TagRepository:ITagRepository
    {
        private MyArtGalleryContext db;
        public TagRepository()
        {
            this.db = new MyArtGalleryContext();
        }
        public TagRepository(MyArtGalleryContext ctx)
        {
            this.db = ctx;
        }
        public async Task AddTag(Tag tag)
        {
            db.Tags.Add(tag);

            await db.SaveChangesAsync();
        }

        public async Task UpdateTag(Tag tag)
        {
            db.Entry(tag).State = EntityState.Modified;
            await db.SaveChangesAsync();
        }

        public IQueryable<Tag> Tags()
        {
            return db.Tags;
        }

        public async Task<Tag> getTag(int? id)
        {
            Tag t = await db.Tags.FindAsync(id);
            return t;
        }


        public async Task<Tag> getTag(string name)
        {
            return await db.Tags.Where(t => t.Name.Equals(name)).FirstOrDefaultAsync();
        }

        public void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
        }


       
    }
}