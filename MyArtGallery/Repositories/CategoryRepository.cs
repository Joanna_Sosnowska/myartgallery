﻿using MyArtGallery.Helpers;
using MyArtGallery.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace MyArtGallery.Repositories
{
    public class CategoryRepository:ICategoryRepository
    {
        private MyArtGalleryContext db;
        public CategoryRepository()
        {
            this.db = new MyArtGalleryContext();
        }
        public CategoryRepository(MyArtGalleryContext ctx)
        {
            this.db = ctx;
        }

        public IQueryable<Category> Categories()
        {
            return db.Categories;
        }

        public IQueryable<Category> MainCategories()
        {
            return db.Categories.Where(c => c.ParentId == null);
        }

        public Category GetCategory(string name)
        {
            return db.Categories.Where(c => c.CategoryName.Equals(name)).FirstOrDefault();
        }

        public async Task<Category> GetCategory(int? id)
        {
            return await db.Categories.FindAsync(id);
        }


        public IQueryable<Picture> ImagesByCategory(string name)
        {
            return db.Pictures.Where(m => m.Category.CategoryName.Equals(name));
        }

        public IEnumerable<SelectListItem> SelectCategories(string lang)
        {

            string culture = CultureHelper.GetImplementedCulture(lang); 
            var categories = Categories()
                        .Select(x =>
                                new SelectListItem
                                {
                                    Value = x.CategoryId.ToString(),
                                    //taka skomplikowana jedna linijka - sprawdza wybrany język i czy dana kategoria jest podkategorią innej - zagnieżdżony op. trójargumentowy
                                    Text = culture.Equals("pl") ? ((x.Parent == null) ? x.CategoryName : x.Parent.CategoryName + ">" + x.CategoryName) : ((x.Parent == null) ? x.CategoryNameEn : x.Parent.CategoryNameEn + ">" + x.CategoryNameEn),

                                });

            return new SelectList(categories, "Value", "Text");
        }

        public void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
        }
    }
}