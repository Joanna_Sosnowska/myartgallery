﻿using MyArtGallery.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyArtGallery.Repositories
{
    public interface ITagRepository:IRepository
    {
        Task AddTag(Tag tag);
        Task UpdateTag(Tag tag);
        IQueryable<Tag> Tags();
        //IQueryable<Picture> ImagesByTag(string tag_name);
        Task<Tag> getTag(int? id);
        Task<Tag> getTag(string name);
    }
}
