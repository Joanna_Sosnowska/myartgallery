﻿using MyArtGallery.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyArtGallery.Repositories
{
    public interface IPictureRepository:IRepository
    {
        IQueryable<Picture> All();
        Task<Picture> getPictureAsync(int id);
        Picture getPicture(int id);
        IQueryable<Picture> userPictures(string username);
        IQueryable<Picture> userPicturesLatest(string username, int i);
        Task AddPicture(Picture pic);
        Task UpdatePicture(Picture pic);
        Task DeletePicture(int id);
        Task DeleteFav(int id);
        Task AddFavourite(Favourite fav);

    }
}
