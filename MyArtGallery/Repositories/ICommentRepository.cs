﻿using MyArtGallery.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyArtGallery.Repositories
{
    public interface ICommentRepository:IRepository
    {
        Task<bool> RemoveComment(Comment comment);
        Task<Comment> AddComment(Comment comment);
        Task<Comment> EditComment(Comment comment);
        IQueryable<Comment> ListComments(int pictureid);
        IQueryable<Comment> ListComments(string username);
        Task<Comment> getCommentAsync(int id);
        Comment getComment(int id);
    }

}
