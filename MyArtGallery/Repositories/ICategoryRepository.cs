﻿using MyArtGallery.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace MyArtGallery.Repositories
{
    public interface ICategoryRepository:IRepository
    {
        //List<Category> CategoryList();
        IQueryable<Category> Categories();
        IQueryable<Category> MainCategories();
        Category GetCategory(string name);
        Task<Category> GetCategory(int? id);
        IQueryable<Picture> ImagesByCategory(string name);
        IEnumerable<SelectListItem> SelectCategories(string lang);
    }
}
