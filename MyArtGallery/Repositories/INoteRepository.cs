﻿using MyArtGallery.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyArtGallery.Repositories
{
    public interface INoteRepository:IRepository
    {
        Task AddNote(Notification note);
        IQueryable<Notification> ReceivedNotes(string username);
        IQueryable<Notification> SentNotes(string name);
        IQueryable<Notification> ReceivedByAuthor(string userid, string user2id);
        IQueryable<Notification> ReceivedByType(string userid, int type);
        Task<Notification> getNotification(int? id);
        Task SignAsRead(Notification note);
        IQueryable<Notification> NewNotifications(string username);
    }
}
