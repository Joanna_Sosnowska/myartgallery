﻿using MyArtGallery.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace MyArtGallery.Repositories
{
    public class ProtestRepository:IProtestRepository
    {
        private MyArtGalleryContext db;
        public ProtestRepository()
        {
            this.db = new MyArtGalleryContext();
        }
        public ProtestRepository(MyArtGalleryContext ctx)
        {
            this.db = ctx;
        }
        public async Task AddProtest(Protest protest)
        {
            db.Protests.Add(protest);
            await db.SaveChangesAsync();
        }

        public async Task<bool> DeleteProtest(int id)
        {
            try
            {
                db.Protests.Remove(await GetProtest(id));
                await db.SaveChangesAsync();
                return true;
            }
            catch
            {
                return false;
            }

        }

        public IQueryable<Protest> GetProtests()
        {
            return db.Protests.OrderByDescending(p => p.SentDate);
        }

        public IQueryable<Protest> GetProtests(string username)
        {
            return db.Protests.Where(p => p.UserId.Equals(username)).OrderByDescending(p=>p.SentDate);
        }

        public IQueryable<Protest> GetProtests(ProtestType kind)
        {
            return db.Protests.Where(p => p.Kind == kind).OrderByDescending(p => p.SentDate);
        }

        public async Task<Protest> GetProtest(int id)
        {
            return await db.Protests.FindAsync(id);
        }

        public void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
        }
    }
}