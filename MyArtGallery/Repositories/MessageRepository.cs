﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using MyArtGallery.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace MyArtGallery.Repositories
{
    public class MessageRepository:IMessageRepository
    {
        private MyArtGalleryContext db = new MyArtGalleryContext();
        public UserManager<UserProfile> UserManager { get; private set; }
        public MessageRepository(UserManager<UserProfile> userManager)
        {
            UserManager = userManager;
        }

        public MessageRepository()
            : this(new UserManager<UserProfile>(new UserStore<UserProfile>(new MyArtGalleryContext())))
        {
        }
        public MessageRepository(MyArtGalleryContext ctx)
        {
            this.db = ctx;
            this.UserManager = new UserManager<UserProfile>(new UserStore<UserProfile>(db));
        }
        public async Task AddMessage(Message message)
        {
            message.WasRead = false;
            db.Messages.Add(message);
            
            await db.SaveChangesAsync();
        }

        public IQueryable<Message> ReceivedMessages(string name)
        {
            IQueryable<Message> msgs = db.Messages.Where(m => m.Addressee.UserName.Equals(name));
            return msgs;
        }

        public IQueryable<Message> ReceivedByAuthor(string myid, string id)
        {
            
            IQueryable<Message> m1 = db.Messages.Where(m => m.Addressee.UserName == myid);
            if (!id.Equals(""))
                return m1.Where(m => m.Author.UserName.Equals(id));
            else
                return m1.Where(m => m.Author.Equals(null));
            
     
        }
       
        public async Task<Message> getMessage(int? id)
        {
            Message m = await db.Messages.FindAsync(id);
            return m;
        }


        public IQueryable<Message> SentMessages(string id)
        {
            return db.Messages.Where(m => m.Author.UserName.Equals(id));
        }


        public async Task SignAsRead(Message message)
        {
            message.WasRead = true;
            db.Entry(message).State = EntityState.Modified;
            await db.SaveChangesAsync();

        }

        public IQueryable<Message> NewMessages(string username)
        {
            IQueryable<Message> list1 = db.Messages.Where(m => m.Addressee.UserName.Equals(username));
            return list1.Where(m => m.WasRead.Equals(false));
        }



        public void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
        }
    }
}