﻿using MyArtGallery.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace MyArtGallery.Repositories
{
    public interface IAlbumRepository:IRepository
    {
        Task<Album> getAlbum(int id);
        Album getAlbum(string path);
        Task AddAlbum(int parentid, Album a);
        Task UpdateAlbum(Album a);
        Task DeleteAlbum(Album a);
        IQueryable<Album> albumList(string username);
        IQueryable<Album> albumList();
        IEnumerable<Picture> galleryPictures(string gallerypath);
        IEnumerable<SelectListItem> SelectAlbumsExcluding(string path, string username);
        IEnumerable<SelectListItem> SelectAlbumContents(string path);


    }
}
