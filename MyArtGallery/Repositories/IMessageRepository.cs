﻿using MyArtGallery.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyArtGallery.Repositories
{
    public interface IMessageRepository:IRepository
    {
        Task AddMessage(Message message);
        IQueryable<Message> ReceivedMessages(string username);
        IQueryable<Message> SentMessages(string name);
        IQueryable<Message> ReceivedByAuthor(string id, string id2);
        Task<Message> getMessage(int? id);
        Task SignAsRead(Message message);
        IQueryable<Message> NewMessages(string username);
    }
}
