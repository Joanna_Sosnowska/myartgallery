﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Web.Mvc;
using System.Web.Helpers;
using System.Net;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using MyArtGallery.Models;

namespace MyArtGallery.Repositories
{
    public class AlbumRepository:IAlbumRepository
    {
        MyArtGalleryContext db = new MyArtGalleryContext();
        public UserManager<UserProfile> UserManager { get; private set; }
        public AlbumRepository(UserManager<UserProfile> userManager)
        {
            UserManager = userManager;
        }

        public AlbumRepository()
            : this(new UserManager<UserProfile>(new UserStore<UserProfile>(new MyArtGalleryContext())))
        {
        }
        public AlbumRepository(MyArtGalleryContext ctx)
        {
            this.db = ctx;
            this.UserManager = new UserManager<UserProfile>(new UserStore<UserProfile>(db));
        }
        public async Task<Album> getAlbum(int id)
        {
            return await db.Albums.FindAsync(id);
        }

        public Album getAlbum(string path)
        {
            return db.Albums.Where(a => a.Path.Equals(path)).FirstOrDefault();
        }

        public async Task AddAlbum(int parentid, Album a)
        {
            //UserProfile user = await UserManager.FindByNameAsync(username);
            Album parent = await db.Albums.FindAsync(parentid);
            //a.UserProfileId = username;
            string title = a.Title;
            //parametry nie ustawiane przez użytkownika
            a.Pictures = new List<Picture> { };
            a.SubAlbums = new List<Album> { };
            a.AddedDate = DateTime.Now;
            a.LastModifiedDate = DateTime.Now;
            a.CoverUrl = "~/Content/Images/defaulticon.png";
            if (parent != null)
            {
                a.Path = parent.Path + "/" + title;
                a.ParentId = parentid;
                db.Albums.Add(a);
            }
            else
                db.Albums.Add(a);
       
            await db.SaveChangesAsync();


        }

        public async Task UpdateAlbum(Album a)
        {
            a.LastModifiedDate = DateTime.Now;
            db.Entry(a).State = EntityState.Modified;
            await db.SaveChangesAsync();
        }

        public async Task DeleteAlbum(Album a)
        {
            db.Albums.Remove(await getAlbum(a.AlbumId));
            db.SaveChanges();
        }

        public IQueryable<Album> albumList(string username)
        {
            return db.Albums.Where(a => a.UserProfile.UserName == username);
        }

        public IQueryable<Album> albumList()
        {
            return db.Albums;
        }

        public IEnumerable<Picture> galleryPictures(string gallerypath)
        {
            return db.Pictures.Where(p => p.Album.Path == gallerypath);
        }

        public IEnumerable<SelectListItem> SelectAlbumsExcluding(string path, string username)
        {
            var albums = albumList(username).Except(new List<Album> { getAlbum(path) })
                        .Select(x =>
                                new SelectListItem
                                {
                                    Value = x.AlbumId.ToString(),
                                    Text = x.Path.ToString()
                                });

            return new SelectList(albums, "Value", "Text");
        }
        public IEnumerable<SelectListItem> SelectAlbumContents(string path)
        {
            Album a = getAlbum(path);
            var imgs = a.Pictures
                        .Select(x =>
                                new SelectListItem
                                {
                                    Value = x.PictureId.ToString(),
                                    Text = x.Title
                                });

            return new SelectList(imgs, "Value", "Text");
        }

        public void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
        }
    }
}