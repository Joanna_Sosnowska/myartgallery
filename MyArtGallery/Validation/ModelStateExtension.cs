﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MyArtGallery.Validation
{
    public class ModelStateExtension:IValidate
    {
        private ModelStateDictionary modelState;

        public ModelStateExtension(ModelStateDictionary msd)
        {
           modelState = msd;
        }

        public void AddError(string key, string errorMessage)
        {
            modelState.AddModelError(key, errorMessage);
        }

        public bool IsValid
        {

            get { return modelState.IsValid; }
        }
    }
}