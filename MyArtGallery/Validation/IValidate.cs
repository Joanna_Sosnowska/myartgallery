﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyArtGallery.Validation
{
    public interface IValidate
    {
        void AddError(string key, string errorMessage);
        bool IsValid { get; }
    }
}
