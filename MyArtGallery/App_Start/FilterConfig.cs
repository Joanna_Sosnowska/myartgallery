﻿using MyArtGallery.Models;
using MyArtGallery.Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Filters;
using System.Web.Routing;

namespace MyArtGallery.App_Start
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
            filters.Add(new MyAuthenticationFilter());
            filters.Add(new RoleAuthorize());
            filters.Add(new CheckIfBanned());
        }
    }
    public class MyAuthenticationFilter: FilterAttribute,
    IAuthenticationFilter
    {
        public void OnAuthentication(AuthenticationContext filterContext)
        {
            IIdentity ident = filterContext.Principal.Identity;
            if (!ident.IsAuthenticated)
            {
                filterContext.Result = new RedirectToRouteResult(
                    new RouteValueDictionary {
                {"controller", "UserProfile"},
                {"action",  "Login"},
                {"url", filterContext.HttpContext.Request.RawUrl},
                {"message", Errors.LoginRequired}
            });
            }

        }

        public void OnAuthenticationChallenge(AuthenticationChallengeContext
            filterContext)
        {
            if (filterContext.Result == null || filterContext.Result
                is HttpUnauthorizedResult)
            {
                filterContext.Result = new RedirectToRouteResult(
                    new RouteValueDictionary {
                {"controller", "UserProfile"},
                {"action",  "Login"},
                {"url", filterContext.HttpContext.Request.RawUrl},
                {"message", Errors.LoginRequired}
            });
            }
        }
    }

    public class RoleAuthorize : AuthorizeAttribute
    {
        MyArtGalleryContext context = new MyArtGalleryContext(); 
        private readonly string[] roles;
        public RoleAuthorize(params string[] roles)
        {
            this.roles = roles;
        }

        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            bool authorize = false;
            foreach (var role in roles)
            {
                var user = context.Users.Where(m => m.UserName.Equals(httpContext.User.Identity.Name) && m.Rank.ToString().Equals(role)); 
                if (user.Count() > 0)
                {
                    authorize = true;
                }
            }
            return authorize;
        }
        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            //moja aplikacja posiada tylko 3 zastrzeżenia co do roli            
            if (roles.Contains("Admin") && roles.Contains("Moderator"))
            {
                filterContext.Result = new RedirectToRouteResult(
                  new RouteValueDictionary {
                {"controller", "Home"},
                {"action",  "Error"},
                {"message", Errors.AdminOrMod}
            });
            }
            else
            {
                filterContext.Result = new RedirectToRouteResult(
                        new RouteValueDictionary {
                {"controller", "Home"},
                {"action",  "Error"},
                {"message", Errors.AdminOnly}
            });
            }
        } 

       
    }
    public class CheckIfBanned : AuthorizeAttribute
    {
        MyArtGalleryContext context = new MyArtGalleryContext();
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            bool authorize = true;
                var user = context.Users.Where(m => m.UserName.Equals(httpContext.User.Identity.Name) && m.Rank.ToString().Equals("Banned"));
                if (user.Count() > 0)
                {
                    authorize = false;
                }
            return authorize;
        }
        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
                filterContext.Result = new RedirectToRouteResult(
                 new RouteValueDictionary {
                {"controller", "Home"},
                {"action",  "Error"},
                {"message", Errors.BannedUser}
            });
        }


    }  
}