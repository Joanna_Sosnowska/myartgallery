﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Optimization;

namespace MyArtGallery
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
            "~/Scripts/jquery*",
            "~/Scripts/navigation_enhancements.js"
            ));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
            "~/Scripts/jquery.validate*"));

            bundles.Add(new ScriptBundle("~/bundles/formsubmit").Include(
           "~/Scripts/formsubmit.js"));
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
            "~/Scripts/modernizr-*"));


            bundles.Add(new ScriptBundle("~/bundles/jqueryui").Include(
           "~/Scripts/jquery-ui-1.11.1.js",
           "~/Scripts/jquery-ui.unobtrusive-1.11.1.js"));


            bundles.Add(new ScriptBundle("~/bundles/imageeditor").Include(
            "~/Scripts/vanishingfields.js",
            "~/Scripts/jQueryTransition.js",
            "~/Scripts/editionpreview.js"));

            bundles.Add(new ScriptBundle("~/bundles/imageadder-button").Include(
           "~/Scripts/enablebutton.add.js"));

            bundles.Add(new ScriptBundle("~/bundles/imageeditor-button").Include(
          "~/Scripts/enablebutton.edit.js"));

            bundles.Add(new ScriptBundle("~/bundles/formsactions").Include(
            "~/Scripts/partialforms.js",
            "~/Scripts/pickdate.js"));

            bundles.Add(new ScriptBundle("~/bundles/partialsub").Include(
            "~/Scripts/partialsubmit.js"));

            bundles.Add(new ScriptBundle("~/bundles/htmlcomments").Include(
            "~/Scripts/htmlcomments.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
            "~/Content/bootstrap-*",
            "~/Content/jquery.fancy*",
            "~/Content/Site.css",
            "~/Content/PagedList.css", 
            "~/Content/imgareaselect-*"
            ));

            bundles.Add(new StyleBundle("~/Content/themes/base/css").Include(
            "~/Content/themes/base/jquery.ui.core.css",
            "~/Content/themes/base/jquery.ui.datepicker.css",
            "~/Content/themes/base/jquery.ui.theme.css")); 
        }
    }
}