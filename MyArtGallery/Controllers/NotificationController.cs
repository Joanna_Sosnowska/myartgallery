﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using MyArtGallery.Models;
using MyArtGallery.Repositories;
using MyArtGallery.Models.UserViewModels;
using MyArtGallery.Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using PagedList;
using MyArtGallery.App_Start;

namespace MyArtGallery.Controllers
{
    public class NotificationController : BasicController
    {
        private INoteRepository notificationrepo;
        public UserManager<UserProfile> UserManager { get; private set; }
        public NotificationController(UserManager<UserProfile> userManager, NoteRepository repo)
        {
            UserManager = userManager;
            notificationrepo = repo;
        }

        public NotificationController()
            : this(new UserManager<UserProfile>(new UserStore<UserProfile>(new MyArtGalleryContext())), new NoteRepository())
        {
        }

        [MyAuthenticationFilter]
        public ActionResult UserNotifications(int? page)
        {
            IQueryable<Notification> notes = notificationrepo.ReceivedNotes(User.Identity.Name).AsQueryable().OrderByDescending(n=>n.SentDate);
            int pagenumber = (page ?? 1);
            return View(notes.ToPagedList(pagenumber, 20));
        }

        [MyAuthenticationFilter]
        public ActionResult ChooseType()
        {
            MessageTypeViewModel model = new MessageTypeViewModel();
            
            return PartialView(model);
        }

        [MyAuthenticationFilter]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult NotesByType(MessageTypeViewModel model)
        {
            List<Notification> notes = notificationrepo.ReceivedByType(User.Identity.Name, (int)model.Type).ToList();
            return View(notes);
        }

        [MyAuthenticationFilter]
        public ActionResult NotesByAuthor(string id)
        {
            List<Notification> notes = notificationrepo.ReceivedByAuthor(User.Identity.Name, id).ToList();

            return View(notes);
        }

        [MyAuthenticationFilter]
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
                return RedirectToAction("Error", "Home", new { message=Errors.MessageNotFound});
            Notification note = await notificationrepo.getNotification((int)id);
            if (note==null)
                return RedirectToAction("Error", "Home", new { message = Errors.MessageNotFound });
            if (!note.AddresseeId.Equals(User.Identity.Name))
                return RedirectToAction("Error", "Home", new { message = Errors.PrivacyViolation });
            await notificationrepo.SignAsRead(note);
           
           
            return View(note);
        }

        
	}
}