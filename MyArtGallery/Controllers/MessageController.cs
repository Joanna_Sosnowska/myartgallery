﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MyArtGallery.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using MyArtGallery.Resources;
using MyArtGallery.Repositories;
using System.Threading.Tasks;
using PagedList;
using MyArtGallery.App_Start;

namespace MyArtGallery.Controllers
{
    public class MessageController : BasicController
    {
        private IMessageRepository messagerepo;
        public UserManager<UserProfile> UserManager { get; private set; }
        public MessageController(UserManager<UserProfile> userManager, MessageRepository repo)
        {
            UserManager = userManager;
            messagerepo = repo;
        }

        public MessageController()
            : this(new UserManager<UserProfile>(new UserStore<UserProfile>(new MyArtGalleryContext())), new MessageRepository())
        {
        }

        [MyAuthenticationFilter]
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
                return RedirectToAction("Error", "Home", new { message = Errors.MessageNotFound });

            Message message = await messagerepo.getMessage(id);
            if (message == null)
                return RedirectToAction("Error", "Home", new { message = Errors.MessageNotFound });

            if (message.Addressee.UserName.Equals(User.Identity.Name))
            {
                if (!message.WasRead)
                {
                    await messagerepo.SignAsRead(message);

                }
                return View(message);
            }
            try
            {
                if (message.Author.UserName.Equals(User.Identity.Name))
                    return View(message);
            }
            catch
            {
            }
            return RedirectToAction("Error", "Home", new { message = Errors.PrivacyViolation });


        }

        [MyAuthenticationFilter]
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
                return RedirectToAction("Error", "Home", new { message = Errors.MessageNotFound });

            Message message = await messagerepo.getMessage(id);
            if (message == null)
                return RedirectToAction("Error", "Home", new { message = Errors.MessageNotFound });

            return View(message);
        }

       [MyAuthenticationFilter]
        public ActionResult Send(string id)
        {
            Message model = new Message();
            UserProfile user = UserManager.FindByName(id);
            string name = User.Identity.GetUserName();

            if (user == null)
                return RedirectToAction("Error", "Home", new { message = Errors.UserNotFound });

            model.AddresseeId = user.UserName;
            model.AuthorId = name;
            model.SentDate = DateTime.Now;

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Send(Message model)
        {

            Message message = new Message {AuthorId = model.AuthorId, AddresseeId = model.AddresseeId, SentDate = model.SentDate, Text = model.Text, Title = model.Title };
          
            if (ModelState.IsValid)
            {
                
                await messagerepo.AddMessage(message);
                return RedirectToAction("Index", "Home");
            }

            return View(message);
        }

        [MyAuthenticationFilter]
        [HttpGet]
        public async Task<ActionResult> Reply(int id)
        {
            Message parent = await messagerepo.getMessage(id);
            if (parent == null)
                return RedirectToAction("Error", "Home", new { message = Errors.MessageNotFound });
            string name = User.Identity.GetUserName();
            UserProfile user = await UserManager.FindByNameAsync(parent.AuthorId);
            if (user == null)
                return RedirectToAction("Error", "Home", new { message = Errors.MessageNotFound });
            Message model = new Message();
            model.AuthorId = name;
            model.AddresseeId = parent.AuthorId;
            model.ParentId = parent.MessageId;
            model.Parent = parent;
            model.SentDate = DateTime.Now;

                 return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Reply(Message model)
        {
         
            Message message = new Message { AuthorId = model.AuthorId, AddresseeId = model.AddresseeId, ParentId=model.ParentId, SentDate = model.SentDate, Text = model.Text, Title = model.Title };
            Message parent = await messagerepo.getMessage(model.ParentId);
            string text = Resource.ReplyFor + parent.Title + " " + parent.SentDate.ToString()+". ";
            message.Text = text + model.Text;
            if (ModelState.IsValid)
            {

                await messagerepo.AddMessage(message);
                return RedirectToAction("SentList", "Message");
            }

            return View(model);
        }

        [MyAuthenticationFilter]
        public ActionResult SentList(int? page)
        {
            IQueryable<Message> messages = messagerepo.SentMessages(User.Identity.Name).AsQueryable().OrderByDescending(m => m.SentDate);
            int pagenumber = (page ?? 1);
            return View(messages.ToPagedList(pagenumber, 25));
        }

        [MyAuthenticationFilter]
        public ActionResult ReceivedList(int? page)
        {
            IQueryable<Message> messages = messagerepo.ReceivedMessages(User.Identity.Name).AsQueryable().OrderByDescending(m => m.SentDate);
            int pagenumber = (page ?? 1);
            return View(messages.ToPagedList(pagenumber,25));
        }


    
    }
}
