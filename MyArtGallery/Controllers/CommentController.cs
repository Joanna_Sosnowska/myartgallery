﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MyArtGallery.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using MyArtGallery.Repositories;
using System.Threading.Tasks;
using MyArtGallery.Models.GalleryViewModels;
using MyArtGallery.Resources;
using PagedList;
using MyArtGallery.App_Start;

namespace MyArtGallery.Controllers
{
    public class CommentController : BasicController
    {
        private MyArtGalleryContext context;
        private ICommentRepository commentrepo;
        private INoteRepository noterepository;
        public UserManager<UserProfile> UserManager { get; private set; }
        public CommentController()
        {
            this.context = new MyArtGalleryContext();
            UserManager = new UserManager<UserProfile>(new UserStore<UserProfile>(context));
            commentrepo = new CommentRepository(context);
            noterepository = new NoteRepository(context);
        }

        public ActionResult ListByAuthor(string username, int? page)
        {
            IQueryable<Comment> comments = commentrepo.ListComments(username).OrderByDescending(c=>c.AddedDate);
            int pagenumber = (page ?? 1);
            ViewBag.Author = username;
            ViewBag.CanEdit = false;
            UserProfile user = UserManager.FindByName(User.Identity.Name);
            if (!UserManager.IsInRole(user.Id, "Banned") && (user.UserName).Equals(username))
                ViewBag.CanEdit = true;
            else if(UserManager.IsInRole(user.Id, "Admin") || UserManager.IsInRole(user.Id, "Moderator"))
                ViewBag.CanEdit = true;
            return View(comments.ToPagedList(pagenumber, 25));
        }

        public ActionResult ListByPicture(int id, int? page)
        {
            IQueryable<Comment> comments = commentrepo.ListComments(id);
            ViewBag.CanEdit = false;
            UserProfile user = UserManager.FindByName(User.Identity.Name);
            if (UserManager.IsInRole(user.Id, "Admin") || UserManager.IsInRole(user.Id, "Moderator"))
                ViewBag.CanEdit = true;
            int pagenumber = (page ?? 1);
            ViewBag.PictureId = id;
            return View(comments.ToPagedList(pagenumber, 10));
        }

        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
                return RedirectToAction("Error", "Home", new ErrorViewModel { Message = Errors.CommentNotFound });
           
            Comment comment = await commentrepo.getCommentAsync((int)id);
            if (comment == null)
                return RedirectToAction("Error", "Home", new ErrorViewModel { Message=Errors.CommentNotFound});
            Comment parent = null;
            if(comment.ParentId!=null)
                parent = await commentrepo.getCommentAsync((int)comment.ParentId);

            ViewBag.Parent = false;
            if (parent != null)
                ViewBag.Parent = true;
            ViewBag.CanEdit = false;
            UserProfile user = await UserManager.FindByNameAsync(User.Identity.Name);
            if (UserManager.IsInRole(user.Id, "Admin") || UserManager.IsInRole(user.Id, "Moderator") || comment.UserName.Equals(User.Identity.Name))
                ViewBag.CanEdit = true;
            //if (user.Rank.Equals(Rank.Admin) || user.Rank.Equals(Rank.Moderator) || comment.UserName.Equals(User.Identity.Name))
            //    ViewBag.CanEdit = true;
            return View(comment);
        }
        
        [MyAuthenticationFilter]
        public ActionResult Comment(int? id)
        {
            if (id == null)
                return PartialView("Confirm", new ActionConfirmViewModel { Message = Errors.PictureNotFound, Success = false });
            UserProfile user = UserManager.FindByName(User.Identity.Name);
            if (UserManager.IsInRole(user.Id, "Banned"))
                return PartialView("Confirm", new ActionConfirmViewModel { Message = Errors.BannedUser, Success = false });
            AddCommentViewModel model = new AddCommentViewModel();
            model.PictureId = (int) id;
            model.UserProfileId = User.Identity.Name;
         
            return PartialView(model);
        }

        [HttpPost]
        public async Task<ActionResult> Comment(AddCommentViewModel model)
        {
            if (ModelState.IsValid)
            {
                Comment comment = new Comment { Text = model.Text, PictureId = model.PictureId, UserName = model.UserProfileId };
                if (await commentrepo.AddComment(comment) != null)
                {
                    Picture pic = await (new PictureRepository()).getPictureAsync(model.PictureId);
                    if (!(pic.UserName).Equals(User.Identity.Name))
                    {
                        //nie dostajesz powiadomienia, jak komentujesz swoje obrazy
                        Notification note = new Notification { ThingId = model.PictureId, UserId = User.Identity.Name, AddresseeId = pic.UserName, SentDate = DateTime.Now, Kind = NoteType.Comment, Text = User.Identity.Name + " " + MessagesAndNotes.CommentNote, WasRead = false };
                        await noterepository.AddNote(note);
                    }
                    return PartialView("Confirm", new ActionConfirmViewModel { Message = CommentOptions.CommentAdded, Success = true });
           
                }
            }

            return PartialView(model);
        }

        [MyAuthenticationFilter]
        public ActionResult Reply(int? id)
        {
            if(id==null)
                return PartialView("Confirm", new ActionConfirmViewModel { Message = Errors.CommentNotFound, Success = false });
            UserProfile user = UserManager.FindByName(User.Identity.Name);
            if (UserManager.IsInRole(user.Id, "Banned"))
                return PartialView("Confirm", new ActionConfirmViewModel { Message = Errors.BannedUser, Success = false });
           
            AddCommentViewModel model = new AddCommentViewModel();
            Comment parent = commentrepo.getComment((int)id);
            if (parent == null)
                return PartialView("Confirm", new ActionConfirmViewModel { Message = Errors.CommentNotFound, Success = false });
            model.ParentId = parent.CommentId;
            model.PictureId = parent.PictureId;
            model.UserProfileId = User.Identity.Name;
            ViewBag.Parent = parent;
            return PartialView(model);
        }

        [HttpPost]
        public async Task<ActionResult> Reply(AddCommentViewModel model)
        {
            Comment parent = commentrepo.getComment(model.ParentId);
            if (ModelState.IsValid)
            {
                Comment comment = new Comment { Text=model.Text, UserName=model.UserProfileId, ParentId=model.ParentId, PictureId=model.PictureId};
                await commentrepo.AddComment(comment);
                if (!(parent.Picture.UserName).Equals(User.Identity.Name))
                {
                    Notification note = new Notification { ThingId = model.ParentId, UserId = model.UserProfileId, AddresseeId = parent.UserName, SentDate = DateTime.Now, Kind = NoteType.CommentReply, Text = model.UserProfileId + " " + MessagesAndNotes.ReplyNote, WasRead = false };
                    await noterepository.AddNote(note);
                }
                return PartialView("Confirm", new ActionConfirmViewModel { Message = CommentOptions.CommentAdded, Success = true });
           
            }
            ViewBag.Parent = parent;
            return PartialView(model);
        }

        // GET: /Comment/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            
            if (id == null)
                return PartialView("Confirm", new ActionConfirmViewModel { Message = Errors.CommentNotFound, Success = false });
            Comment comment = commentrepo.getComment((int)id);
            if (comment == null)
                return PartialView("Confirm", new ActionConfirmViewModel { Message = Errors.CommentNotFound, Success = false });       
            UserProfile logged = await UserManager.FindByNameAsync(User.Identity.Name);
            if (!(comment.UserName).Equals(User.Identity.Name) && UserManager.IsInRole(logged.Id, "User"))
                return PartialView("Confirm", new ActionConfirmViewModel { Message = Errors.PrivacyViolation, Success = false });
            if (UserManager.IsInRole(logged.Id, "Banned"))
                return PartialView("Confirm", new ActionConfirmViewModel { Message = Errors.BannedUser, Success = false });
           
            return PartialView(comment);
        }


        [HttpPost]
        public async Task<ActionResult> Edit(Comment model)
        {
            if (ModelState.IsValid)
            {
                Comment comment = await commentrepo.getCommentAsync(model.CommentId);
                comment.Text = model.Text;
                await commentrepo.EditComment(comment);
                if (!(User.Identity.Name).Equals(comment.UserName))
                {
                    Notification note = new Notification { AddresseeId = comment.UserName, SentDate = DateTime.Now, WasRead = false, Kind = NoteType.EditedComment, UserId = User.Identity.Name, Text = MessagesAndNotes.EditedCommentNote, ThingId = comment.CommentId };
                    await noterepository.AddNote(note);
                }
                return PartialView("Confirm", new ActionConfirmViewModel { Message = CommentOptions.CommentEdited, Success = true });
           
            }
            return PartialView(model);
        }


        // GET: /Comment/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
                return PartialView("Confirm", new ActionConfirmViewModel { Message = Errors.CommentNotFound, Success = false });
            Comment comment = commentrepo.getComment((int)id);
            if (comment == null)
                return PartialView("Confirm", new ActionConfirmViewModel { Message = Errors.CommentNotFound, Success = false });
            UserProfile logged = UserManager.FindByName(User.Identity.Name);
            if (!(comment.UserName).Equals(User.Identity.Name) && UserManager.IsInRole(logged.Id, "User"))
                return PartialView("Confirm", new ActionConfirmViewModel { Message = Errors.PrivacyViolation, Success = false });
            RemoveCommentViewModel model = new RemoveCommentViewModel();
            model.CommentId = (int)id;
            model.Comm = commentrepo.getComment((int)id);
            return PartialView(model);
        }

        // POST: /Comment/Delete/5
        [HttpPost]
        public async Task<ActionResult> Delete(RemoveCommentViewModel model)
        {
            Comment comment = await commentrepo.getCommentAsync(model.CommentId);       
            await commentrepo.RemoveComment(comment);
            if (!(User.Identity.Name).Equals(comment.UserName))
            {
                Notification note = new Notification { AddresseeId = comment.UserName, SentDate = DateTime.Now, WasRead = false, Kind = NoteType.DeletedComment, UserId = User.Identity.Name, Text = MessagesAndNotes.DeletedCommentNote, ThingId = comment.CommentId };
                await noterepository.AddNote(note);
            }
            return PartialView("Confirm", new ActionConfirmViewModel { Message = CommentOptions.CommentDeleted, Success = true });
        }

        [MyAuthenticationFilter]
        [HttpPost]
        public ActionResult Mark(int? id, string rate)
        {
            UserProfile user = UserManager.FindByName(User.Identity.Name);
            Comment comment = commentrepo.getComment((int)id);
            if (rate.Equals("+"))
                comment.Good += 1;
            else if(rate.Equals("-"))
                comment.Bad += 1;
            commentrepo.EditComment(comment);
            if (!(comment.UserName).Equals(User.Identity.Name))
            {
                //nie dostajesz powiadomienia, jak oceniasz swoje obrazy
                Notification note = new Notification { ThingId = comment.CommentId, UserId = User.Identity.Name, AddresseeId = comment.UserName, SentDate = DateTime.Now, Kind = NoteType.CommentRated, Text = User.Identity.Name + " " + MessagesAndNotes.CommentRateNote, WasRead = false };
                noterepository.AddNote(note);
            }
            return PartialView("Confirm", new ActionConfirmViewModel { Message = CommentOptions.CommentRated, Success = true });
        }

        public ActionResult Confirm(ActionConfirmViewModel model)
        {

            return View(model);
        }
    }
}
