﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MyArtGallery.Models;
using Microsoft.AspNet.Identity;
using MyArtGallery.Repositories;
using Microsoft.AspNet.Identity.EntityFramework;
using MyArtGallery.Services;
using MyArtGallery.Validation;
using System.Threading.Tasks;
using MyArtGallery.Models.GalleryViewModels;
using System.Web.Helpers;
using System.Drawing;
using MyArtGallery.Models.Helpers;
using MyArtGallery.Resources;
using System.Text;
using MyArtGallery.Helpers;
using PagedList;
using System.IO;
using System.Drawing.Imaging;
using MyArtGallery.App_Start;

namespace MyArtGallery.Controllers
{
    public class PictureController : BasicController
    {
        private IImageService imageservice;
        public PictureController()
        {
            imageservice = new ImageService(new ModelStateExtension(this.ModelState));
        }


        public PictureController(IImageService s)
        {
            imageservice = s;
        }
        // GET: /Picture/
        public async Task<ActionResult> Index()
        {
            var pictures = imageservice.Pictures();
            return View(await pictures.ToListAsync());
        }

        public ActionResult ListByAuthor(string id, int? page)
        {
            var pictures = imageservice.Pictures(id).OrderByDescending(p=>p.LastModifiedDate);
            ViewBag.Author = id;

            int pagenumber = (page ?? 1);
            return View(pictures.ToPagedList(pagenumber,20));
        }
        public ActionResult Featured(string id)
        {
            IQueryable<Picture> model = imageservice.Pictures(id, 9);
            return View(model.ToList());
        }
        // GET: /Picture/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
                return RedirectToAction("Error", "Home", new { message = Errors.PictureNotFound });
            Picture picture = await imageservice.GetPictureAsync((int)id);
            if (picture == null)
                return RedirectToAction("Error", "Home", new { message = Errors.PictureNotFound });
            if ((!picture.Public || picture.Adult) && !Request.LogonUserIdentity.IsAuthenticated)
                return RedirectToAction("Login", "UserProfile", new { message = Errors.LoginRequired });
            UserProfile user = await imageservice.GetUser(User.Identity.Name);
            ViewBag.CanEdit = false;
            bool admin=await imageservice.CheckRole(user.Id, "Admin");
            bool mod = await imageservice.CheckRole(user.Id, "Moderator");
            if (admin|| mod || picture.UserName.Equals(User.Identity.Name))
                ViewBag.CanEdit = true;
            
            if(picture.Adult && UserProfileHelper.CountUserAge(user.BirthDate)<18)
                return RedirectToAction("Error", "Home", new { message = Errors.TooYoung });
            string lang = RouteData.Values["culture"] as string;
            ViewBag.Language = lang;
            return View(picture);
        }

        public async Task<ActionResult> FullSize(int? id)
        {
            if (id == null)
                return RedirectToAction("Error", "Home", new { message = Errors.PictureNotFound });
            Picture picture = await imageservice.GetPictureAsync((int)id);
            if (picture == null)
                return RedirectToAction("Error", "Home", new { message = Errors.PictureNotFound });
            if ((!picture.Public || picture.Adult) && !Request.LogonUserIdentity.IsAuthenticated)
                return RedirectToAction("Login", "UserProfile", new { message = Errors.LoginRequired });
            UserProfile user = await imageservice.GetUser(User.Identity.Name);
            if (picture.Adult && UserProfileHelper.CountUserAge(user.BirthDate) < 18)
                return RedirectToAction("Error", "Home", new { message = Errors.TooYoung });
            return View(picture);
        }

        [MyAuthenticationFilter]
        public async Task<ActionResult> Favourite(int? id)
        {
            if (id == null)
                return PartialView("Confirm", new ActionConfirmViewModel { Message = Errors.PictureNotFound, Success = false });
            Picture picture = imageservice.GetPicture((int)id);
            if (picture == null)
                return PartialView("Confirm", new ActionConfirmViewModel { Message = Errors.PictureNotFound, Success = false });
            string name = User.Identity.Name;
            UserProfile user = await imageservice.GetUser(name);
            
            if (user.Favourites.AsQueryable().Where(f=>f.PictureId==picture.PictureId).Count()==0)
            {
                Favourite fav = new Favourite { UserName = name, PictureId = picture.PictureId };

                picture.Favourites += 1;
                await imageservice.EditPictureInfo(picture);
                await imageservice.AddFavourite(fav);
            }
            //jak obraz jest już w twoich ulubionych - nic się nie dzieje
            Notification note = new Notification { ThingId = picture.PictureId, UserId = user.UserName, AddresseeId = picture.UserName, SentDate = DateTime.Now, Kind = NoteType.Favourited, Text = user.UserName + " " + MessagesAndNotes.FavouritedNote, WasRead = false };
            await imageservice.AddNote(note);
            return PartialView("Confirm", new ActionConfirmViewModel {  Message=MessagesAndNotes.YouFavouritedNote, Success=true});
        }
        
        public ActionResult Favourites(int? page, int? id)
        {
            if (id == null)
                return RedirectToAction("Error", new {  message = Errors.PictureNotFound });
            Picture picture = imageservice.GetPicture((int)id);
            if (picture == null)
                return RedirectToAction("Error", new { message = Errors.PictureNotFound });
            ViewBag.PictureName = picture.Title;
            ViewBag.PictureId = picture.PictureId;
            ViewBag.Author = picture.UserName;
            int pagenumber = (page ?? 1);
            IQueryable<Favourite> favs = picture.Favourited.AsQueryable().OrderByDescending(f => f.FavouriteId);

            return View(favs.ToPagedList(pagenumber, 25));
        }
        public async Task<ActionResult> DeleteFav(int? id)
        {
            if (id == null)
                return RedirectToAction("Error", "Home", new { message = Errors.PictureNotFound });
            
            await imageservice.DeleteFav((int)id);
            return RedirectToAction("Favourites", "UserProfile", new { id = User.Identity.Name });

        }
        public ActionResult Download(int id)
        {
            DownloadViewModel model = new DownloadViewModel();
            model.Id = id;
            Dictionary<int,string> sizes = new Dictionary<int,string>();
            Picture picture = imageservice.GetPicture(id);
            sizes.Add(1, ImageOptions.FullSize);
            if (picture.Height > 800 || picture.Width > 800)
            {
                sizes.Add(2, ImageOptions.MediumSize);
            }
            sizes.Add(3, ImageOptions.Thumbnail);
            
            model.Sizes = ImageHelper.Sizes(sizes);
            return View(model);
        }

        [MyAuthenticationFilter]
        [HttpPost]
        public async Task<FileResult> Download(DownloadViewModel model)
        {
            Picture picture = await imageservice.GetPictureAsync(model.Id);
            DeleteTemps(User.Identity.Name);
            string p;
            if (model.Size == 1)
                p = picture.FullSizeUrl;
            else if (model.Size == 3)
                p = picture.ThumbnailUrl;
            else
                p = picture.Url;
            picture.Downloads += 1;
            string tempPath="";
            await imageservice.EditPictureInfo(picture);
            if(ImageHelper.GetFileType(picture.FullSizeUrl).ToLower().Equals(".gif")){
                return File(p, "image/gif", picture.Title + ".gif");
            }
            else{
            tempPath =  ImageHelper.TemporaryPath(User.Identity.Name, ImageHelper.GetFileType(picture.FullSizeUrl));
            Image img = Image.FromFile(Request.MapPath(p));
            img.Save(Request.MapPath(tempPath));
            string downloadPath = ImageHelper.SetImageProperties(picture, Request.MapPath(tempPath), Request.MapPath(ImageHelper.TemporaryPath(User.Identity.Name, ImageHelper.GetFileType(tempPath))));
            return File(downloadPath, "image/jpeg", picture.Title + ".jpg");
            }
        } 

        //dodajemy do albumu
        [MyAuthenticationFilter]
        [CheckIfBanned]
        public async Task<ActionResult> Add(int id)
        {
            
            Album a = await imageservice.GetAlbum(id);
            if (a==null)
                return RedirectToAction("Error", "Home", new { message = Errors.AlbumNotFound });
            DeleteTemps(User.Identity.Name);
            AddPictureViewModel model = new AddPictureViewModel();
            model.AlbumId = id;
            model.UserProfileId = User.Identity.Name;
            string culture = RouteData.Values["culture"] as string;
            model.Categories = imageservice.CategoriesSelect(culture);
            return View(model);
        }

    
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Add(AddPictureViewModel model)
        {
            string[] paths = new string[3];
            CreateFolder(User.Identity.Name);
            string proto_path = @"~/Images/" + User.Identity.Name + "/Temp/";
            Image img = null;
            string ext = "";
            bool validType = false;
            if (model.IsUrl)
            {
                if (model.Url != null)
                {
                    validType = imageservice.UrlValidation(model.Url);
                    if (validType)
                    {
                        ext = ImageHelper.GetFileType(model.Url);
                        proto_path += "proto" + ext;
                        ImageHelper.SaveImageProto(model.Url, Request.MapPath(proto_path));
                    }
                }
                else
                {
                    validType = imageservice.UrlValidation("");
                }
            }
            else
            {
                if (model.File != null)
                {
                    string filename = model.File.FileName;
                    validType = imageservice.FileValidation(filename);
                    if (validType)
                    {
                        ext = Path.GetExtension(filename);
                        proto_path += "proto"+ext;
                        ImageHelper.SaveImageProto(model.File, Request.MapPath(proto_path));
                    }
                }
                else
                {
                    validType = imageservice.FileValidation("");
                }
            }
            try
            {
                img = Image.FromFile(Request.MapPath(proto_path));
            }
            catch
            {
                img = null;

            }
            if (ModelState.IsValid)
            {
                Picture picture = new Picture { Title = model.Title, CategoryId = model.CategoryId, Description = model.Description, Public = model.Public, Adult = model.Adult, CommentsOff=model.CommentsOff, UserName = model.UserProfileId, AlbumId = model.AlbumId};
                picture.Tags = new List<Tag> { };
                picture.AdditionDate = DateTime.Now;
                picture.LastModifiedDate = DateTime.Now;

                if (model.Tags!=null)
                {
                    List<Tag> ts = await imageservice.AddTags((model.Tags).Trim(' '));
                    
                    foreach (Tag item in ts)
                    {
                        picture.Tags.Add(item);
                    }
                }
                
                if (img != null)
                {
                    Bitmap img0 = ImageHelper.CreateImage(img, model.X, model.Y, model.Width, model.Height, model.TurnAngle, model.MirrorHorizontal, model.MirrorVertical);                   
                    Bitmap img1 = new Bitmap(800, 800);
                    Bitmap thumb = new Bitmap(150, 150);
                    double h = img0.Size.Height;
                    double w = img0.Size.Width;
                    double wh_ratio, hw_ratio;
                    wh_ratio = w / h;
                    hw_ratio = h / w;
                    if (hw_ratio < 1 && w>=800)
                    {
                        img1 = ImageHelper.ResizeImage(img0, 800, 800 / wh_ratio);
                        thumb = ImageHelper.ResizeImage(img0, 150, 150 / wh_ratio);
                    }
                    else if (hw_ratio < 1 && w < 800)
                    {
                        img1 = ImageHelper.ResizeImage(img0, w, w / wh_ratio);
                        thumb = ImageHelper.ResizeImage(img0, 150, 150 / wh_ratio);
                    }
                    else if (hw_ratio > 1 && h >= 800)
                    {
                        img1 = ImageHelper.ResizeImage(img0, 800 / hw_ratio, 800);
                        thumb = ImageHelper.ResizeImage(img0, 150 / hw_ratio, 150);

                    }
                    else
                    {
                        img1 = ImageHelper.ResizeImage(img0, h / hw_ratio, h);
                        thumb = ImageHelper.ResizeImage(img0, 150 / hw_ratio, 150);
                    }
                    paths = ImageHelper.MapImagePaths(picture, User.Identity.Name, ext.ToLower());
                    if (ext.ToLower().Equals(".gif") && model.Animated)
                    {
                        img.Save(Request.MapPath(paths[0]), ImageFormat.Gif);
                        img.Save(Request.MapPath(paths[1]), ImageFormat.Gif);
                        thumb.Save(Request.MapPath(paths[2]), ImageFormat.Png);
                        picture.FileSize = ImageHelper.GetFileSize(img);
                    }
                    else
                    {
                        ImageHelper.GetImageProperties(Request.MapPath(proto_path), picture);
                        ImageHelper.GetImageResolution(img0, picture);
                        img0.Save(Request.MapPath(paths[0]));
                        img1.Save(Request.MapPath(paths[1]));
                        thumb.Save(Request.MapPath(paths[2]));
                        picture.FileSize = ImageHelper.GetFileSize(img0);
                    }
                    img.Dispose();
                    img0.Dispose();
                    img1.Dispose();
                    thumb.Dispose();
                    bool res = await imageservice.UploadPicture(picture, model.UserProfileId);
                    Album a = await imageservice.GetAlbum(model.AlbumId);
                    ImageHelper.DeleteImage(proto_path);
                    return RedirectToAction("Browse", "Album", new { path = a.Path });
                  
                }
            }
            ImageHelper.DeleteImage(proto_path);
            string culture = RouteData.Values["culture"] as string;
            model.Categories = imageservice.CategoriesSelect(culture);
            return View(model);
        }

        public ActionResult Move(int id)
        {
            MovePictureViewModel model = new MovePictureViewModel();
            model.Albums = imageservice.SelectAlbums(User.Identity.Name);
            model.Id = id;
            return View(model);
        }

        [MyAuthenticationFilter]
        [HttpPost]
        public async Task<ActionResult> Move(MovePictureViewModel model)
        {
            Picture picture = await imageservice.GetPictureAsync(model.Id);
            if (!(picture.UserName).Equals(User.Identity.Name))
                return RedirectToAction("Error", "Home", new { message = Errors.PrivacyViolation });
            Album parent = await imageservice.GetAlbum(model.AlbumId);
            if (parent==null)
                return RedirectToAction("Error", "Home", new { message = Errors.AlbumNotFound });
            
            if (ModelState.IsValid)
            {
                picture.AlbumId = model.AlbumId;
                await imageservice.EditPictureInfo(picture);
                return RedirectToAction("Browse", "Album", new { path = parent.Path });
            }
            return View(model);
        }

        [MyAuthenticationFilter]
        [CheckIfBanned]
        public async Task<ActionResult> EditImage(int id)
        {
            Picture picture = await imageservice.GetPictureAsync(id);
            if (!(picture.UserName).Equals(User.Identity.Name))
                return RedirectToAction("Error", "Home", new { message = Errors.PrivacyViolation });
            DeleteTemps(User.Identity.Name);
            ChangePictureViewModel model = new ChangePictureViewModel();
            model.PictureId = picture.PictureId;
            model.Title = picture.Title;
            model.Path = picture.FullSizeUrl;           
            return View(model);
        }


        [HttpPost]
        public async Task<ActionResult> EditImage(ChangePictureViewModel model)
        {
            CreateFolder(User.Identity.Name);
            Picture pic = await imageservice.GetPictureAsync(model.PictureId);
            Image img = null;
            string ext = "";
            bool validType = false;
            string[] paths;
            string proto_path = @"~/Images/" + User.Identity.Name + "/Temp/";
            if (model.Replace)
            {
                if (model.IsUrl)
                {
                    if (model.Url != null)
                    {
                        validType = imageservice.UrlValidation(model.Url);
                        if (validType)
                        {
                            ext = ImageHelper.GetFileType(model.Url);
                            proto_path += "proto" + ext;
                            ImageHelper.SaveImageProto(model.Url, Request.MapPath(proto_path));
                        }
                        else
                        {
                            validType = imageservice.UrlValidation("");
                        }
                    }

                }
                else
                {
                    if (model.File != null)
                    {
                        string filename = model.File.FileName;
                        validType = imageservice.FileValidation(filename);
                        if (validType)
                        {
                            ext = Path.GetExtension(filename);                      
                            proto_path += "proto" + ext;
                            ImageHelper.SaveImageProto(model.File, Request.MapPath(proto_path));
                        }
                    }
                    else
                    {
                        validType = imageservice.FileValidation("");
                    }
                }
                try
                {
                    img = Image.FromFile(Request.MapPath(proto_path));
                    
                }
                catch
                {
                    img = null;
                }
            }
            else
            {
                img = new Bitmap(Request.MapPath(pic.FullSizeUrl));
                ext = ImageHelper.GetFileType(Request.MapPath(pic.FullSizeUrl));
                validType = true;
            }
            if (ModelState.IsValid)
            {
                
                if (img != null)
                {
                    string[] oldpaths = new string[] { pic.ThumbnailUrl, pic.Url, pic.FullSizeUrl};
                    Image img0 = ImageHelper.CreateImage(img, model.X, model.Y, model.Width, model.Height, model.TurnAngle, model.MirrorHorizontal, model.MirrorVertical );
                    Image img1 = new Bitmap(800, 800);
                    Image thumb = new Bitmap(150, 150);
                    double h = img0.Height;
                    double w = img0.Width;
                    double wh_ratio, hw_ratio;
                    wh_ratio = w / h;
                    hw_ratio = h / w;
                    if (hw_ratio < 1 && w >= 800)
                    {
                        img1 = ImageHelper.ResizeImage(img0, 800, 800 / wh_ratio);
                        thumb = ImageHelper.ResizeImage(img0, 150, 150 / wh_ratio);
                    }
                    else if (hw_ratio < 1 && w < 800)
                    {
                        img1 = ImageHelper.ResizeImage(img0, w, w / wh_ratio);
                        thumb = ImageHelper.ResizeImage(img0, 150, 150 / wh_ratio);
                    }
                    else if (hw_ratio > 1 && h >= 800)
                    {
                        img1 = ImageHelper.ResizeImage(img0, 800 / hw_ratio, 800);
                        thumb = ImageHelper.ResizeImage(img0, 150 / hw_ratio, 150);

                    }
                    else
                    {
                        img1 = ImageHelper.ResizeImage(img0, h / hw_ratio, h);
                        thumb = ImageHelper.ResizeImage(img0, 150 / hw_ratio, 150);
                    }
                    if (ext.ToLower().Equals(".gif") && model.Animated)
                    {
                        paths = ImageHelper.MapImagePaths(pic, User.Identity.Name, ext.ToLower());
                        ImageHelper.GetImageResolution(img, pic);
                        img.Save(Request.MapPath(paths[0]));
                        img.Save(Request.MapPath(paths[1]));
                        thumb.Save(Request.MapPath(paths[2]), ImageFormat.Png);
                        pic.FileSize = ImageHelper.GetFileSize(img0);
                    }
                    else
                    {
                        paths = ImageHelper.MapImagePaths(pic, User.Identity.Name, ext.ToLower());
                        ImageHelper.GetImageResolution(img0, pic);
                        img0.Save(Request.MapPath(paths[0]));
                        img1.Save(Request.MapPath(paths[1]), ImageFormat.Png);
                        thumb.Save(Request.MapPath(paths[2]), ImageFormat.Png);
                        if (model.Replace)
                        {
                            ImageHelper.GetImageProperties(Request.MapPath(proto_path), pic);
                        }
                        pic.FileSize = ImageHelper.GetFileSize(img0);
                    }
                    img.Dispose();
                    img0.Dispose();
                    img1.Dispose();
                    thumb.Dispose();
                    foreach (string path in oldpaths)
                        ImageHelper.DeleteImage(Request.MapPath(path));
                    pic.LastModifiedDate = DateTime.Now;
                    await imageservice.EditPictureInfo(pic);
                    ImageHelper.DeleteImage(proto_path);
                    return RedirectToAction("Details", "Picture", new { id = pic.PictureId });
                }
            
            }
            ImageHelper.DeleteImage(proto_path);
            return View(model);
        }

        [MyAuthenticationFilter]
        [CheckIfBanned]
        public async Task<ActionResult> EditData(int? id)
        {
            if (id == null)
                return RedirectToAction("Error", "Home", new { message = Errors.PictureNotFound });
            Picture picture = await imageservice.GetPictureAsync((int)id);
            if (picture == null)
                return RedirectToAction("Error", "Home", new { message = Errors.PictureNotFound });
            UserProfile logged =await imageservice.GetUser(User.Identity.Name);
            bool normal_user = await imageservice.CheckRole(logged.Id, "User");
            if (!(picture.UserName).Equals(User.Identity.Name) && normal_user)
                return RedirectToAction("Error", "Home", new { message = Errors.PrivacyViolation });
            EditPictureDataViewModel model = new EditPictureDataViewModel();
            model.PictureId = (int)id;
            string culture = RouteData.Values["culture"] as string;
            model.Categories = imageservice.CategoriesSelect(culture);
            model.CategoryId = picture.CategoryId;
            model.Description = picture.Description;
            model.Title = picture.Title;
            model.Public = picture.Public;
            model.Adult = picture.Adult;
            model.CommentsOff = picture.CommentsOff;
            model.Tags = "";
            foreach (var item in picture.Tags)
            {
                model.Tags += item.Name + ",";
            }
            return View(model);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> EditData(EditPictureDataViewModel model)
        {
            if (ModelState.IsValid)
            {
                Picture picture = await imageservice.GetPictureAsync(model.PictureId);
                if (!model.Title.Equals(null))
                    picture.Title = model.Title;
                picture.Description = model.Description;
                picture.Public = model.Public;
                picture.Adult = model.Adult;
                picture.CategoryId = model.CategoryId;
                picture.CommentsOff = model.CommentsOff;
                picture.LastModifiedDate = DateTime.Now;
                if (model.Tags != null)
                {
                    List<Tag> ts = await imageservice.AddTags((model.Tags).Trim(' '));
                    List<Tag> tags = new List<Tag> { };

                    picture.Tags.Clear();
                    foreach (Tag item in ts)
                    {
                        picture.Tags.Add(item);
                    }
                }
                if (model.AlbumCover == true)
                    await imageservice.SetAlbumCover(picture);
                if (await imageservice.EditPictureInfo(picture))
                {
                    if (!(User.Identity.Name).Equals(picture.UserName))
                    {
                        Notification note = new Notification { AddresseeId = picture.UserName, SentDate = DateTime.Now, WasRead = false, Kind = NoteType.EditedImage, UserId = User.Identity.Name, Text = MessagesAndNotes.EditedImageNote, ThingId = picture.PictureId };
                        await imageservice.AddNote(note);
                    }
                    return RedirectToAction("Details", "Picture", new { id = model.PictureId });
                }
            }
            return View(model);
        }

        [MyAuthenticationFilter]
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
                return RedirectToAction("Error", "Home", new { message = Errors.PictureNotFound });
            Picture picture = await imageservice.GetPictureAsync((int)id);
            if (picture == null)
                return RedirectToAction("Error", "Home", new { message = Errors.PictureNotFound });
            UserProfile logged = await imageservice.GetUser(User.Identity.Name);
            bool normal_user = await imageservice.CheckRole(logged.Id, "User");
            if (!(picture.UserName).Equals(User.Identity.Name) && normal_user)
                return RedirectToAction("Error", "Home", new { message = Errors.PrivacyViolation });
            return View(picture);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Delete(Picture picture)
        {
                string p = picture.Album.Path;
                string[] paths = new string[] { picture.FullSizeUrl, picture.Url, picture.ThumbnailUrl };
                if (await imageservice.DeletePicture(picture))
                {
                    if (!(User.Identity.Name).Equals(picture.UserName))
                    {
                        Notification note = new Notification { AddresseeId = picture.UserName, SentDate = DateTime.Now, WasRead = false, Kind = NoteType.DeletedImage, UserId = User.Identity.Name, Text = MessagesAndNotes.DeletedImageNote, ThingId = picture.PictureId };
                        await imageservice.AddNote(note);
                    }
                    
                    foreach (string path in paths)
                    {
                        ImageHelper.DeleteImage(Request.MapPath(path));
                    }
                    return RedirectToAction("Browse", "Album", new { path = p });
                }

          
            return View(picture);
        }

        public ActionResult Confirm(ActionConfirmViewModel model)
        {
            return View(model);
        }
        #region helpers
        private void CreateFolder(string username)
        {
            string path = Request.MapPath(@"~/Images/" + username);
            DirectoryInfo dir_info = new DirectoryInfo(path);
            if (!dir_info.Exists)
            {
                Directory.CreateDirectory(path);
                DirectoryInfo dir_info2 = new DirectoryInfo(path);
                dir_info2.CreateSubdirectory("Thumbnails");
                dir_info2.CreateSubdirectory("FullSize");
                dir_info2.CreateSubdirectory("Temp");
            }

        }
        private void DeleteTemps(string username)
        {
            try
            {
                string tempfolder = Request.MapPath(@"~/Images/" + username + "/Temp");
                Array.ForEach(Directory.GetFiles(tempfolder), System.IO.File.Delete);
            }
            catch
            {
            }
        }
        #endregion

    }
}
