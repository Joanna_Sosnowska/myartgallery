﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MyArtGallery.Models;
using MyArtGallery.Repositories;
using MyArtGallery.Resources;
using PagedList;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using MyArtGallery.App_Start;

namespace MyArtGallery.Controllers
{
    public class TagController : BasicController
    {
        ITagRepository tag_repo;
        private UserManager<UserProfile> usermanager;
        public TagController()
            : this(new TagRepository())
        {}
         public TagController(ITagRepository repository)
        {
            tag_repo = repository;
            usermanager = new UserManager<UserProfile>(new UserStore<UserProfile>(new MyArtGalleryContext()));
            
        }
        
        public ActionResult AllTags(int? page)
        {
            //alfabetycznie!
            IQueryable<Tag> tags = tag_repo.Tags().OrderBy(t=>t.Name);
            int pageSize = 100;
            int pageNumber = (page ?? 1);
            return View(tags.ToPagedList(pageNumber, pageSize));
        }

        
        public async Task<ActionResult> Images(int? id, int? page)
        {
            if (id == null)
                return RedirectToAction("Error", "Home", new { message = Tagging.TagNotFound });
            Tag tag = await tag_repo.getTag(id);
            if(tag==null)
                return RedirectToAction("Error", "Home", new { message = Tagging.TagNotFound });
            ViewBag.TagName = tag.Name;
            ViewBag.Id = tag.TagId;
            IQueryable<Picture> pictures = tag.Pictures.AsQueryable();
            pictures = pictures.OrderByDescending(p => p.LastModifiedDate);
            int pageSize = 20;
            int pageNumber = (page ?? 1);
            return View(pictures.ToPagedList(pageNumber, pageSize));
        }

        [MyAuthenticationFilter]
        [CheckIfBanned]
        public ActionResult NewTag()
        {
            Tag t = new Tag();
            return View(t);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> NewTag(Tag tag)
        {
            Tag test = await tag_repo.getTag(tag.Name);
            if (test != null)
                ModelState.AddModelError("Name", Tagging.TagNameError);
        
            if (ModelState.IsValid)
            {
                Tag t = new Tag();
                t.Name = tag.Name;
                t.Pictures = new List<Picture> { };
                await tag_repo.AddTag(t);
                return RedirectToAction("AllTags");
            }

            return View(tag);
        }

        [MyAuthenticationFilter]
        [RoleAuthorize("Admin", "Moderator")]
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
                return RedirectToAction("Error", "Home", Errors.NotFound);
            Tag tag = await tag_repo.getTag(id);
            if (tag == null)
                return RedirectToAction("Error", "Home", Tagging.TagNotFound);
            return View(tag);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(Tag model)
        {
            Tag tag = await tag_repo.getTag(model.TagId);
            if (ModelState.IsValid)
            {
                if(model.Name!=null)
                   tag.Name = model.Name;
                await tag_repo.UpdateTag(tag);
                return RedirectToAction("AllTags");
            }
            return View(tag);
        }

    }
}
