﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MyArtGallery.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Threading.Tasks;
using Microsoft.Owin.Security;
using System.Security.Claims;
using System.Web.Helpers;
using MyArtGallery.Models.Helpers;
using MyArtGallery.Repositories;
using System.IO;
using MyArtGallery.Models.UserViewModels;
using PagedList;
using System.Drawing;
using System.Drawing.Imaging;
using MyArtGallery.Resources;
using MyArtGallery.Helpers;
using System.Net.Mail;
using MyArtGallery.Services;
using MyArtGallery.Validation;
using MyArtGallery.App_Start;

namespace MyArtGallery.Controllers
{
    public class UserProfileController : BasicController
    {
        private IUserAccountService user_service;
        public UserProfileController()
        {
            user_service = new UserAccountService(new ModelStateExtension(this.ModelState));
        }
        public UserProfileController(IUserAccountService s)
        {
            user_service = s;
        }
        public ActionResult List(int? page)
        {
            IQueryable<UserProfile> lista = user_service.UserManager.Users.OrderByDescending(u=>u.UserName);
            int pagenumber = (page ?? 1);
            return View(lista.ToPagedList(pagenumber, 10));
        }

        [HttpGet]
        [AllowAnonymous]
        public ActionResult Login(string url, string message)
        {
            LoginViewModel model = new LoginViewModel();
            model.Message = message;
            model.Url = url;
            return View(model);
        }
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginViewModel model)
        {
            if (ModelState.IsValid)
            {
                UserProfile user = await user_service.UserManager.FindAsync(model.UserName, model.Password);
                UserProfile user1 = await user_service.UserManager.FindByNameAsync(model.UserName);
                if (user != null)
                {
                    if (user.AccessFailedCount < 3)
                    {

                        user.LockoutEndDateUtc = null;
                        user.AccessFailedCount = 0;
                        await user_service.UserManager.UpdateAsync(user);
                        await AsyncLogin(user, model.RememberMe);

                        if (string.IsNullOrEmpty(model.Url))
                            return RedirectToAction("Index", "Home");
                        else
                            return Redirect(model.Url);
                    }
                    else if (user.LockoutEnabled && user.LockoutEndDateUtc < DateTime.UtcNow)
                    {
                        user.LockoutEndDateUtc = null;
                        user.LockoutEnabled = false;
                        user.AccessFailedCount = 0;
                        await user_service.UserManager.UpdateAsync(user);
                        await AsyncLogin(user, model.RememberMe);
                        return RedirectToAction("Index", "Home");
                    }
                    else
                    {
                        ModelState.AddModelError("Password", UserAccount.LimitPassed);
                    }
                }
                else if (user1 != null)
                {
                    user1.AccessFailedCount += 1;
                    user1.LockoutEnabled = true;
                    user1.LockoutEndDateUtc = DateTime.UtcNow.AddHours(12);
                    await user_service.UserManager.UpdateAsync(user1);
                    ModelState.AddModelError("Password", UserAccount.LoginError);
                }
                else
                {
                    ModelState.AddModelError("UserName", UserAccount.UserNameError);                  
                }
            }

             //w razie błędu
            return View(model);
        }

        [AllowAnonymous]
        public ActionResult Register()
        {
            Random r = new Random();
            int A = r.Next(0, 21);
            int B = r.Next(0, 21);
            RegistrationViewModel model = new RegistrationViewModel();
            model.A = A;
            model.B = B;
            model.Answer = A + B;
            model.Gender = Gender.N;
            string culture = RouteData.Values["culture"] as string;
            model.Genres = user_service.CategoriesSelect(culture);
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [AllowAnonymous]
        public async Task<ActionResult> Register(RegistrationViewModel model)
        {
            CreateAvatarDirectory();
            DeleteTemps();
            CreateUserFolders(model.UserName);
            Image img = null;
            string ext = ".png";
            bool validType = false;
            string proto_path = @"~/Images/Avatars/Temp/";
            if (model.IsUrl)
            {
                if (model.Url != null)
                {
                    if (validType)
                    {
                        validType = user_service.ValidAvatar(model.Url, true);
                        ext = ImageHelper.GetFileType(model.Url);
                        proto_path += model.UserName +ext;
                        ImageHelper.SaveImageProto(model.Url, Request.MapPath(proto_path));
                    }
                }
                else
                {
                    validType = user_service.ValidAvatar("",true);
                }
            }
            else
            {
                if (model.File != null)
                {
                    validType = user_service.ValidAvatar(model.File.FileName, false);
                    ext = Path.GetExtension(model.File.FileName);
                    if (validType)
                    {
                        proto_path += model.UserName +ext;
                        ImageHelper.SaveImageProto(model.File, Request.MapPath(proto_path));
                    }
                }
                else
                {
                    validType = user_service.ValidAvatar("", false);
                }
            }
            try
            {
                img = Image.FromFile(Request.MapPath(proto_path));
            }
            catch
            {
                img = null;
            }
            ImageHelper.DeleteImage(Request.MapPath(proto_path));
            if (user_service.UserManager.FindByName(model.UserName) != null)
                ModelState.AddModelError("UserName", UserAccount.UserExists);
            if (model.Answer != model.Result)
                ModelState.AddModelError("Result", UserAccount.HumanValidation);
           
            if (ModelState.IsValid)
            {
                var user = new UserProfile() { UserName = model.UserName, Email = model.Email, BirthDate = (DateTime)model.BirthDate, Country = model.Country, Rank = Rank.User, UserDescription = model.UserDescription, Gender = model.Gender, Albums = new List<Album> { }, Comments = new List<Comment> { } };
                string[] path = UserProfileHelper.SetAvatarPath(img, model.UserName);
                user.AvatarPath = path[0];
                user.AvatarThumbPath = path[1];             
                List<Category> genres = await user_service.SelectedCategories(model.SelectedGenres).ToListAsync();
                user.Genres = genres;
                user.Favourites = new List<Favourite> { };
                Album def = new Album { UserName=user.UserName, Path = user.UserName, Pictures = new List<Picture> { }, SubAlbums = new List<Album> { }, Title="Default", AddedDate=DateTime.Now, LastModifiedDate=DateTime.Now, Public=true };
                AlbumRepository repo = new AlbumRepository();
                IdentityResult result = await user_service.UserManager.CreateAsync(user, model.Password);
                await repo.AddAlbum(0, def);
                    if (result.Succeeded)
                    {
                        if (img != null)
                        {
                            img = ImageHelper.CreateImage(img, model.X, model.Y, model.Width, model.Height, 0, false, false);
                            Bitmap img1 = ImageHelper.ResizeImage(img, 150, 150);
                            Bitmap thumb = ImageHelper.ResizeImage(img, 50, 50);
                            img.Dispose();
                            img1.Save(Request.MapPath(path[0]), ImageFormat.Png);
                            thumb.Save(Request.MapPath(path[1]), ImageFormat.Png);
                            ImageHelper.DeleteImage(proto_path);
                        }
                        await AsyncLogin(user, isPersistent: false);
                        return RedirectToAction("Index", "Home");
                    }
            }
            // jak są błędy
            ImageHelper.DeleteImage(proto_path);
            Random r = new Random();
            int A = r.Next(0, 21);
            int B = r.Next(0, 21);
            model.A = A;
            model.B = B;
            model.Answer = A + B;
            model.Gender = Gender.N;
            string culture = RouteData.Values["culture"] as string;
            model.Genres = user_service.CategoriesSelect(culture);
            model.SelectedGenres = null;
            model.Gender = Gender.N;
            return View(model);
        }

        public async Task<ActionResult> Browse(string id)
        {
            if (id == null)
                return RedirectToAction("Error", "Home", new { message = Errors.UserNotFound });
            UserProfile user = await user_service.UserManager.FindByNameAsync(id);
            if (user == null)
                return RedirectToAction("Error", "Home", new { message = Errors.UserNotFound });
            string lang = RouteData.Values["culture"] as string;
            ViewBag.Language = lang;
            return View(user);
        }


        public async Task<ActionResult> Gallery(string id)
        {
            if (id == null)
                return RedirectToAction("Error", "Home", new { message = Errors.UserNotFound });
            UserProfile user = await user_service.UserManager.FindByNameAsync(id);
            if (user == null)
                return RedirectToAction("Error", "Home", new { message = Errors.UserNotFound });
            return RedirectToAction("Browse", "Album", new { path = user.Albums.ElementAt(0).Path });
        }

        public async Task<ActionResult> Favourites(string id, int? page)
        {
            UserProfile user = await user_service.UserManager.FindByNameAsync(id);
            if (user == null)
                return RedirectToAction("Error", "Home", new { message = Errors.UserNotFound });
            ViewBag.UserName = id;
            int pagenumber = (page ?? 1);
            IQueryable<Favourite> favs = user.Favourites.AsQueryable().OrderByDescending(f=>f.FavouriteId);
            return View(favs.ToPagedList(pagenumber,20));
        }

        [MyAuthenticationFilter]
        public ActionResult DeleteFav(string id)
        {
            UserProfile user = user_service.UserManager.FindByName(id);
            if (user == null)
                return RedirectToAction("Error", "Home", new { message = Errors.UserNotFound });
            return View(user);
        }

        [MyAuthenticationFilter]
        public ActionResult Messages(string id)
        {
            return RedirectToAction("ReceivedList", "Message");
        }

        [MyAuthenticationFilter]
        public ActionResult Notifications(string id)
        {
            return RedirectToAction("UserNotifications", "Notification");
        }

        public ActionResult Sidebar()
        {
            SidebarViewModel model = new SidebarViewModel();
            MessageRepository repo = new MessageRepository();
            NoteRepository repo1 = new NoteRepository();
            model.NewMessages = false;
            model.NewNotifications = false;
            model.IsModOrAdmin = false;
            model.MessageCount = 0;
            model.NotificationCount = 0;
            UserProfile user = user_service.UserManager.FindByName(User.Identity.Name);
            if (user != null)
            {
                int mc = repo.NewMessages(user.UserName).Count();
                int nc = repo1.NewNotifications(user.UserName).Count();
                if (mc > 0)
                {
                    model.NewMessages = true;
                    model.MessageCount = mc;
                }

                if (nc > 0)
                {
                    model.NewNotifications = true;
                    model.NotificationCount = nc;
                }
                if (user.Rank != Rank.User)
                    model.IsModOrAdmin = true;
            }
            return View(model);
        }



        [MyAuthenticationFilter]
        public async Task<ActionResult> Edit(string id)
        {
            CreateAvatarDirectory();
            DeleteTemps();
            if (id == null || !id.Equals(User.Identity.Name))
                return RedirectToAction("Error", "Home", new { message = Errors.PrivacyViolation });

            UserProfile userprofile = await user_service.UserManager.FindByNameAsync(id);
            if (userprofile == null)
                return RedirectToAction("Error", "Home", new { message = Errors.UserNotFound });
            EditProfileViewModel model = new EditProfileViewModel();
            model.UserName = userprofile.UserName;
            model.UserDescription = userprofile.UserDescription;
            model.Gender = userprofile.Gender;
            model.Country = userprofile.Country;
            model.Email = userprofile.Email;
            model.BirthDate = userprofile.BirthDate;
            string culture = RouteData.Values["culture"] as string;
            model.Genres = user_service.CategoriesSelect(culture);
            model.AvatarPath = userprofile.AvatarPath;
            foreach (Category c in userprofile.Genres)
            {
                model.Genres.Select(f => f.Value.Equals(c.CategoryId.ToString()));
            }
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(EditProfileViewModel model)
        {
            CreateUserFolders(model.UserName);
            Image img = null;
            //tylko po to żeby walidacja przeszła, jak obrazek jest null
            string ext = ".png";
            bool validType = false;
            string proto_path = @"~/Images/Avatars/Temp/";
            if (model.IsUrl)
            {
                if (model.Url != null)
                {
                    validType = user_service.ValidAvatar(model.Url, true);
                    ext = ImageHelper.GetFileType(model.Url);
                    if (validType)
                    {
                        proto_path += model.UserName+ext;
                        ImageHelper.SaveImageProto(model.Url, Request.MapPath(proto_path));
                    }
                }
                else
                {
                    validType = user_service.ValidAvatar("", true);
                }

            }
            else
            {
                if (model.File != null)
                {
                    validType = user_service.ValidAvatar(model.File.FileName, false);
                    ext = Path.GetExtension(model.File.FileName);
                    if (validType)
                    {
                        proto_path += model.UserName + ext;
                        ImageHelper.SaveImageProto(model.File, Request.MapPath(proto_path));
                    }
                }
                else
                {
                    validType = user_service.ValidAvatar("", false);
                }
            }
            try
            {
                img = Image.FromFile(Request.MapPath(proto_path));
            }
            catch
            {
                img = null;
            }
            ImageHelper.DeleteImage(Request.MapPath(proto_path));
            //mozna nie posiadac avatara, więc walidacja nie robi problemów gdy obraz jest null
            if (!(model.IsFile) && !(model.IsUrl))
            {
                    img = new Bitmap(Request.MapPath(model.AvatarPath));
                    ext = ImageHelper.GetFileType(Request.MapPath(model.AvatarPath));
            }

            if (ModelState.IsValid)
            {
                UserProfile user = user_service.UserManager.FindByName(model.UserName);
                if(model.UserName!=null)
                      user.UserName = model.UserName;
                if(model.Email != null)
                    user.Email = model.Email;
                if(model.UserDescription != null)
                    user.UserDescription = model.UserDescription;
                user.BirthDate=(DateTime)model.BirthDate;
                if(model.Country != null)
                    user.Country = model.Country;
                user.Gender = model.Gender;
                if (model.SelectedGenres != null)
                {
                    List<Category> genres = await user_service.SelectedCategories(model.SelectedGenres).ToListAsync();
                    IQueryable<Category> user_genres = user.Genres.AsQueryable();
                    IQueryable<Category> gs;
                    foreach (Category item in genres)
                    {
                        gs = user_genres.Where(g=>g.CategoryName.Equals(item.CategoryName));
                        if (gs.Count() == 0)
                            user.Genres.Add(item);
                    }
                    foreach (Category item in user_genres)
                    {
                        if (!genres.Contains(item))
                            user.Genres.Remove(item);

                    }
                    user.Genres = genres;
                }
                string oldpath = user.AvatarPath;
                string oldpath2 = user.AvatarThumbPath;
                if (img != null && !ImageHelper.IsDefault(oldpath))
                {
                    RemoveAvatar(oldpath, oldpath2);
                    string[] paths = new string[2];
                    Bitmap image = ImageHelper.CreateImage(img, model.X, model.Y, model.Width, model.Height, 0, false, false);
                    paths = UserProfileHelper.SetAvatarPath(img, model.UserName);
                    user.AvatarPath = paths[0];
                    user.AvatarThumbPath = paths[1];
                    img.Dispose();
                    ImageHelper.ResizeImage(image, 150, 150).Save(Request.MapPath(paths[0]), ImageFormat.Png);
                    ImageHelper.ResizeImage(image, 50, 50).Save(Request.MapPath(paths[1]), ImageFormat.Png);
                    ImageHelper.DeleteImage(proto_path);
                }               
                if(model.RemoveAvatar)
                {
                    RemoveAvatar(oldpath, oldpath2);
                    user.AvatarThumbPath = UserProfileHelper.DefaultAvatarThumbPath;
                    user.AvatarPath = UserProfileHelper.DefaultAvatarPath;
                }
                ImageHelper.DeleteImage(proto_path);
                var result = await user_service.UserManager.UpdateAsync(user);              
                await AsyncLogin(user, isPersistent: false);
                return RedirectToAction("Index", "Home");
            }

            // jak są błędy
            string culture = RouteData.Values["culture"] as string;
            model.Genres = user_service.CategoriesSelect(culture);
            model.SelectedGenres = null;
            return View(model);
        }

        [MyAuthenticationFilter]
        public ActionResult ChangePassword(string message)
        {
            ChangePasswordViewModel model = new ChangePasswordViewModel();          
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ChangePassword(ChangePasswordViewModel model)
        {
                if (ModelState.IsValid)
                {
                    IdentityResult result = await user_service.UserManager.ChangePasswordAsync(User.Identity.GetUserId(), model.OldPassword, model.NewPassword);
                    if (result.Succeeded)
                    {
                        return RedirectToAction("Index", "Home");
                    }
                    else
                    {
                    //    AddErrors(result);
                    }
                }
            
            return View(model);
        }

        
        public ActionResult Logout()
        {            
            AuthenticationManager.SignOut();
            return RedirectToAction("Index", "Home");
        }
        #region helpers

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        private async Task AsyncLogin(UserProfile user, bool isPersistent)
        {
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ExternalCookie);
            var identity = await user_service.UserManager.CreateIdentityAsync(user, DefaultAuthenticationTypes.ApplicationCookie);
            AuthenticationManager.SignIn(new AuthenticationProperties() { IsPersistent = isPersistent }, identity);
        }

        private void RemoveAvatar(string path, string thumb_path)
        {
            if (!ImageHelper.IsDefault(path))
            {
                ImageHelper.DeleteImage(Request.MapPath(path));
                ImageHelper.DeleteImage(Request.MapPath(thumb_path));
            }
        }
        private void CreateUserFolders(string username)
        {
            string path = Request.MapPath(@"~/Images/" + username);
            DirectoryInfo dir_info = new DirectoryInfo(path);
            if (!dir_info.Exists)
            {
                Directory.CreateDirectory(path);
                DirectoryInfo dir_info2 = new DirectoryInfo(path);
                dir_info2.CreateSubdirectory("Thumbnails");
                dir_info2.CreateSubdirectory("FullSize");
                dir_info2.CreateSubdirectory("Temp");
            }

        }
        private void CreateAvatarDirectory()
        {
            string path = Request.MapPath(@"~/Images/Avatars");
            string path1 = Request.MapPath(@"~/Images/Avatars/Temp");
            DirectoryInfo dir_info = new DirectoryInfo(path);
            if (!dir_info.Exists)
                Directory.CreateDirectory(path);
            DirectoryInfo dir_info1 = new DirectoryInfo(path1);
            if (!dir_info1.Exists)
                Directory.CreateDirectory(path1);

        }
        private void DeleteTemps()
        {
            try
            {
                string tempfolder = Request.MapPath(@"~/Images/Avatars/Temp");
                Array.ForEach(Directory.GetFiles(tempfolder), System.IO.File.Delete);
            }
            catch
            {
            }
        }

        #endregion
    }
}
