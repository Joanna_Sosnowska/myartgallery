﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MyArtGallery.Models;
using MyArtGallery.Repositories;
using System.Threading.Tasks;
using PagedList;
using MyArtGallery.Resources;

namespace MyArtGallery.Controllers
{
    public class CategoryController : BasicController
    {
        private ICategoryRepository cat_repo;
        public CategoryController()
            : this(new CategoryRepository())
        {}
         public CategoryController(ICategoryRepository repository)
        {
            cat_repo = repository;
        }
        // GET: /Category/
        public async Task<ActionResult> Index()
        {
            var categories = cat_repo.Categories();
            string lang = RouteData.Values["culture"] as string;
            ViewBag.Language = lang;
            return View(await categories.ToListAsync());
        }

        public ActionResult StartList()
        {
            var categories = cat_repo.MainCategories();

            return View(categories.ToList());
        }

        
        public async Task<ActionResult> Images(int? id, int? page)
        {
            if (id == null)
                return RedirectToAction("Error", "Home", new { message = Errors.NoSuchGenre });

            Category category = await cat_repo.GetCategory(id);
            if (category == null)
                return RedirectToAction("Error", "Home", new { message = Errors.NoSuchGenre });
            IQueryable<Picture> pictures = category.Pictures.AsQueryable().OrderByDescending(p=>p.LastModifiedDate);
            int pagenumber = (page ?? 1);
            ViewBag.Category = category;
            string lang = RouteData.Values["culture"] as string;
            ViewBag.Language = lang;
            return View(pictures.ToPagedList(pagenumber,12));
        }
    }
}
