﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MyArtGallery.Helpers;
using MyArtGallery.Models;
using MyArtGallery.Models.SearchEngineViewModels;
using MyArtGallery.Services;
using PagedList;

namespace MyArtGallery.Controllers
{
    
    public class HomeController : BasicController
    {
        //
        // GET: /Home/
        private ISearchEngineService searchhelper;
        public HomeController()
        {
            searchhelper = new SearchEngineService();
        }


        public HomeController(ISearchEngineService s)
        {
            searchhelper = s;
        }
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult SetCulture(string culture)
        {
            culture = CultureHelper.GetImplementedCulture(culture);
            RouteData.Values["culture"] = culture; 
            return RedirectToAction("Index");
        }
        public ActionResult Error(string message)
        {
            ErrorViewModel model = new ErrorViewModel();
            model.Message = message;
            return View(model);
        }

        public ActionResult SearchMenu()
        {
            SearchTypeViewModel model = new SearchTypeViewModel();
            model.SearchRange = SearchRange.Pictures;
            model.SearchType = SearchType.Basic;
            return View(model);
        }
        [HttpPost]
        public ActionResult SearchMenu(SearchTypeViewModel model)
        {
            //wybór rodzaju szukania - 4 opcje
            if ((int)model.SearchRange == 1 && (int)model.SearchType == 1)
                return RedirectToAction("SearchPictures", new { phrase = model.Phrase });
            if ((int)model.SearchRange == 1 && (int)model.SearchType == 2)
                return RedirectToAction("SearchPicturesAdvanced", new { phrase = model.Phrase });
            if ((int)model.SearchRange == 2 && (int)model.SearchType == 1)
                return RedirectToAction("SearchProfiles", new { phrase = model.Phrase });
            if ((int)model.SearchRange == 2 && (int)model.SearchType == 2)
                return RedirectToAction("SearchProfilesAdvanced", new { phrase = model.Phrase });
            return View(model);
        }
        //
        public ActionResult SearchPicturesAdvanced(string phrase)
        {
            SearchImagesViewModel model = new SearchImagesViewModel { Title = phrase };
            string culture = RouteData.Values["culture"] as string;
            model.Categories = searchhelper.CategoriesSelect(culture);
            
            return View("SearchPicturesForm", model);
        }
        [HttpPost]
        public ActionResult SearchPicturesAdvanced(SearchImagesViewModel model, int? page)
        {
            IQueryable<Picture> pictures = searchhelper.SearchPictures(model).OrderByDescending(p => p.LastModifiedDate);
            int pagenumber = (page ?? 1);
            ViewBag.Model = model;
            return View(pictures.ToPagedList(pagenumber, 20));
        }

        public ActionResult SearchProfilesAdvanced(string phrase)
        {
            SearchProfilesViewModel model = new SearchProfilesViewModel { UserName = phrase };
            string culture = RouteData.Values["culture"] as string;
            model.Genres = searchhelper.CategoriesSelect(culture);
            model.SelectedGenres = new List<int>{ };
            return View("SearchProfilesForm", model);
        }
        [HttpPost]
        public ActionResult SearchProfilesAdvanced(SearchProfilesViewModel model, int? page)
        {
            IQueryable<UserProfile> profiles = searchhelper.SearchProfiles(model).OrderBy(p => p.UserName);
            int pagenumber = (page ?? 1);
            ViewBag.Model = model;
            return View(profiles.ToPagedList(pagenumber, 10));
        }
        //wyniki szukania prostego -tworzenie listy
        public ActionResult SearchPictures(string phrase, int? page)
        {
            IQueryable<Picture> pictures1 = searchhelper.SearchPictures(phrase).OrderByDescending(p=>p.LastModifiedDate);
            ViewBag.Phrase = phrase;
            int pagenumber = (page ?? 1);
            return View(pictures1.ToPagedList(pagenumber, 20));
        }
        //wyniki szukania prostego -tworzenie listy
        public ActionResult SearchProfiles(string phrase, int? page)
        {
            IQueryable<UserProfile> profiles = searchhelper.SearchProfiles(phrase).OrderBy(p => p.UserName);
            ViewBag.Phrase = phrase;
            int pagenumber = (page ?? 1);
            return View(profiles.ToPagedList(pagenumber, 10));
        }

        public ActionResult Rules(string lang)
        {

            switch (lang)
            {
                case "en":
                    return File(Request.MapPath(@"~/Content/Reg/rules.pdf"), "application/pdf", "rules.pdf");
                case "pl":
                    return File(Request.MapPath(@"~/Content/Reg/regulamin.pdf"), "application/pdf", "regulamin.pdf");
                default:
                    return View();

            }
        }

       

        

        
    }
}
