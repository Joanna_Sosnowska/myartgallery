﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MyArtGallery.Models;
using System.Threading.Tasks;
using MyArtGallery.Repositories;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using MyArtGallery.Models.GalleryViewModels;
using System.IO;
using System.Threading;
using MyArtGallery.Helpers;
using PagedList;
using MyArtGallery.Resources;
using MyArtGallery.App_Start;

namespace MyArtGallery.Controllers
{
    public class AlbumController : BasicController
    {
        private IAlbumRepository albumrepo;
        public UserManager<UserProfile> UserManager { get; private set; }
        public AlbumController(UserManager<UserProfile> userManager, AlbumRepository repo)
        {
            UserManager = userManager;
            albumrepo = repo;
        }

        public AlbumController()
            : this(new UserManager<UserProfile>(new UserStore<UserProfile>(new MyArtGalleryContext())), new AlbumRepository())
        {
        }
        // GET: /Album/
        public async Task<ActionResult> Index()
        {
            return View(await albumrepo.albumList().ToListAsync());
        }
        public async Task<ActionResult> UserGalleries(string id)
        {
            IQueryable<Album> albums = albumrepo.albumList(id);
            ViewBag.UserName = id;
            return View(await albums.ToListAsync());
        }

        public ActionResult Browse(string path)
        {
            Album album = albumrepo.getAlbum(path);
            if (album == null)
                return RedirectToAction("Error", "Home", new { message = Errors.AlbumNotFound });

            if (!album.Public && !Request.LogonUserIdentity.IsAuthenticated)
                return RedirectToAction("Login", "UserProfile", new { message = Errors.LoginRequired });
            return View(album);
        }

        public async Task<ActionResult> NameCollision(int id)
        {
            Album album = await albumrepo.getAlbum(id);
            if (album == null)
                return RedirectToAction("Error", "Home", new { message = Errors.AlbumNotFound });
            if (!(album.UserName.Equals(User.Identity.Name)))
                return RedirectToAction("Error", "Home", new { message = Errors.PrivacyViolation });
            return View(album);
        }

        public ActionResult Pictures(string path, int? page)
        {
            Album album = albumrepo.getAlbum(path);
            if (album == null)
                return RedirectToAction("Error", "Home", new { message = Errors.AlbumNotFound });
            if (!album.Public && !Request.LogonUserIdentity.IsAuthenticated)
                return RedirectToAction("Login", "UserProfile", new { message = Errors.LoginRequired });
            IQueryable<Picture> pictures = album.Pictures.AsQueryable().OrderByDescending(p => p.LastModifiedDate);
            int page_nr = (page ?? 1);
            ViewBag.Path = path;
            return View(pictures.ToPagedList(page_nr, 8));
        }

        public ActionResult SubAlbums(string path, int? page)
        {
            Album album = albumrepo.getAlbum(path);
            if (album == null)
                return RedirectToAction("Error", "Home", new { message = Errors.AlbumNotFound });
            if (!album.Public && !Request.LogonUserIdentity.IsAuthenticated)
                return RedirectToAction("Login", "UserProfile", new { message = Errors.LoginRequired });
            IQueryable<Album> albums = album.SubAlbums.AsQueryable().OrderByDescending(a => a.LastModifiedDate);
            int page_nr = (page ?? 1);
            ViewBag.Path = path;
            return View(albums.ToPagedList(page_nr, 3));
        }

        [MyAuthenticationFilter]
        [CheckIfBanned]
        public async Task<ActionResult> Add(int id)
        {
            //parametr id - id albumu, do którego chcemy dodać album
            string name = User.Identity.GetUserName();
            Album album = await albumrepo.getAlbum(id);
            if (album == null)
                return RedirectToAction("Error", "Home", new { message = Errors.AlbumNotFound });
            if (!(album.UserName).Equals(name))
                return RedirectToAction("Error", "Home", new { message = Errors.PrivacyViolation });
            AddAlbumViewModel model = new AddAlbumViewModel();
            model.UserProfileId = name;
            model.ParentId = id;
            
            return View(model);
        }

    
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Add(AddAlbumViewModel model)
        {
            Album parent = await albumrepo.getAlbum(model.ParentId);
            if (parent.SubAlbums.Where(a => a.Title.Equals(model.Title)).Count()!=0)
                ModelState.AddModelError("Title", Errors.AlbumNameCollision);
            if (ModelState.IsValid)
            {
                Album album = new Album { Title = model.Title, Public = model.Public, Description = model.Description, UserName = model.UserProfileId};
                await albumrepo.AddAlbum(model.ParentId, album);
                return RedirectToAction("Browse", new { path=album.Path});
            }

            return View(model);
        }

        [MyAuthenticationFilter]
        [CheckIfBanned]
        public async Task<ActionResult> Edit(string path)
        {
            if (path=="")
                return RedirectToAction("Error", "Home", new { message = Errors.AlbumNotFound });
            Album album = albumrepo.getAlbum(path);
            
            if (album == null)
                return RedirectToAction("Error", "Home", new { message = Errors.AlbumNotFound });
            UserProfile user = await UserManager.FindByNameAsync(User.Identity.Name);
            if (!(user.UserName).Equals(User.Identity.Name) && !(UserManager.IsInRole(user.Id, "Admin") || UserManager.IsInRole(user.Id, "Moderator")))
                return RedirectToAction("Error", "Home", new { message = Errors.PrivacyViolation });
            EditAlbumViewModel model = new EditAlbumViewModel();
            model.Description = album.Description;
            model.Title = album.Title;
            model.Public = album.Public;
            model.Path = album.Path;
            return View(model);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(EditAlbumViewModel model)
        {
             Album a = albumrepo.getAlbum(model.Path);
             if (a.Parent.SubAlbums.Where(album => album.Title.Equals(model.Title)).Count()!=0)
                 ModelState.AddModelError("Title", Errors.AlbumNameCollision);
            if (ModelState.IsValid)
            {
               
                if (model.Description != null)
                    a.Description = model.Description;
                if (model.Title != null)
                {
                    a.Title = model.Title;
                    a.Path = a.Parent.Path + "/" + model.Title;
                    AlbumHelper.RenameSubalbums(a);
                }
                a.Public = model.Public;
             
                await albumrepo.UpdateAlbum(a);
                return RedirectToAction("Browse", new { path = a.Path });
            }
            return View(model);
        }

        [MyAuthenticationFilter]
        public ActionResult Move(string path)
        {
            if (path == "")
                return RedirectToAction("Error", "Home", new { message = Errors.AlbumNotFound });
            Album album = albumrepo.getAlbum(path);
            if (album == null)
                return RedirectToAction("Error", "Home", new { message = Errors.AlbumNotFound });
            if(album.ParentId==null)
                return RedirectToAction("Error", "Home", new { message = Errors.CantMoveDefault });
            
            if (album.UserName != User.Identity.Name)
                return RedirectToAction("Error", "Home", new { message = Errors.PrivacyViolation });       
            MoveAlbumViewModel model = new MoveAlbumViewModel();
            model.Albums = albumrepo.SelectAlbumsExcluding(model.Path, User.Identity.Name);
            model.Path = album.Path;
            return View(model);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Move(MoveAlbumViewModel model)
        {
            Album parent = await albumrepo.getAlbum(model.AlbumId);
            Album a = albumrepo.getAlbum(model.Path);
            if ((model.Path).Equals(parent.Path))
                return RedirectToAction("Error", "Home", new { message = Errors.MovingError });
            if (parent.SubAlbums.Where(album => album.Title.Equals(a.Title)).Count()!=0)
                return RedirectToAction("NameCollision", "Album", new { id=a.AlbumId });
           
            if (ModelState.IsValid)
            {
                
                a.ParentId = model.AlbumId;
              
                a.Path = parent.Path+"/"+a.Title;
                AlbumHelper.RenameSubalbums(a);
                await albumrepo.UpdateAlbum(a);
                return RedirectToAction("Browse", new { path = a.Path });
            }
            return View(model);
        }

        [MyAuthenticationFilter]
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
                return RedirectToAction("Error", "Home", new { message = Errors.AlbumNotFound });
            Album album = await albumrepo.getAlbum((int)id);
            if (album == null)
                return RedirectToAction("Error", "Home", new { message = Errors.AlbumNotFound });

            if (!(album.UserName).Equals(User.Identity.Name))
                return RedirectToAction("Error", "Home", new { message = Errors.PrivacyViolation });
            if (album.SubAlbums.Count() != 0 || album.Pictures.Count() != 0)
            {
                //można usunąć tylko pusty album
                return RedirectToAction("Error", "Home", new { message = Errors.DeleteError });
            }
            return View(album);

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Delete(Album album)
        {
            await albumrepo.DeleteAlbum(album);
            return RedirectToAction("Gallery", "UserProfile", new{id=User.Identity.Name});
        }

    }
}
