﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using MyArtGallery.Models;
using MyArtGallery.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using PagedList;
using MyArtGallery.Resources;
using MyArtGallery.Models.GalleryViewModels;
using MyArtGallery.App_Start;

namespace MyArtGallery.Controllers
{
    public class AdministrationController : BasicController
    {
        //
        // GET: /Administration/
        private MyArtGalleryContext context;
        private IProtestRepository protestrepo;
        private INoteRepository noterepository;
        public UserManager<UserProfile> UserManager { get; private set; }
        public AdministrationController()
        {
            this.context = new MyArtGalleryContext();
            this.protestrepo = new ProtestRepository(context);
            this.noterepository = new NoteRepository(context);
            this.UserManager = new UserManager<UserProfile>(new UserStore<UserProfile>(context));
        }
        public ActionResult Index()
        {
            return View();
        }

        [MyAuthenticationFilter]
        public ActionResult MarkAsImproper(int id, ProtestType kind)
        {
            Protest model = new Protest();
            model.ThingId = id;
            model.Kind = kind;
            model.UserId = User.Identity.Name;
            ViewBag.Path = "";
            return PartialView(model);
        }
        [HttpPost]
        public async Task<ActionResult> MarkAsImproper(Protest model)
        {

            if (ModelState.IsValid)
            {
                model.SentDate = DateTime.Now;
                model.Verified = false;
                await protestrepo.AddProtest(model);
                return PartialView("Confirm", new ActionConfirmViewModel { Message = MessagesAndNotes.MarkedAsImproper, Success = true });
            }
            return PartialView(model);
        }

        [MyAuthenticationFilter]
        [RoleAuthorize("Admin", "Moderator")]
        public ActionResult Protests(int? page, ProtestType type)
        {
            IQueryable<Protest> protests = protestrepo.GetProtests(type);
            int pagenumber = (page ?? 1);
            ViewBag.N = (int)type;
            return View(protests.ToPagedList(pagenumber, 10));
        }

        [MyAuthenticationFilter]
        [RoleAuthorize("Admin", "Moderator")]
        public ActionResult Users(int? page)
        {
            IQueryable<UserProfile> users = UserManager.Users.OrderBy(u=>u.UserName);
            int pagenumber = (page ?? 1);
            ViewBag.N = 3;
            return View(users.ToPagedList(pagenumber, 10));
        }

        [MyAuthenticationFilter]
        public ActionResult MyProtests(int? page)
        {
            IQueryable<Protest> protests = protestrepo.GetProtests(User.Identity.Name);
            int pagenumber = (page ?? 1);
            return View(protests.ToPagedList(pagenumber, 10));
        }

        [MyAuthenticationFilter]
        [RoleAuthorize("Admin")]
        public ActionResult ChangeUserRole(string id)
        {
            ChangeRoleViewModel model = new ChangeRoleViewModel();
            model.UserName = id;
            model.Role = Rank.User;
            return View(model);
        }
        [HttpPost]
        [RoleAuthorize("Admin")]
        public async Task<ActionResult> ChangeUserRole(ChangeRoleViewModel model)
        {
            UserProfile user = await UserManager.FindByNameAsync(model.UserName);
            user.Rank = model.Role;
            await UserManager.AddToRoleAsync(user.Id, model.Role.ToString());
            await UserManager.UpdateAsync(user);
            Notification note = new Notification { UserId = User.Identity.Name, ThingId=0, AddresseeId = model.UserName, SentDate = DateTime.Now, Kind = NoteType.RightsChanged, Text = User.Identity.Name + " " + MessagesAndNotes.ChangeRole, WasRead = false };
            await noterepository.AddNote(note);
            return RedirectToAction("Index", "Administration", new { tab=2});
        }

        [MyAuthenticationFilter]
        [RoleAuthorize("Admin", "Moderator")]
        public async Task<ActionResult> RemoveProtest(int id, ProtestType typ)
        {
            await protestrepo.DeleteProtest(id);
            int t = 1;
            if (typ == ProtestType.Picture)
                t = 2;
            return RedirectToAction("Index", "Administration", new { tab=t, type=typ });
        }

        public ActionResult Confirm(ActionConfirmViewModel model)
        {
            return PartialView(model);
        }

	}
}