﻿using Microsoft.Owin;
using Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity.Infrastructure;
using System.Xml;
using System.Text;
using MyArtGallery.Models; 

[assembly: OwinStartupAttribute(typeof(MyArtGallery.Startup))]
namespace MyArtGallery
{
    
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);           
        }


    }
}