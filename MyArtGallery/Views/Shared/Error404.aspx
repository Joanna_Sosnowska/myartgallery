﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Error404.aspx.cs" Inherits="MyArtGallery.Views.Shared.Error404" %>
<% Response.StatusCode = 404; %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Error 404</title>
    <link href="~/favicon.ico" rel="shortcut icon" type="image/x-icon" />
    <link rel="stylesheet" type="text/css" href="~/~/Content/Site.css"/>
    <script src="~/~/Scripts/jquery-1.4.1.min.js" type="text/javascript"></script>
    <meta name="viewport" content="width=device-width" />
</head>
<body>
    <h1 class="error">Error 404</h1>
     <h2 class="error">Page not found</h2>
</body>
</html>
