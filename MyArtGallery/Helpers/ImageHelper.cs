﻿using MyArtGallery.Helpers;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Text;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;

namespace MyArtGallery.Models.Helpers
{
    public static class ImageHelper
    {
        private static string defpath1 = "~/Content/Images/default.png";
        private static string defpath2 = "~/Content/Images/defaultmini.png";

        public static string GetFileType(string url)
        {
            var parts = url.Split(new char[] { '/' }, StringSplitOptions.RemoveEmptyEntries);
            var last = parts[parts.Length - 1];
            return Path.GetExtension(last);
        }

        public static bool IsDefault(string path)
        {
            return path.Equals(defpath1);
        }
        //właściwości tylko do odczytu
        public static string DefaultPath
        {
            get { return defpath1; }
        }
        public static string DefaultThumbPath
        {
            get { return defpath2; }
        }
     
        public static bool ValidType(string url)
        {
            string type = GetFileType(url).ToLower();
            bool result = true;
            string[] types = new string[]{".gif", ".jpg", ".jpeg", ".png", ".bmp"};
            foreach (var t in types)
            {
                if (type == t || url.Contains(t))
                {
                    result = true;
                    break;
                }                   
            }
            return result;
        }

        public static Bitmap CreateImage(Image img, int x, int y, double width, double height, int turn, bool mirror_hor, bool mirror_vert)
        {
            //transformacje jak w gimpie
            switch (turn)
            {
                  case 90:
                    img.RotateFlip(RotateFlipType.Rotate90FlipNone);
                    double t = width;
                    width = height;
                    height = t;
                    int tt = x;
                    x = y;
                    y = tt;
                    break;
                case 270:
                    img.RotateFlip(RotateFlipType.Rotate270FlipNone);
                    double t1 = width;
                    width = height;
                    height = t1;
                    int tt1 = x;
                    x = y;
                    y = tt1;
                    break;
                case 180:
                    img.RotateFlip(RotateFlipType.Rotate180FlipNone);
                    break;
                default:
                    break;
            }
            if (mirror_hor)
                img.RotateFlip(RotateFlipType.RotateNoneFlipX);
            if (mirror_vert)
                img.RotateFlip(RotateFlipType.RotateNoneFlipY);
            Rectangle cropRect = new Rectangle(Convert.ToInt32(x), Convert.ToInt32(y), Convert.ToInt32(width), Convert.ToInt32(height));
            Bitmap image = new Bitmap((int)width, (int)height);

            using (var g = Graphics.FromImage(image))
            {
                g.InterpolationMode = InterpolationMode.HighQualityBicubic;
                g.SmoothingMode = SmoothingMode.HighQuality;
                g.PixelOffsetMode = PixelOffsetMode.HighQuality;
                g.CompositingQuality = CompositingQuality.HighQuality;
                g.DrawImage(
                    img,
                    new Rectangle(0, 0, image.Width, image.Height),
                    cropRect,
                    GraphicsUnit.Pixel);
            }
            
            return image;
        }
        public static Bitmap ResizeImage(Image img, double width, double height)
        {
            Bitmap image = new Bitmap(Convert.ToInt32(width), Convert.ToInt32(height));
            //ten sposób umożliwia mozliwie najmniejszą utratę jakości
            using (var g = Graphics.FromImage(image))
            {
                g.InterpolationMode = InterpolationMode.HighQualityBicubic;
                g.SmoothingMode = SmoothingMode.HighQuality;
                g.PixelOffsetMode = PixelOffsetMode.HighQuality;
                g.CompositingQuality = CompositingQuality.HighQuality;
                g.DrawImage(
                    img,
                    0,
                    0,
                    image.Width,
                    image.Height);
            }
            return image;
        }
        public static string[] MapImagePaths(Picture picture, string username, string ext)
        {
            //nazwa pliku jest generowana
            string filename = GenerateFileName(15);
            string fpath="";
            string path="";
            string tpath="";
            if (ext.Equals(".gif"))
            {
                path = @"~/Images/" + username + "/" + filename + ext;
            }
            else
            {
                path = @"~/Images/" + username + "/" + filename + ".png";
            }
            tpath = @"~/Images/" + username + "/Thumbnails/" + filename + ".png";
            fpath = @"~/Images/" + username + "/FullSize/" + filename + ext;
            picture.Url = path;
            picture.FullSizeUrl = fpath;
            picture.ThumbnailUrl = tpath;
            return new string[] { fpath, path, tpath};

          
        }
        public static string TemporaryPath(string username, string ext)
        {
            //nazwa pliku jest generowana
            string filename = GenerateFileName(15);
            string path = "";
            path = @"~/Images/" + username + "/Temp/" + filename + ext;           
            return path;
        }
        public static string GetFileSize(Image image)
        {
            Image img = new Bitmap(image);
            int size;
            using (var ms = new MemoryStream(1024))
            {
                img.Save(ms, ImageFormat.Png);
                size = (int)ms.Length;
            }
            int size_kb = size / 1024;
            return size_kb + "KB";
        }
        public static string SetImageProperties(Picture picture, string path, string path2)
        {
            return MetadataHelper.SaveMetadata(Image.FromFile(path), picture, path2);
        }
        public static void GetImageProperties(string url, Picture picture)
        {
            Image image = new Bitmap(url);
            picture.Colours = MetadataHelper.GetColorSpace(image);
            picture.Software = MetadataHelper.GetStringPropertyItem(image, 305);
            picture.DateTaken = MetadataHelper.GetStringPropertyItem(image, 36867);
            picture.Flash = MetadataHelper.GetBoolPropertyItem(image, 37385);
            int? fl = MetadataHelper.GetInt32PropertyItem(image, 37386);
            if (fl != null)
                picture.FocalLength = (int)fl;
            picture.ExposureTime = MetadataHelper.GetPartialPropertyItem(image, 33434)+" s";
            picture.ShutterSpd = MetadataHelper.GetPartialPropertyItem(image, 37377)+" s";
            picture.CameraType = MetadataHelper.GetStringPropertyItem(image, 271) + " - " +MetadataHelper.GetStringPropertyItem(image, 272);
        }  
        
        public static void GetImageResolution(Image im, Picture picture)
        {
            picture.Height = im.Height;
            picture.Width = im.Width;
        }
        public static void DeleteImage(string path)
        {
            try
            {
                System.IO.File.Delete(path);
            }
            catch
            {
                return;
            }
        }
        public static void SaveImageProto(string url, string path)
        {
            WebClient client = new WebClient();
            client.DownloadFile(url, path);
            client.Dispose();
        }
        public static void SaveImageProto(HttpPostedFileBase file, string path)
        {
            file.SaveAs(path);
        }
        public static IEnumerable<SelectListItem> Sizes(Dictionary<int, string> sizes)
        {
                var all_sizes = sizes.Select(f => new SelectListItem
                {
                    Value = f.Key.ToString(),
                    Text = f.Value
                });
                return all_sizes;

        }
        #region helpers
        const string pl_alphabeth = "AĄBCDEĘFGHIJKLŁMNOÓPQRSŚTUVWXYZŹŻaąbcćdeęfghijklłmnoópqrsśtuvwxyzźż0123456789";
        private static string GenerateFileName(int length)
        {
            Random random = new Random();
            StringBuilder proto = new StringBuilder(length);
            for (int i = 0; i < length; i++)
            {
                int randomNumber = random.Next(0, pl_alphabeth.Length);
                proto.Append(pl_alphabeth, randomNumber, 1);
            }
            return proto.ToString();
        }
        #endregion
    }

}