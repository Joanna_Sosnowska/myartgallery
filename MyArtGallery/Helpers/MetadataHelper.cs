﻿using MyArtGallery.Models;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;

namespace MyArtGallery.Helpers
{
    public static class MetadataHelper
    {
        public static string GetStringPropertyItem(Image theImage, int i)
        {
            try
            {
                PropertyItem item = theImage.GetPropertyItem(i);
                Encoding ascii = Encoding.ASCII;
                return ascii.GetString(item.Value, 0, item.Len - 1);
            }
            catch
            {
                return "";
            }
        }
        public static string GetColorSpace(Image theImage)
        {
            try
            {
                PropertyItem item = theImage.GetPropertyItem(40961);
                int val = BitConverter.ToInt16(item.Value, 0);
                switch(val)
                {
                    case 0:
                    case 1:
                        return "Black and white";
                    case 2:
                        return "RGB";
                    case 3:
                        return "RGB Palette";
                    case 4:
                        return "Transparency Mask";
                    case 5:
                        return "CMYK";
                    case 6:
                        return "YCbCr";
                    case 8:
                        return "CIELab";
                    case 9:
                        return "ICCLab";
                    case 10:
                        return "ITULab";
                    case 32803:
                        return "Color Filter Array";
                    case 32844:
                        return "Pixar LogL";
                    case 32845:
                        return "Pixar LogLuv";
                    case 34892:
                        return "Linear Raw";
                    default:
                        return "Other";
                }
              
            }
            catch
            {
                return "";
            }
        }
        public static string SaveMetadata(Image image, Picture pic, string path)
        {
            Bitmap img = new Bitmap(image);
            image.Dispose();
            PropertyItem item = CreatePropertyItem();
            if(pic.Description!=null)
                SaveStringProperty(img, item, 270, pic.Description);
            if (pic.UserName != null)
            {
                SaveStringProperty(img, item, 315, pic.UserName);
                SaveStringProperty(img, item, 33432, pic.UserName);
            }
            if (pic.CameraType != null)
            {
                SaveStringProperty(img, item, 271, pic.CameraType.Split('-')[0]);
                SaveStringProperty(img, item, 272, pic.CameraType.Split('-')[1]);
            }
            if (pic.Software != null)
            {
                SaveStringProperty(img, item, 305, pic.Software);
            }
            if (pic.DateTaken != null)
            {
                SaveStringProperty(img, item, 36867, pic.DateTaken);
            }
            img.Save(path, ImageFormat.Jpeg);
            return path;

        }
        private static void SaveStringProperty(Image image, PropertyItem propertyItem, int prop_id, string propertyValue)
        {
            propertyValue += "X";
            var data = System.Text.Encoding.UTF8.GetBytes(propertyValue);
            data[data.Length - 1] = 0;
            propertyItem.Id =prop_id;
            propertyItem.Type = 2;
            propertyItem.Len = data.Length;
            propertyItem.Value = data;
            image.SetPropertyItem(propertyItem);
        }
       private static PropertyItem CreatePropertyItem()
       {
           ConstructorInfo cons = typeof(PropertyItem).GetConstructor(BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Public, null, new Type[0], null);        
           return (PropertyItem)cons.Invoke(null);

       }
        public static int? GetInt32PropertyItem(Image theImage, int i)
        {
            try
            {
                PropertyItem item = theImage.GetPropertyItem(i);
                return BitConverter.ToInt32(item.Value, 0);
            }
            catch
            {
                return null;
            }
        }
        public static string GetPartialPropertyItem(Image theImage, int i)
        {
            try
            {
                PropertyItem item = theImage.GetPropertyItem(i);
                byte[] value = item.Value;
                int numerator = BitConverter.ToInt32(value, 0);
                int denominator = BitConverter.ToInt32(value, 4);
                double partial = (double)numerator / (double)denominator;
                return numerator + "/" + denominator;
            }
            catch
            {
                return "";
            }
        }
        public static bool GetBoolPropertyItem(Image theImage, int i)
        {
            try
            {
                PropertyItem item = theImage.GetPropertyItem(i);
                //bierzemy tylko bit 0 - interesuje nas tylko czy użyto flesza czy nie - bez szczegółów
                string val = BitConverter.ToInt16(item.Value, 0).ToString();
                if (val == "1")
                    return true;
                else
                    return false;
            }
            catch
            {
                return false;
            }
        }
    }
}