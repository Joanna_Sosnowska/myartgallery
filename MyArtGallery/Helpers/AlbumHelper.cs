﻿using MyArtGallery.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MyArtGallery.Helpers
{
    public static class AlbumHelper
    {
        public static void RenameSubalbums(Album a)
        {
            foreach (Album item in a.SubAlbums)
            {
                item.Path = a.Path + "/" + item.Title;
                foreach (Album sub in item.SubAlbums)
                {
                    RenameSubalbums(sub);

                }

            }
        }
    }
}