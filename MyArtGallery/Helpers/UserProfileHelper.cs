﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Web;

namespace MyArtGallery.Helpers
{
    public static class UserProfileHelper
    {
        private static string def_avatarpath1 = "~/Content/Images/default_avatar.png";
        private static string def_avatarpath2 = "~/Content/Images/default_avatar_mini.png";
        public static string DefaultAvatarPath
        {
            get { return def_avatarpath1; }
        }
        public static string DefaultAvatarThumbPath
        {
            get { return def_avatarpath2; }
        }
        public static int CountUserAge(DateTime bdate)
        {
            DateTime now = DateTime.Now;
            int age = DateTime.Now.Year-bdate.Year;
            if (now.Month < bdate.Month || (now.Month == bdate.Month && now.Day < bdate.Day)) age--;
            return age;
        }
        public static string[] SetAvatarPath(Image img, string name)
        {
            string path1 = def_avatarpath1;
            string path2 = def_avatarpath2;
            if (img != null)
            {
                path1 = @"~/Images/Avatars/" + name+".png";
                path2 = @"~/Images/Avatars/" + name +"_thumb.png";
            }

            return new string[2] { path1, path2 };
        }

        const string chars = "ABCDEFGHIJKLMNOPRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
       
        public static string GeneratePassword()
        {
            Random random = new Random();
            Random r1 = new Random();
            int length = r1.Next(10, 16);
            StringBuilder proto = new StringBuilder(length);
            for (int i = 0; i < length; i++)
            {
                int randomNumber = random.Next(0, chars.Length);
                proto.Append(chars, randomNumber, 1);
            }
            return proto.ToString();
        }

    }
}