﻿using MyArtGallery.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace MyArtGallery.Models
{
    public enum NoteType
    {
        [Display(Name = "FavouritedNote", ResourceType = typeof(MessagesAndNotes))]
        Favourited = 1,
        [Display(Name = "CommentNote", ResourceType = typeof(MessagesAndNotes))]
        Comment,
        [Display(Name = "CommentRateNote", ResourceType = typeof(MessagesAndNotes))]
        CommentRated,
        [Display(Name = "ReplyNote", ResourceType = typeof(MessagesAndNotes))]
        CommentReply,
        [Display(Name = "EditedCommentNote", ResourceType = typeof(MessagesAndNotes))]
        EditedComment,
        [Display(Name = "DeletedCommentNote", ResourceType = typeof(MessagesAndNotes))]
        DeletedComment,
        [Display(Name = "DeletedImageNote", ResourceType = typeof(MessagesAndNotes))]
        DeletedImage,
        [Display(Name = "EditedImageNote", ResourceType = typeof(MessagesAndNotes))]
        EditedImage,
        [Display(Name = "RightsChanged", ResourceType = typeof(MessagesAndNotes))]
        RightsChanged
    };
    public class Notification
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int NotificationId { get; set; }
        //jakiego obrazka lub komentarza dotyczy
        public int ThingId { get; set; }
        //od kogo
        public string UserId { get; set; }
        //do kogo
        public string AddresseeId { get; set; }
        [StringLength(500)]
        [Display(Name = "Text", ResourceType = typeof(Resource))]
        public string Text { get; set; }
        [Display(Name = "MessageDate", ResourceType = typeof(MessagesAndNotes))]
        [DataType(DataType.Date)]
        public DateTime SentDate { get; set; }
        [Display(Name = "WasRead", ResourceType = typeof(MessagesAndNotes))]
        public bool WasRead { get; set; }
        public NoteType Kind { get; set; }
    }
}