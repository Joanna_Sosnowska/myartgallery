﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using MyArtGallery.Models.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MyArtGallery.Models
{
    public class MyArtGalleryInitializer : System.Data.Entity.DropCreateDatabaseIfModelChanges<MyArtGalleryContext>
    {
        protected override void Seed(MyArtGalleryContext context)
        {
            var categories = new List<Category>
            {
                new Category{CategoryId=1, CategoryName = "Fotografia", CategoryNameEn="Photography", CategoryDescription = ""},
                new Category{CategoryId=2, CategoryName = "Kolorowe", CategoryNameEn="Color", CategoryDescription = "", ParentId=1},
                new Category{CategoryId=3, CategoryName = "Czarno białe", CategoryNameEn="Black and white", CategoryDescription = "", ParentId=1},
                new Category{CategoryId=4, CategoryName = "Sepia", CategoryNameEn="Sepia", CategoryDescription = "", ParentId=1},
                new Category{CategoryId=5, CategoryName = "Fotomontaż", CategoryNameEn="Photomanipulation", CategoryDescription = "", ParentId=1},
                new Category{CategoryId=6, CategoryName = "Dokumentalne", CategoryNameEn="Photojournalism",  CategoryDescription = "", ParentId=1},

                new Category{CategoryId=7, CategoryName = "Sztuka tradycyjna", CategoryNameEn="Traditional art", CategoryDescription = ""},
                new Category{CategoryId=8, CategoryName = "Rysunek", CategoryNameEn="Drawings", CategoryDescription = "", ParentId=7},
                new Category{CategoryId=9, CategoryName = "Szkic", CategoryNameEn="Pencil sketches", CategoryDescription = "", ParentId=7},
                new Category{CategoryId=10, CategoryName = "Malarstwo", CategoryNameEn="Paintings", CategoryDescription = "", ParentId=7},
                new Category{CategoryId=11, CategoryName = "Rzeźba", CategoryNameEn="Sculptures", CategoryDescription = "", ParentId=7},
                new Category{CategoryId=12, CategoryName = "Media mieszane", CategoryNameEn="Mixed media", CategoryDescription = "", ParentId=7},
                new Category{CategoryId=13, CategoryName = "Scratchboard", CategoryNameEn="Scratchboard", CategoryDescription = "", ParentId=7},
                new Category{CategoryId=14, CategoryName = "Kompozycje", CategoryNameEn="Assemblages", CategoryDescription = "", ParentId=7},
         
                new Category{CategoryId=15, CategoryName = "Nowe media", CategoryNameEn="Digital art", CategoryDescription = ""},
                new Category{CategoryId=16, CategoryName = "Grafika rastrowa", CategoryNameEn="Pixel art", CategoryDescription = "", ParentId=15},
                new Category{CategoryId=17, CategoryName = "Grafika wektorowa", CategoryNameEn="Vector", CategoryDescription = "", ParentId=15},
                new Category{CategoryId=18, CategoryName = "3D", CategoryNameEn="3D", CategoryDescription = "", ParentId=15},
                new Category{CategoryId=19, CategoryName = "Animacja", CategoryNameEn="Animation", CategoryDescription = "", ParentId=15},
                new Category{CategoryId=20, CategoryName = "Fraktale", CategoryNameEn="Fractal art", CategoryDescription = "", ParentId=15},
                new Category{CategoryId=21, CategoryName = "HDR", CategoryNameEn="HDR", CategoryDescription = "", ParentId=15},
                new Category{CategoryId=22, CategoryName = "Fotomanipulacja", CategoryNameEn="Photomanipulation", CategoryDescription = "", ParentId=15},
                new Category{CategoryId=23, CategoryName = "Typografia", CategoryNameEn="Typography", CategoryDescription = "", ParentId=15},
                new Category{CategoryId=24, CategoryName = "Tapety na pulpit", CategoryNameEn="Wallpapers", CategoryDescription = "", ParentId=15},
          
                new Category{CategoryId=25, CategoryName = "Tekstylia", CategoryNameEn="Textiles", CategoryDescription = ""},
                new Category{CategoryId=26, CategoryName = "Haft", CategoryNameEn="Embroidery", CategoryDescription = "", ParentId=25},
                new Category{CategoryId=27, CategoryName = "Haft krzyżykowy", CategoryNameEn="Cross stitch", CategoryDescription = "", ParentId=25},
                new Category{CategoryId=28, CategoryName = "Tkactwo", CategoryNameEn="Weaving", CategoryDescription = "", ParentId=25},
                new Category{CategoryId=29, CategoryName = "Szydełkowanie", CategoryNameEn="Crochet", CategoryDescription = "", ParentId=25},
                new Category{CategoryId=30, CategoryName = "Robienie na drutach", CategoryNameEn="Knitting", CategoryDescription = "", ParentId=25},
                

                new Category{CategoryId=31, CategoryName = "Ubiory", CategoryNameEn="Clothing", CategoryDescription = ""},
                new Category{CategoryId=32, CategoryName = "Przebieranki", CategoryNameEn="Cosplay", CategoryDescription = "", ParentId=31},
                new Category{CategoryId=33, CategoryName = "Historyczne", CategoryNameEn="Historical", CategoryDescription = "", ParentId=31},
                new Category{CategoryId=34, CategoryName = "Nadruk na odzieży", CategoryNameEn="Clothing prints", CategoryDescription = "", ParentId=31},
                new Category{CategoryId=35, CategoryName = "Dodatki", CategoryNameEn="Accessories", CategoryDescription = "", ParentId=31},
               

                new Category{CategoryId=36, CategoryName = "Wyroby z metalu", CategoryNameEn="Metalwork", CategoryDescription = ""},
                new Category{CategoryId=37, CategoryName = "Rzeźba", CategoryNameEn="Sculpture", CategoryDescription = "", ParentId=36},
                new Category{CategoryId=38, CategoryName = "Wyroby dekoracyjne", CategoryNameEn="Decorative pieces", CategoryDescription = "", ParentId=36},
                new Category{CategoryId=39, CategoryName = "Użytkowe", CategoryNameEn="Utitities", CategoryDescription = "", ParentId=36},
                new Category{CategoryId=40, CategoryName = "Obrazy", CategoryNameEn="Images", CategoryDescription = "", ParentId=36},


                new Category{CategoryId=41, CategoryName = "Szkło", CategoryNameEn="Glass", CategoryDescription = ""},
                new Category{CategoryId=42, CategoryName = "Zastawa stołowa", CategoryNameEn="Tableware", CategoryDescription = "", ParentId=41},
                new Category{CategoryId=43, CategoryName = "Malarstwo na szkle", CategoryNameEn="Glass paintings", CategoryDescription = "", ParentId=41},
                new Category{CategoryId=44, CategoryName = "Ozdoby", CategoryNameEn="Ornaments", CategoryDescription = "", ParentId=41},
                new Category{CategoryId=45, CategoryName = "Lampy", CategoryNameEn="Lights", CategoryDescription = "", ParentId=41},
                new Category{CategoryId=46, CategoryName = "Witraż", CategoryNameEn="Stained glass", CategoryDescription = "", ParentId=41},
                
                new Category{CategoryId=47, CategoryName = "Malarstwo naścienne", CategoryNameEn="Wall paintings", CategoryDescription = ""},
                new Category{CategoryId=48, CategoryName = "Wewnętrzne", CategoryNameEn="Interior", CategoryDescription = "", ParentId=47},
                new Category{CategoryId=49, CategoryName = "Zewnętrzne", CategoryNameEn="Exterior", CategoryDescription = "", ParentId=47},
                new Category{CategoryId=50, CategoryName = "Sztuka uliczna", CategoryNameEn="Streetart", CategoryDescription = "", ParentId=47},
                
                new Category{CategoryId=51, CategoryName = "Wyroby drewniane", CategoryNameEn="Woodwork", CategoryDescription = ""},
                
                new Category{CategoryId=52, CategoryName = "Rzeźba", CategoryNameEn="Sculpture", CategoryDescription = "", ParentId=51},
                new Category{CategoryId=53, CategoryName = "Dekoracja wnętrz", CategoryNameEn="Interior decoration",  CategoryDescription = "", ParentId=51},
                new Category{CategoryId=54, CategoryName = "Meble", CategoryNameEn="Furniture", CategoryDescription = "", ParentId=51},
                new Category{CategoryId=55, CategoryName = "Inne", CategoryNameEn="Other", CategoryDescription = "", ParentId=51},
                
                new Category{CategoryId=56, CategoryName = "Ceramika", CategoryNameEn="Ceramics", CategoryDescription = ""},
                new Category{CategoryId=57, CategoryName = "Garncarstwo", CategoryNameEn="Pottery", CategoryDescription = "", ParentId=56},
                new Category{CategoryId=58, CategoryName = "Zastawa stołowa", CategoryNameEn="Tableware", CategoryDescription = "", ParentId=56},

                new Category{CategoryId=59, CategoryName = "Biżuteria", CategoryNameEn="Jewelry", CategoryDescription = ""},
                new Category{CategoryId=60, CategoryName = "Zawieszki", CategoryNameEn="Pendants and charms", CategoryDescription = "", ParentId=59},
                new Category{CategoryId=61, CategoryName = "Przypinki i broszki", CategoryNameEn="Pins and brooches", CategoryDescription = "", ParentId=59},
                new Category{CategoryId=62, CategoryName = "Kolczyki", CategoryNameEn="Earrings", CategoryDescription = "", ParentId=59},
                new Category{CategoryId=63, CategoryName = "Ozdoby do włosów", CategoryNameEn="Hair accessories and headpieces", CategoryDescription = "", ParentId=59},
                new Category{CategoryId=64, CategoryName = "Pierścionki", CategoryNameEn="Rings", CategoryDescription = "", ParentId=59},
                new Category{CategoryId=65, CategoryName = "Bransoletki", CategoryNameEn="Bracelets and anklets", CategoryDescription = "", ParentId=59},
                new Category{CategoryId=66, CategoryName = "Zestawy", CategoryNameEn="Sets", CategoryDescription = "", ParentId=59},
                
                
                new Category{CategoryId=67, CategoryName = "Reklama i plakaty", CategoryNameEn="Posters and advertising", CategoryDescription = ""},

                new Category{CategoryId=68, CategoryName = "Plecionki", CategoryNameEn="Weavings", CategoryDescription = ""},
                new Category{CategoryId=69, CategoryName = "Koszyki", CategoryNameEn="Basketry", CategoryDescription = "", ParentId=68},
                
                new Category{CategoryId=70, CategoryName = "Wyroby z papieru", CategoryNameEn="Paperwork", CategoryDescription = ""},
                new Category{CategoryId=71, CategoryName = "Wycinanki", CategoryNameEn="Papercutting", CategoryDescription = "", ParentId=70},
                new Category{CategoryId=72, CategoryName = "Origami", CategoryNameEn="Origami", CategoryDescription = "", ParentId=70},
                new Category{CategoryId=73, CategoryName = "Modele", CategoryNameEn="Models", CategoryDescription = "", ParentId=70},


                new Category{CategoryId=74, CategoryName = "Zabawki", CategoryNameEn="Toys", CategoryDescription = ""},
                new Category{CategoryId=75, CategoryName = "Lalki", CategoryNameEn="Dolls", CategoryDescription = "", ParentId=74},
                new Category{CategoryId=76, CategoryName = "Pluszaki", CategoryNameEn="Plushies", CategoryDescription = "", ParentId=74},

                
                new Category{CategoryId=77, CategoryName = "Wzornictwo", CategoryNameEn="Design", CategoryDescription = ""},
                new Category{CategoryId=78, CategoryName = "Ubiory", CategoryNameEn="Fashion design", CategoryDescription = "", ParentId=77},
                new Category{CategoryId=79, CategoryName = "Architektura", CategoryNameEn="Architecture", CategoryDescription = "", ParentId=77},
                new Category{CategoryId=80, CategoryName = "Architektura wnętrz", CategoryNameEn="Interior design",  CategoryDescription = "", ParentId=77},
                new Category{CategoryId=81, CategoryName = "Inne", CategoryNameEn="Other", CategoryDescription = "", ParentId=77},

                new Category{CategoryId=82, CategoryName = "Niedokończone lub w trakcie tworzenia", CategoryNameEn="Works in progress", CategoryDescription = ""},

                new Category{CategoryId=83, CategoryName = "Miniatury", CategoryNameEn="Miniatures", CategoryDescription = ""},

                new Category{CategoryId=84, CategoryName = "Inne rodzaje sztuki", CategoryNameEn="Other arts", CategoryDescription = ""},




            };
          


            var tags = new List<Tag>
            {
                new Tag{Name="ludzie", Pictures=new List<Picture>{}},
                new Tag{Name="portret", Pictures=new List<Picture>{}},
                new Tag{Name="natura", Pictures=new List<Picture>{}},
                new Tag{Name="martwa natura", Pictures=new List<Picture>{}},
                new Tag{Name="zwierzęta", Pictures=new List<Picture>{}},
                new Tag{Name="rośliny", Pictures=new List<Picture>{}},
                new Tag{Name="pojazdy", Pictures=new List<Picture>{}},
                new Tag{Name="morze", Pictures=new List<Picture>{}},
                new Tag{Name="góry", Pictures=new List<Picture>{}},
                new Tag{Name="las", Pictures=new List<Picture>{}},
                new Tag{Name="koty", Pictures=new List<Picture>{}},
                new Tag{Name="literatura", Pictures=new List<Picture>{}},
                new Tag{Name="Fan Art", Pictures=new List<Picture>{}},
                new Tag{Name="ilustracja", Pictures=new List<Picture>{}},
                new Tag{Name="humor", Pictures=new List<Picture>{}},
                new Tag{Name="fantasy", Pictures=new List<Picture>{}},
                new Tag{Name="mitologia", Pictures=new List<Picture>{}},
                new Tag{Name="surrealizm", Pictures=new List<Picture>{}},
                new Tag{Name="abstrakcja", Pictures=new List<Picture>{}},
                new Tag{Name="architektura", Pictures=new List<Picture>{}},
                new Tag{Name="tutorial", Pictures=new List<Picture>{}},
                new Tag{Name="lekcja", Pictures=new List<Picture>{}},
                new Tag{Name="krajobraz", Pictures=new List<Picture>{}},
                new Tag{Name="miasto", Pictures=new List<Picture>{}},

            };
            categories.ForEach(c => context.Categories.Add(c));

            tags.ForEach(t => context.Tags.Add(t));
            context.SaveChanges();
            var UserManager = new UserManager<UserProfile>(new UserStore<UserProfile>(context));
            var RoleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));
            var role = new IdentityRole() {  Name="Administrator"};
            var role1 = new IdentityRole() { Name = "Moderator" };
            var role2 = new IdentityRole() { Name = "User" };
            var role3 = new IdentityRole() { Name = "Banned" };
            RoleManager.Create(role);
            RoleManager.Create(role1);
            RoleManager.Create(role2);
            RoleManager.Create(role3);

            string name = "Admin";
            string password = "florentynka11";
            var user = new UserProfile() { UserName = name, BirthDate = new DateTime(1992, 02, 18), Email = "kotka5351@gmail.com", AccessFailedCount = 0, Albums = new List<Album> { }, Country = "Poland", Gender = Gender.F, Genres = new List<Category> { }, Comments = new List<Comment> { }, Favourites = new List<Favourite> { }, Rank = Rank.Admin, UserDescription = "", AvatarPath=ImageHelper.DefaultPath, AvatarThumbPath=ImageHelper.DefaultThumbPath };
            var adminresult = UserManager.Create(user, password);
            var u = UserManager.FindByName("Admin");
            UserManager.AddToRole(u.Id, "Administrator");
            var album = new Album { UserName = user.UserName, Description = "Admins photos", AddedDate = DateTime.Now, LastModifiedDate = DateTime.Now, Path = user.UserName, SubAlbums = new List<Album> { }, Pictures = new List<Picture> { }, Public=true, Title="Default"  };
            context.SaveChanges();
            context.Albums.Add(album);
         
            context.SaveChanges();
        }
    }
}