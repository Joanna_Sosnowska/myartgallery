﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;

namespace MyArtGallery.Models
{
    public class MyArtGalleryContext : IdentityDbContext<UserProfile>
    {

        public MyArtGalleryContext()
            : base("MyArtGalleryContext")
        {
        }

        public DbSet<Album> Albums { get; set; }
        public DbSet<Picture> Pictures { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Comment> Comments { get; set; }
        public DbSet<Message> Messages { get; set; }
        public DbSet<Notification> Notifications { get; set; }
        public DbSet<Tag> Tags { get; set; }
        public DbSet<Favourite> Favourites { get; set; }
        public DbSet<Protest> Protests { get; set; }
    }
}