﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using MyArtGallery.Resources;

namespace MyArtGallery.Models
{
    public class Comment
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int CommentId { get; set; }
        [ForeignKey("Picture")]
        public int PictureId { get; set; }
        [ForeignKey("Parent")]
        public int? ParentId { get; set; }
        public virtual Comment Parent { get; set; }
        public virtual Picture Picture { get; set; }
        [ForeignKey("UserProfile")]
        public string UserName { get; set; }
        [Display(Name = "Author", ResourceType = typeof(Resource))]
        public virtual UserProfile UserProfile{ get; set; }
        [Display(Name = "Comment", ResourceType = typeof(CommentOptions))]
        [StringLength(500, ErrorMessageResourceType = typeof(CommentOptions),
              ErrorMessageResourceName = "CommentLong")]
        public string Text { get; set; }
        [Display(Name = "Added", ResourceType = typeof(Resource))]
        [DataType(DataType.Date)]
        public DateTime AddedDate { get; set; }
        [Display(Name = "LastModified", ResourceType = typeof(Resource))]
        [DataType(DataType.Date)]
        public DateTime EditedDate { get; set; }
        public virtual ICollection<Comment> Replies{ get; set; }
        [Display(Name = "Good", ResourceType = typeof(CommentOptions))]
        public int Good { get; set; }
        [Display(Name = "Bad", ResourceType = typeof(CommentOptions))]
        public int Bad { get; set; }

    }
}