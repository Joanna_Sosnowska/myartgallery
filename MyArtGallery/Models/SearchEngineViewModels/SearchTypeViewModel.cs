﻿using MyArtGallery.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MyArtGallery.Models.SearchEngineViewModels
{
    public enum SearchType
    {
        [Display(Name = "BasicSearch", ResourceType = typeof(SearchEngine))]
        Basic=1,
        [Display(Name = "AdvancedSearch", ResourceType = typeof(SearchEngine))]
        Advanced=2

    }
    public enum SearchRange
    {
        [Display(Name = "Pictures", ResourceType = typeof(Resource))]
        Profiles = 1,
        [Display(Name = "Profiles", ResourceType = typeof(Resource))]
        Pictures = 2

    }
    public class SearchTypeViewModel
    {
        //enum - nie bool bo rodzajów wyszukiwania mogłoby być więcej niż 2
        [Display(Name = "SearchType", ResourceType = typeof(SearchEngine))]
        public SearchType SearchType { get; set; }
        [Display(Name = "SearchRange", ResourceType = typeof(SearchEngine))]
        public SearchRange SearchRange { get; set; }
        [Display(Name = "Phrase", ResourceType = typeof(SearchEngine))]
        public string Phrase { get; set; }
    }
}