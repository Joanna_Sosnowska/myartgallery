﻿using MyArtGallery.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MyArtGallery.Models.SearchEngineViewModels
{
    public class SearchImagesViewModel
    {
        [Display(Name = "Title", ResourceType = typeof(Resource))]
        public string  Title { get; set; }
        [Display(Name = "Description", ResourceType = typeof(Resource))]
        public string Description { get; set; }
        [Display(Name = "Keywords", ResourceType = typeof(Resource))]
        public string Tags { get; set; }
        [Display(Name = "Author", ResourceType = typeof(Resource))]
        public string Author { get; set; }
        [Display(Name = "Category", ResourceType = typeof(ImageOptions))]
        public int CategoryId { get; set; }
        public IEnumerable<SelectListItem> Categories { get; set; }
        [Display(Name = "AllCategories", ResourceType = typeof(SearchEngine))]
        public bool AllCategories { get; set; }
        


    }
}