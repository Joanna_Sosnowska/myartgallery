﻿using MyArtGallery.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MyArtGallery.Models.SearchEngineViewModels
{
    public class SearchProfilesViewModel
    {
        [Display(Name = "UserName", ResourceType = typeof(UserAccount))]
        public string UserName { get; set; }
        [Display(Name = "Genres", ResourceType = typeof(UserAccount))]
        public IEnumerable<SelectListItem> Genres { get; set; }
        public List<int> SelectedGenres { get; set; }
    }
}