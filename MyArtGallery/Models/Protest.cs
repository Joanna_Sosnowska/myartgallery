﻿using MyArtGallery.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace MyArtGallery.Models
{
    public enum ProtestType
    {
        Picture = 1,
        Comment
    };
    public class Protest
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ProtestId { get; set; }
        //jakiego obrazka lub komentarza dotyczy
        public int ThingId { get; set; }
        //od kogo
        public string UserId { get; set; }
        [Required(ErrorMessageResourceName = "DescriptionRequired", ErrorMessageResourceType = typeof(Resource))]       
        [StringLength(500, ErrorMessageResourceName="DescriptionLong", ErrorMessageResourceType=typeof(Resource))]
        [Display(Name = "Text", ResourceType = typeof(Resource))]
        public string Description { get; set; }
        [Display(Name = "ProtestDate", ResourceType = typeof(MessagesAndNotes))]
        [DataType(DataType.Date)]
        public DateTime SentDate { get; set; }
        [Display(Name = "Verified", ResourceType = typeof(MessagesAndNotes))]
        public bool Verified { get; set; }
        public ProtestType Kind { get; set; }
    }
}