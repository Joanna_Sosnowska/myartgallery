﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using MyArtGallery.Resources;

namespace MyArtGallery.Models
{
    public class Category
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int CategoryId { get; set; }
        [ForeignKey("Parent")]
        public int? ParentId { get; set; }
        public virtual Category Parent { get; set; }
        [Display(Name = "CategoryName", ResourceType = typeof(Resource))]
        public String CategoryName { get; set; }
        [Display(Name = "CategoryName", ResourceType = typeof(Resource))]
        public String CategoryNameEn { get; set; }
        [Display(Name = "CategoryDescription", ResourceType = typeof(CategoryOptions))]
        [StringLength(300, ErrorMessageResourceType = typeof(CategoryOptions),
              ErrorMessageResourceName = "CategoryDescLong")]
        public String CategoryDescription { get; set; }
        [Display(Name = "CategoryImages", ResourceType = typeof(CategoryOptions))]
        public virtual ICollection<Picture> Pictures{ get; set; }
        [Display(Name = "SubCats", ResourceType = typeof(CategoryOptions))]
        public virtual ICollection<Category> SubCategories { get; set; }
        public virtual ICollection<UserProfile> UserProfiles { get; set; }
    }
}