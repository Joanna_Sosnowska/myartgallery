﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using MyArtGallery.Resources;
using Microsoft.AspNet.Identity.EntityFramework;
using System.ComponentModel;

namespace MyArtGallery.Models
{
    //to jest tylko informacja o roli 
    public enum Rank{
        Admin = 1,
        Moderator,
        User,
        Banned
    };
    public enum Gender
    {
        [Display(Name="♂")]
        M=1,
        [Display(Name="♀")]
        F,
        [Display(Name="-")]
        N
    };
    public class UserProfile:IdentityUser
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int UserId { get; set; }
        [Key]
        [Display(Name = "UserName", ResourceType = typeof(UserAccount))]
        public override string UserName { get; set; }
        [Display(Name = "UserDescription", ResourceType = typeof(UserAccount))]
        public string UserDescription { get; set; }
        [Display(Name = "Password", ResourceType = typeof(UserAccount))]
        [DataType(DataType.Password)]
        public string Password { get; set; }
        [Display(Name = "BirthDate", ResourceType = typeof(UserAccount))]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = false)]
        [DataType(DataType.Date, ErrorMessageResourceType = typeof(UserAccount), ErrorMessageResourceName = "BirthDateError")]
        public DateTime BirthDate { get; set; }
        [Display(Name = "Active", ResourceType = typeof(UserAccount))]
        public bool Active { get; set; }
        public Rank Rank { get; set; }
        [Display(Name = "Gender", ResourceType = typeof(UserAccount))]
        public Gender Gender { get; set; }
        [Display(Name = "Country", ResourceType = typeof(UserAccount))]
        public string Country{ get; set; }
        [Display(Name = "Avatar", ResourceType = typeof(UserAccount))]
        public string AvatarPath { get; set; }
        public string AvatarThumbPath { get; set; }
        [Display(Name = "Genres", ResourceType = typeof(UserAccount))]
        public virtual ICollection<Category> Genres { get; set; }
        [Display(Name = "Favourites", ResourceType = typeof(UserAccount))]
        public virtual ICollection<Favourite> Favourites { get; set; }
        [Display(Name = "UserAlbums", ResourceType = typeof(UserAccount))]
        public virtual ICollection<Album> Albums { get; set; }
        [Display(Name = "Comments", ResourceType = typeof(UserAccount))]
        public virtual ICollection<Comment> Comments { get; set; }

    }
}