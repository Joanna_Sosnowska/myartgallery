﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using MyArtGallery.Resources;

namespace MyArtGallery.Models
{
    public class Album
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int AlbumId { get; set; }
        [Display(Name = "Title", ResourceType = typeof(Resource))]
        [StringLength(150, ErrorMessageResourceType = typeof(GalleryOptions),
          ErrorMessageResourceName = "AlbumTitleLong")]
        public string Title { get; set; }
        [ForeignKey("UserProfile")]
        public string UserName { get; set; }
        [Display(Name = "Author", ResourceType = typeof(Resource))]
        public virtual UserProfile UserProfile{ get; set; }
        [StringLength(250, ErrorMessageResourceType = typeof(GalleryOptions),
              ErrorMessageResourceName = "AlbumDescLong")]
        [Display(Name = "Description", ResourceType = typeof(Resource))]
        public string Description { get; set; }
        [Display(Name = "Added", ResourceType = typeof(Resource))]
        [DataType(DataType.Date)]
        public DateTime AddedDate { get; set; }
        [Display(Name = "LastModified", ResourceType = typeof(Resource))]
        [DataType(DataType.Date)]
        public DateTime LastModifiedDate { get; set; }
        public string Path { get; set; }
        public bool Public { get; set; }

        public string CoverUrl { get; set; }
        [ForeignKey("Parent")]
        public int? ParentId { get; set; }
        [Display(Name = "ParentPath", ResourceType = typeof(GalleryOptions))]
        public virtual Album Parent { get; set; }
        public virtual ICollection<Picture> Pictures { get; set; }
        public virtual ICollection<Album> SubAlbums { get; set; }
        
    }
}