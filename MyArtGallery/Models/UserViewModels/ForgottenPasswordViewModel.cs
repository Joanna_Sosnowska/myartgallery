﻿using MyArtGallery.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MyArtGallery.Models
{
    public class ForgottenPasswordViewModel
    {
        [Display(Name = "UserName", ResourceType = typeof(UserAccount))]
        [Required(ErrorMessageResourceType = typeof(UserAccount),
              ErrorMessageResourceName = "UserNameRequired")]
        public string UserName { get; set; }
        [Display(Name = "Email", ResourceType = typeof(UserAccount))]
        [Required(ErrorMessageResourceType = typeof(UserAccount),
              ErrorMessageResourceName = "EmailRequired")]
        public string UserEmail { get; set; }
    }
}