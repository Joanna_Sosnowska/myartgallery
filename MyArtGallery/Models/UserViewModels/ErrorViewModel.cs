﻿using MyArtGallery.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MyArtGallery.Models
{
    public class ErrorViewModel
    {
        [Display(Name = "Error", ResourceType = typeof(Resource))]
        public string Message { get; set; }
    }
}