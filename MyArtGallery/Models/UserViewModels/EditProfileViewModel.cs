﻿using MyArtGallery.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MyArtGallery.Models.UserViewModels
{
    public class EditProfileViewModel
    {
        [Display(Name = "UserName", ResourceType = typeof(UserAccount))]
        [Required(ErrorMessageResourceType = typeof(UserAccount),
              ErrorMessageResourceName = "UserNameRequired")]
        [StringLength(100, ErrorMessageResourceType = typeof(UserAccount),
              ErrorMessageResourceName = "UserNameLong")]
        public string UserName { get; set; }
        //email ma mieć format emaila - podasz fake - masz problem (przejdzie, ale przy ewentualnym newsletterze nie będziesz dostawał wiadomości)
        [RegularExpression(@"^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$", ErrorMessageResourceType = typeof(UserAccount), ErrorMessageResourceName = "InvalidEmail")]
        [Display(Name = "Email", ResourceType = typeof(UserAccount))]
        public string Email { get; set; }

        [Display(Name = "UserDescription", ResourceType = typeof(UserAccount))]
        [StringLength(100, ErrorMessageResourceType = typeof(UserAccount),
              ErrorMessageResourceName = "UserDescriptionLong")]
        public string UserDescription { get; set; }

        [Display(Name = "BirthDate", ResourceType = typeof(UserAccount))]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-mm-dd}")]
        [DataType(DataType.DateTime)]
        public DateTime? BirthDate { get; set; }


        [Display(Name = "Country", ResourceType = typeof(UserAccount))]
        public string Country { get; set; }

        [Display(Name = "Gender", ResourceType = typeof(UserAccount))]
        public Gender Gender { get; set; }

        [Display(Name = "ImgFromDisk", ResourceType = typeof(ImageEditorRes))]
        public bool IsFile { get; set; }
        [Display(Name = "File", ResourceType = typeof(Resource))]
        public HttpPostedFileBase File { get; set; }

        [Display(Name = "ImgFromUrl", ResourceType = typeof(ImageEditorRes))]
        public bool IsUrl { get; set; }
        [Display(Name = "Url", ResourceType = typeof(ImageEditorRes))]
        //[RegularExpression(@"^$|.*\b(gif|jpg|jpeg|png|bmp|tif|tiff|GIF|JPG|JPEG|PNG|BMP|TIF|TIFF)\b.*$ ", ErrorMessageResourceType = typeof(ImageEditorRes), ErrorMessageResourceName = "NotAnImage")]
        public string Url { get; set; }
        public string AvatarPath { get; set; }

        [Display(Name = "RemoveAvatar", ResourceType = typeof(UserAccount))]
        public bool RemoveAvatar { get; set; }

        [Display(Name = "Genres", ResourceType = typeof(UserAccount))]
        public IEnumerable<SelectListItem> Genres { get; set; }
        public List<int> SelectedGenres { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public int X { get; set; }
        public int Y { get; set; }
    }
}