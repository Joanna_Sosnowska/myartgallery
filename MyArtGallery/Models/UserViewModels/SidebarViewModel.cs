﻿using MyArtGallery.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MyArtGallery.Models
{
    public class SidebarViewModel
    {
        public bool NewMessages { get; set; }
        public bool NewNotifications { get; set; }
        public int MessageCount { get; set; }
        public int NotificationCount { get; set; }
        public bool IsModOrAdmin { get; set; }
    }
}