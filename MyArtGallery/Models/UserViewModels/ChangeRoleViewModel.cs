﻿using MyArtGallery.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MyArtGallery.Models
{
    public class ChangeRoleViewModel
    {
        [Display(Name = "UserName", ResourceType = typeof(UserAccount))]
        public string UserName { get; set; }
        [Display(Name = "UserRole", ResourceType = typeof(UserAccount))]
        public Rank Role { get; set; }

    }
}