﻿using MyArtGallery.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MyArtGallery.Models
{
    public class LoginViewModel
    {
        public string Url { get; set; }
        public string Message { get; set; }
        [Display(Name = "UserName", ResourceType = typeof(UserAccount))]
        public string UserName { get; set; }

        [Display(Name = "Password", ResourceType = typeof(UserAccount))]
        [Required(ErrorMessageResourceType = typeof(UserAccount),
              ErrorMessageResourceName = "PasswordRequired")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Display(Name = "RememberMe", ResourceType = typeof(UserAccount))]
        public bool RememberMe { get; set; }
    }
}