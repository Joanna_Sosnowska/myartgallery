﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MyArtGallery.Models.UserViewModels
{
    public class MessageTypeViewModel
    {
        public NoteType Type { get; set; }
    }
}