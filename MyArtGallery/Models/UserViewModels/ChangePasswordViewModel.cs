﻿using MyArtGallery.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MyArtGallery.Models
{
    public class ChangePasswordViewModel
    {
        [Required(ErrorMessageResourceType=typeof(UserAccount), ErrorMessageResourceName="PasswordRequired")]
        [DataType(DataType.Password)]
        [Display(Name = "Password", ResourceType = typeof(UserAccount))]
        public string OldPassword { get; set; }

        [Display(Name = "NewPassword", ResourceType = typeof(UserAccount))]
        [Required(ErrorMessageResourceType = typeof(UserAccount),
              ErrorMessageResourceName = "NewPasswordRequired")]
        [RegularExpression(@"(^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,50}$)", ErrorMessageResourceType = typeof(UserAccount), ErrorMessageResourceName = "PasswordWeak")]
        [DataType(DataType.Password)]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "NewPasswordConfirm", ResourceType = typeof(UserAccount))]
        [Compare("NewPassword", ErrorMessageResourceType = typeof(UserAccount), ErrorMessageResourceName = "PasswordConfirmError")]
        public string ConfirmPassword { get; set; }
    }
}