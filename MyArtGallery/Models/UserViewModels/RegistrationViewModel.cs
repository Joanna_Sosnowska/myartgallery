﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using MyArtGallery.Resources;
using System.Web.Mvc;
using MyArtGallery.Helpers;
//using System.Web.Mvc;

namespace MyArtGallery.Models
{
    public class RegistrationViewModel
    {
        [Display(Name = "UserName", ResourceType = typeof(UserAccount))]
        [Required(ErrorMessageResourceType = typeof(UserAccount),
              ErrorMessageResourceName = "UserNameRequired")]
        [StringLength(100, ErrorMessageResourceType = typeof(UserAccount),
              ErrorMessageResourceName = "UserNameLong")]
        public string UserName { get; set; }

        [Display(Name = "Email", ResourceType = typeof(UserAccount))]
        [Required(ErrorMessageResourceType = typeof(UserAccount),
              ErrorMessageResourceName = "EmailRequired")]
        //najlepiej działający ze znalezionych regexów - trzeba jeszcze dodać listę domen i dodać dodatkową walidację
        [RegularExpression(@"^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$", ErrorMessageResourceType = typeof(UserAccount), ErrorMessageResourceName = "InvalidEmail")]
        public string Email { get; set; }

        [Display(Name = "Password", ResourceType = typeof(UserAccount))]
        [Required(ErrorMessageResourceType = typeof(UserAccount),
              ErrorMessageResourceName = "PasswordRequired")]
        [RegularExpression(@"(^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,50}$)", ErrorMessageResourceType = typeof(UserAccount), ErrorMessageResourceName = "PasswordWeak")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "PasswordConfirm", ResourceType = typeof(UserAccount))]
        [System.ComponentModel.DataAnnotations.Compare("Password", ErrorMessageResourceType = typeof(UserAccount), ErrorMessageResourceName = "PasswordConfirmError")]
        public string ConfirmPassword { get; set; }

        [Display(Name = "UserDescription", ResourceType = typeof(UserAccount))]
        [StringLength(250, ErrorMessageResourceType = typeof(UserAccount),
              ErrorMessageResourceName = "UserDescriptionLong")]
        public string UserDescription { get; set; }

        [Display(Name = "BirthDate", ResourceType = typeof(UserAccount))]
        [Required(ErrorMessageResourceType = typeof(UserAccount),
              ErrorMessageResourceName = "BirthDateRequired")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        [DataType(DataType.DateTime)]
        public DateTime? BirthDate { get; set; }


        [Display(Name = "Country", ResourceType = typeof(UserAccount))]
        [Required(ErrorMessageResourceType = typeof(UserAccount),
              ErrorMessageResourceName = "CountryRequired")]
        public string Country { get; set; }

        [Display(Name = "Gender", ResourceType = typeof(UserAccount))]
        public Gender Gender { get; set; }

        [Display(Name = "ImgFromDisk", ResourceType = typeof(ImageEditorRes))]
        public bool IsFile { get; set; }
        [Display(Name = "File", ResourceType = typeof(Resource))]
        public HttpPostedFileBase File { get; set; }

        [Display(Name = "ImgFromUrl", ResourceType = typeof(ImageEditorRes))]
        public bool IsUrl { get; set; }
        [Display(Name = "Url", ResourceType = typeof(ImageEditorRes))]
        //[RegularExpression(@"^$|.*\b(gif|jpg|jpeg|png|bmp|tif|tiff|GIF|JPG|JPEG|PNG|BMP|TIF|TIFF)\b.*$ ", ErrorMessageResourceType = typeof(ImageEditorRes), ErrorMessageResourceName = "NotAnImage")]
        public string Url { get; set; }

        [Display(Name = "Genres", ResourceType = typeof(UserAccount))]
        public IEnumerable<SelectListItem> Genres { get; set; }
        public List<int> SelectedGenres { get; set; }

        public int A { get; set; }
        public int B { get; set; }
        [Display(Name = "Result", ResourceType = typeof(Resource))]
        public int Result { get; set; }
        public int Answer { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public int X { get; set; }
        public int Y { get; set; }

    }
}