﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace MyArtGallery.Models
{
    public class Favourite
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int FavouriteId { get; set; }
        [ForeignKey("User")]
        public string UserName { get; set; }
        public virtual UserProfile User { get; set; }
        [ForeignKey("Image")]
        public int PictureId { get; set; }
        public virtual Picture Image { get; set; }

    }
}