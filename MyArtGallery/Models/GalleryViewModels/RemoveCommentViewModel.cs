﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MyArtGallery.Models.GalleryViewModels
{
    public class RemoveCommentViewModel
    {
        public int CommentId { get; set; }
        public Comment Comm { get; set; }

    }
}