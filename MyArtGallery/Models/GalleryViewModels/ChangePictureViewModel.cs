﻿using MyArtGallery.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MyArtGallery.Models.GalleryViewModels
{
    public class ChangePictureViewModel
    {
        public int PictureId { get; set; }
        [Display(Name = "Title", ResourceType = typeof(Resource))]
        public string Title { get; set; }

        [Display(Name = "ReplaceImage", ResourceType = typeof(ImageEditorRes))]
        public bool Replace { get; set; }
        [Display(Name = "EditImage", ResourceType = typeof(ImageEditorRes))]
        public bool Edit { get; set; }
        [Display(Name = "Animated", ResourceType = typeof(ImageEditorRes))]
        public bool Animated { get; set; }
        [Display(Name = "File", ResourceType = typeof(Resource))]
        public HttpPostedFileBase File { get; set; }
        [Display(Name = "ImgFromDisk", ResourceType = typeof(ImageEditorRes))]
        public bool IsFile { get; set; }
        [Display(Name = "Url", ResourceType = typeof(ImageEditorRes))]
        //[RegularExpression(@"^.*\b(gif|jpg|jpeg|png|bmp|tif|tiff|GIF|JPG|JPEG|PNG|BMP|TIF|TIFF)\b.*$ ", ErrorMessageResourceType = typeof(ImageEditorRes), ErrorMessageResourceName = "NotAnImage")]
        public string Url { get; set; }
        [Display(Name = "ImgFromUrl", ResourceType = typeof(ImageEditorRes))]
        public bool IsUrl { get; set; }
        //ścieżka istniejącego obrazu na dysku
        public string Path { get; set; }

        public int Width { get; set; }
        public int Height { get; set; }
        public int X { get; set; }
        public int Y { get; set; }
        [Display(Name = "TurnAngle", ResourceType = typeof(ImageEditorRes))]
        public int TurnAngle { get; set; }
        [Display(Name = "HorizontalFlip", ResourceType = typeof(ImageEditorRes))]
        public bool MirrorHorizontal { get; set; }
        [Display(Name = "VerticalFlip", ResourceType = typeof(ImageEditorRes))]
        public bool MirrorVertical { get; set; }

   
    }
}