﻿using MyArtGallery.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MyArtGallery.Models.GalleryViewModels
{
    public class EditPictureDataViewModel
    {
        public int PictureId { get; set; }
        [Display(Name = "Title", ResourceType = typeof(Resource))]
        [Required(ErrorMessageResourceType = typeof(ImageOptions),
              ErrorMessageResourceName = "TitleRequired")]
        [StringLength(100, ErrorMessageResourceType = typeof(ImageOptions),
              ErrorMessageResourceName = "TitleLong")]
        public string Title { get; set; }
        [Display(Name = "Description", ResourceType = typeof(Resource))]
        [StringLength(300, ErrorMessageResourceType = typeof(Resource),
              ErrorMessageResourceName = "DescriptionLong")]
        public string Description { get; set; }
        [Display(Name = "SetCover", ResourceType = typeof(ImageOptions))]
        public bool AlbumCover { get; set; }
        public string Url { get; set; }
        [Display(Name = "Tags", ResourceType = typeof(Resource))]
        [StringLength(300, ErrorMessageResourceType = typeof(ImageOptions),
              ErrorMessageResourceName = "TooMuchTags")]
        public string Tags { get; set; }

      

        [Display(Name = "Public", ResourceType = typeof(Resource))]
        public bool Public { get; set; }
        [Display(Name = "Adult", ResourceType = typeof(ImageOptions))]
        public bool Adult { get; set; }
        [Display(Name = "CommentsOff", ResourceType = typeof(CommentOptions))]
        public bool CommentsOff { get; set; }

        [Display(Name = "Category", ResourceType = typeof(ImageOptions))]
        [Required(ErrorMessageResourceType = typeof(ImageOptions),
              ErrorMessageResourceName = "CategoryRequired")]
        public int CategoryId { get; set; }
        public IEnumerable<SelectListItem> Categories { get; set; }
    }
}