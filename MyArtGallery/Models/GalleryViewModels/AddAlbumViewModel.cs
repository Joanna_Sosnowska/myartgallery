﻿using MyArtGallery.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MyArtGallery.Models.GalleryViewModels
{
    public class AddAlbumViewModel
    {
        [Display(Name = "Title", ResourceType = typeof(Resource))]
        [Required(ErrorMessageResourceType = typeof(GalleryOptions),
              ErrorMessageResourceName = "AlbumTitleRequired")]
        [StringLength(150, ErrorMessageResourceType = typeof(GalleryOptions),
              ErrorMessageResourceName = "AlbumTitleLong")]
        public string Title { get; set; }
        [Required]
        public string UserProfileId { get; set; }
        [Required]
        public int ParentId { get; set; }
        [Display(Name = "Description", ResourceType = typeof(Resource))]
        [StringLength(250, ErrorMessageResourceType = typeof(GalleryOptions),
              ErrorMessageResourceName = "AlbumDescLong")]
        public string Description { get; set; }
        public bool Public { get; set; }
        //nowy album nie ma okładki bo jest pusty
        
    }
}