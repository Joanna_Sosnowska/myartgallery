﻿using MyArtGallery.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MyArtGallery.Models.GalleryViewModels
{
    public class EditAlbumViewModel
    {
        [Display(Name = "Title", ResourceType = typeof(Resource))]
        [StringLength(150, ErrorMessageResourceType = typeof(GalleryOptions),
              ErrorMessageResourceName = "AlbumTitleLong")]
        public string Title { get; set; }
        public string Path { get; set; }
        [Display(Name = "Description", ResourceType = typeof(Resource))]
        [StringLength(250, ErrorMessageResourceType = typeof(GalleryOptions),
              ErrorMessageResourceName = "AlbumDescLong")]
        public string Description { get; set; }
        public bool Public { get; set; }
        
    }
}