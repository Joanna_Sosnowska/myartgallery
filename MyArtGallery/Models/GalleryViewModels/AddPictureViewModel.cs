﻿using MyArtGallery.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MyArtGallery.Models.GalleryViewModels
{
    public class AddPictureViewModel
    {
        [Display(Name = "Title", ResourceType = typeof(Resource))]
        [Required(ErrorMessageResourceType = typeof(ImageOptions),
              ErrorMessageResourceName = "TitleRequired")]
        [StringLength(100, ErrorMessageResourceType = typeof(ImageOptions),
              ErrorMessageResourceName = "TitleLong")]
        public string Title { get; set; }
        [Display(Name = "Description", ResourceType = typeof(Resource))]
        [StringLength(300, ErrorMessageResourceType = typeof(Resource),
              ErrorMessageResourceName = "DescriptionLong")]
        public string Description { get; set; }
        [Display(Name = "TagsForm", ResourceType = typeof(ImageOptions))]
        [StringLength(300, ErrorMessageResourceType = typeof(ImageOptions),
              ErrorMessageResourceName = "TooMuchTags")]
        public string Tags { get; set; }
        //autor albumu jest autorem obrazów w tym albumie
        public int AlbumId { get; set; }
        public string UserProfileId { get; set; }
        [Display(Name = "File", ResourceType = typeof(Resource))]
        public HttpPostedFileBase File { get; set; }
        [Display(Name = "ImgFromDisk", ResourceType = typeof(ImageEditorRes))]
        public bool IsFile { get; set; }
        [Display(Name = "Url", ResourceType = typeof(ImageEditorRes))]
        //RegularExpression(@"^.*\b(gif|jpg|jpeg|png|bmp|tif|tiff|GIF|JPG|JPEG|PNG|BMP|TIF|TIFF)\b.*$ ", ErrorMessageResourceType = typeof(ImageEditorRes), ErrorMessageResourceName = "NotAnImage")]
        public string Url { get; set; }
        [Display(Name = "ImgFromUrl", ResourceType = typeof(ImageEditorRes))]
        public bool IsUrl { get; set; }
        public string Path { get; set; }

        public int Width { get; set; }
        public int Height { get; set; }
        public int X { get; set; }
        public int Y { get; set; }
        [Display(Name = "Animated", ResourceType = typeof(ImageEditorRes))]
        public bool Animated { get; set; }
        [Display(Name = "TurnAngle", ResourceType = typeof(ImageEditorRes))]
        public int TurnAngle { get; set; }
        [Display(Name = "HorizontalFlip", ResourceType = typeof(ImageEditorRes))]
        public bool MirrorHorizontal { get; set; }
        [Display(Name = "VerticalFlip", ResourceType = typeof(ImageEditorRes))]
        public bool MirrorVertical { get; set; }
        [Display(Name = "Public", ResourceType = typeof(Resource))]
        public bool Public { get; set; }
        [Display(Name = "Adult", ResourceType = typeof(ImageOptions))]
        public bool Adult { get; set; }
        [Display(Name = "CommentsOff", ResourceType = typeof(CommentOptions))]
        public bool CommentsOff { get; set; }

        //wybór kategorii
        [Display(Name = "Category", ResourceType = typeof(ImageOptions))]
        [Required(ErrorMessageResourceType = typeof(ImageOptions),
              ErrorMessageResourceName = "CategoryRequired")]
        public int CategoryId { get; set; }
        public IEnumerable<SelectListItem> Categories { get; set; }
    }
}