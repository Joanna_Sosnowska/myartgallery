﻿using MyArtGallery.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MyArtGallery.Models.GalleryViewModels
{
    public class AddCommentViewModel
    {
        [Required]
        public int PictureId { get; set; }
        public int ParentId { get; set; }
        public virtual Picture Picture { get; set; }
        [Display(Name = "Author", ResourceType = typeof(Resource))]
        [Required]
        public string UserProfileId { get; set; }
        [Display(Name = "Comment", ResourceType = typeof(CommentOptions))]
        [Required(ErrorMessageResourceType = typeof(CommentOptions),
              ErrorMessageResourceName = "CommentRequired")]
        [StringLength(500, ErrorMessageResourceType = typeof(CommentOptions),
              ErrorMessageResourceName = "CommentLong")]
        public string Text { get; set; }
    }
}