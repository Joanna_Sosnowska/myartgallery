﻿using MyArtGallery.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MyArtGallery.Models.GalleryViewModels
{
    public class DownloadViewModel
    {
        public int Id { get; set; }
        [Display(Name = "Sizes", ResourceType = typeof(ImageOptions))]
        public int Size { get; set; }
        public IEnumerable<SelectListItem> Sizes { get; set; }
    }
}