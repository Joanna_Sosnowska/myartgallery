﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MyArtGallery.Models.GalleryViewModels
{
    public class MoveAlbumViewModel
    {

        public int AlbumId { get; set; }
        public string Path { get; set; }
        public IEnumerable<SelectListItem> Albums { get; set; }
    }
}