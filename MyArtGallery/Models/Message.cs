﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using MyArtGallery.Resources;

namespace MyArtGallery.Models
{
    public class Message
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int MessageId { get; set; }
        [ForeignKey("Author")]
        public string AuthorId { get; set; }
        [ForeignKey("Addressee")]
        public string AddresseeId { get; set; }
        [Display(Name = "From", ResourceType = typeof(MessagesAndNotes))]
        public virtual UserProfile Author { get; set; }
        [Display(Name = "To", ResourceType = typeof(MessagesAndNotes))]
        public virtual UserProfile Addressee { get; set; }
        [Display(Name = "MessageTitle", ResourceType = typeof(MessagesAndNotes))]
        [StringLength(100, ErrorMessageResourceType = typeof(MessagesAndNotes),
              ErrorMessageResourceName = "MessageTitleLong")]
        public string Title { get; set; }
        [Display(Name = "Message", ResourceType = typeof(MessagesAndNotes))]
        [Required(ErrorMessageResourceType = typeof(MessagesAndNotes),
              ErrorMessageResourceName = "MessageRequired")]
        [StringLength(500, ErrorMessageResourceType = typeof(MessagesAndNotes),
              ErrorMessageResourceName = "MessageLong")]
        public string Text { get; set; }
        [Display(Name = "MessageDate", ResourceType = typeof(MessagesAndNotes))]
        [DataType(DataType.Date)]
        public DateTime SentDate { get; set; }
        [Display(Name = "WasRead", ResourceType = typeof(MessagesAndNotes))]
        public bool WasRead { get; set; }
        [ForeignKey("Parent")]
        [Display(Name = "ParentMessage", ResourceType = typeof(MessagesAndNotes))]
        public int? ParentId{ get; set; }
        public virtual Message Parent{ get; set; }
    }
}