﻿using MyArtGallery.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace MyArtGallery.Models
{
    public class Tag
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int TagId { get; set; }
        [Display(Name = "TagName", ResourceType = typeof(Tagging))]
        [StringLength(100, ErrorMessageResourceType = typeof(Tagging),
              ErrorMessageResourceName = "TagNameLong")]
        [Required(ErrorMessageResourceType = typeof(Tagging),
              ErrorMessageResourceName = "TagNameRequired")]
        public string Name { get; set; }
        public virtual ICollection<Picture> Pictures { get; set; }
    }
}