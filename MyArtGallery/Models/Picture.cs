﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using MyArtGallery.Resources;

namespace MyArtGallery.Models
{
    public class Picture
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int PictureId { get; set; }
        [Display(Name = "Title", ResourceType = typeof(Resource))]
        [StringLength(100)]
        public string Title { get; set; }
        [Display(Name = "Description", ResourceType = typeof(Resource))]
        [StringLength(300)]
        public string Description { get; set; }
        [ForeignKey("Category")]
        public int CategoryId { get; set; }
        [Display(Name = "Category", ResourceType = typeof(ImageOptions))]
        public virtual Category Category { get; set; }
        [ForeignKey("UserProfile")]
        public string UserName { get; set; }
        [Display(Name = "Author", ResourceType = typeof(Resource))]
        public virtual UserProfile UserProfile{ get; set; }
        [ForeignKey("Album")]
        public int AlbumId { get; set; }
        [Display(Name = "Album", ResourceType = typeof(GalleryOptions))]
        public virtual Album Album { get; set; }
        [Display(Name = "File", ResourceType = typeof(Resource))]
        public string Url { get; set; }
        public string ThumbnailUrl { get; set; }
        public string FullSizeUrl { get; set; }
        [Display(Name = "Added", ResourceType = typeof(Resource))]
        [DataType(DataType.Date)]
        public DateTime AdditionDate { get; set; }
        [Display(Name = "LastModified", ResourceType = typeof(Resource))]
        [DataType(DataType.Date)]
        public DateTime LastModifiedDate { get; set; }
        [Display(Name = "Favourites", ResourceType = typeof(ImageOptions))]
        public int Favourites { get; set; }
        [Display(Name = "Downloads", ResourceType = typeof(ImageOptions))]
        public int Downloads { get; set; }
        //metadane
        [Display(Name = "Width", ResourceType = typeof(ImageOptions))]
        public int? Width { get; set; }
        [Display(Name = "Height", ResourceType = typeof(ImageOptions))]
        public int? Height { get; set; }
        [Display(Name = "FileSize", ResourceType = typeof(ImageOptions))]
        public string FileSize { get; set; }
        [Display(Name = "ExpTime", ResourceType = typeof(ImageOptions))]
        public string ExposureTime { get; set; }
        [Display(Name = "Camera", ResourceType = typeof(ImageOptions))]
        public string CameraType { get; set; }
        [Display(Name = "Flash", ResourceType = typeof(ImageOptions))]
        public bool Flash { get; set; }
        [Display(Name = "FocalLength", ResourceType = typeof(ImageOptions))]
        public int FocalLength { get; set; }
        [Display(Name = "ShutterSpd", ResourceType = typeof(ImageOptions))]
        public string ShutterSpd { get; set; }
        [Display(Name = "Software", ResourceType = typeof(ImageOptions))]
        public string Software { get; set; }
        [Display(Name = "DateTaken", ResourceType = typeof(ImageOptions))]
        public string DateTaken { get; set; }
        [Display(Name = "Colours", ResourceType = typeof(ImageOptions))]
        public string Colours{ get; set; }
        //metadane
        [Display(Name = "Public", ResourceType = typeof(Resource))]
        public bool Public { get; set; }
        [Display(Name = "Adult", ResourceType = typeof(ImageOptions))]
        public bool Adult { get; set; }
        [Display(Name = "CommentsOff", ResourceType = typeof(CommentOptions))]
        public bool CommentsOff { get; set; }
        public virtual ICollection<Comment> Comments { get; set; }
        [Display(Name = "Tags", ResourceType = typeof(Resource))]
        public virtual ICollection<Tag> Tags { get; set; }
        [Display(Name = "Favourites", ResourceType = typeof(ImageOptions))]
        public virtual ICollection<Favourite> Favourited { get; set; }
    }
}